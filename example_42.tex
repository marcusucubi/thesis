\documentclass{article}

\usepackage{latexsym}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{thmtools}
\usepackage{tikz-cd}
\usepackage{float}

\newcommand{\vs}{\vspace{0.2in}}
\newcommand{\vvs}{\vspace{0.1in}}
\newcommand{\aaa}{\alpha}
\newcommand{\bbb}{\beta}
\newcommand{\K}{\operatornamewithlimits{K}}
\newcommand{\C}{\mathbb{C}}

\newcommand{\mprod}[2]{
  \overset{\curvearrowright}
 {\prod_{#1}^{#2}}
}


\newcommand{\condots}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{\dots}}}
\newcommand{\cadd}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{+}}}
\newcommand{\cminus}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{-}}}
\newcommand{\AlgI}{\textbf{$\text{CF}^{-1}$}}
\newcommand{\Sub}{\textbf{Sub}}
\newcommand{\Id}{\textbf{Id}}
\newcommand{\Even}{\textbf{Even}}
\newcommand{\Alg}{\textbf{CF}\,}
\newcommand{\Contrac}{\textbf{Contraction}}
\newcommand{\GoldenF}
  {$[1,1, \dots]$\hspace{0.1cm}}
\newcommand{\GoldenR}
  {$(1+\sqrt{5})/2$\hspace{0.1cm}}
\newcommand{\SilverF}
  {$[2,2, \dots]$\,}
\newcommand{\SilverR}
  {$1+\sqrt{2}$\hspace{0.1cm}}
\newcommand{\GDF}
  {General Determinant Formula}


\declaretheorem[
  style=definition]
  {definition}
\declaretheorem[
  style=definition,
  qed=$\blacktriangle$]
  {example}
\declaretheorem[
  style=definition]
  {discussion}

\newtheorem*{LL}{Little Theorem}
\newtheorem*{BL}{Big Theorem}
\newtheorem*{Cassini}{Cassini's Identity}
\newtheorem*{golden}{Golden Ratio Lemma}
\newtheorem*{transformation}{Transformation Lemma}

\begin{document}

\begin{example}

The \textbf{Lucus numbers}
are defined by the following
recursive formula:
\begin{align*}
L_0 &= 2, \hspace{0.5cm} L_1 = 1,\\
L_n &= L_{n-1} + L_{n-2}
\end{align*}
for $n \ge 2$.

Let us calculate the
contraction of \GoldenF
using $f_{i} \mapsto f_{L_{i+1}}$
to generate the subsequence.
We can reuse table
\ref{tab:continuedFractionGoldenF}
and table
\ref{tab:partialGoldenF}
from example
\ref{example:evenContrationOf111}.
We generate the subsequence.

\begin{table}[H]
  \begin{center}
    \begin{tabular}
      {l|c|c|c|c|c|c|c|c|c}
      i & 0 & 1 & 2 & 3 &
      4 & 5 & 6 & 7 &
      $\cdots$ \\
      \hline
      $C_i$ &
      $F_{3}$ &
      $F_{5}$ &
      $F_{6}$ &
      $F_{9}$ &
      $F_{13}$ &
      $F_{20}$ &
      $F_{31}$ &
      $F_{49}$ &
      $\cdots$ \\
      \hline
      $D_i$ &
      $F_{2}$ &
      $F_{4}$ &
      $F_{5}$ &
      $F_{8}$ &
      $F_{12}$ &
      $F_{19}$ &
      $F_{30}$ &
      $F_{48}$ &
      $\cdots$
    \end{tabular}
    \caption{
    $f_{i} \mapsto f_{L_{i+1}}$ subsequence.}
  \end{center}
\end{table}

\noindent
We apply \AlgI.

\begin{table}[H]
  \begin{center}
    \begin{tabular}
    {l|c|c|c|c|c|c|c|c|c}
      i & 0 & 1 & 2 &
      3 & 4 & 5 & 6 & 7 &
      $\cdots$  \\
      \hline
      $c_i$ &
      $X$ &
      $-F_2/F_2$   &
      $-F_1/F_2$   &
      $F_3/F_1$    &
      $F_4/F_3$    &
      $-F_7/F_4$   &
      $F_{11}/F_7$ &
      $F_{18}/F_{11}$ &
      $\cdots$ \\
      \hline
      $d_i$ &
      $F_3/F_2$    &
      $F_4/F_2$    &
      $F_3/F_2$    &
      $F_4/F_1$    &
      $F_7/F_3$    &
      $F_{11}/F_4$ &
      $F_{18}/F_7$ &
      $F_{29}/F_{11}$ &
      $\cdots$
    \end{tabular}
    \caption{
    $f_i \mapsto f_{L_{i+1}}$
    contraction with F values}
  \end{center}
\end{table}

\noindent
Notice that a pattern starts
at column 3.
We can use D’Ocagne’s Identity
to calculate
the $n$th partial numerator
and denominator
for $n \ge 3$.
We find the numerator of $c_n$:
\begin{align*}
 F_{L_{n+1}+2} F_{L_{n}+1}-
 F_{L_{n+1}+1} F_{L_{n}+2}
 &=
 (-1)
 (F_{L_{n+1}+1} F_{{L_{n}}+2}-
 F_{L_{n}+1} F_{L_{n+1}+2}) \\
 &=
 (-1)^{L_{n}}
 F_{(L_{n+1}+1)-(F_{n}+1)}\\
 &=
 (-1)^{L_{n}}
 F_{L_{n-1}}.
\end{align*}
We find the numerator of $d_n$:
\begin{align*}
 F_{L_{n+1}+1} F_{L_{n-1}+2}-
 F_{L_{n-1}+1}   F_{F_{n+1}+2}
 &=
 (-1)^{L_{n-1}+1}
 F_{(L_{n+1}+1)-(L_{n-1}+1)}\\
 &=
 (-1)^{L_{n-1}+1}
 F_{L_{n}}.
\end{align*}
We find the denominator for
both $c_b$ and $d_n$:
\begin{align*}
 F_{L_{n-1}+2} F_{{L_{n}}+1}-
 F_{L_{n}+2} F_{L_{n-1}+1}
 &=
 (F_{L_{n}+1} F_{{L_{n-1}}+2}-
 F_{L_{n-1}+1} F_{L_{n}+2}) \\
 &=
 (-1)^{L_{n-1}+1}
 F_{(L_{n}+1)-(L_{n-1}+1)}\\
 &=
 (-1)^{L_{n-1}+1}
 F_{L_{n-2}}.
\end{align*}
Therefore,
\begin{align*}
c_n = (-1)^{L_{n-2}}
\cfrac{F_{L_{n-1}}}{F_{L_{n-2}}}
\hspace{0.5cm}
\text{and}
\hspace{0.5cm}
d_n =
\cfrac{F_{L_{n}}}{F_{L_{n-2}}}
\end{align*}
for $n \ge 3$.

\noindent
Since we are contracting
\GoldenF and \GoldenF converges
to \GoldenR,
Theorem \ref{thm:contractionsConverge}
implies that the contraction
also converges to \GoldenR.
Therefore, we can write:
\begin{align*}
 \frac{1+\sqrt{5}}{2}
 = 2 -
 \cfrac{1}{3}
 \cminus
 \cfrac{1}{2}
 \cadd
 \cfrac{2}{3}
 \cadd
 \cfrac{
 F_{L_3}/F_{L_2}
 }{
 F_{L_4}/F_{L_2}
 }
 \cminus
 \cfrac{
 F_{L_4}/F_{L_3}
 }{
 F_{L_5}/F_{L_3}
 }
 \cadd
 \cfrac{
 F_{L_5}/F_{L_4}
 }{
 F_{L_6}/F_{L_4}
 }
 \cadd
 \cdots
\end{align*}

\begin{align*}
 \frac{1+\sqrt{5}}{2}
 = 2 -
 \cfrac{1}{3}
 \cminus
 \cfrac{1}{2}
 \cadd
 \cfrac{2}{3}
 \cadd
 \cfrac{
 3 \cdot F_{L_1}/F_{L_4}
 }{1}
 \cminus
 \cfrac{
 F_{L_2}/F_{L_5}
 }{1}
 \cadd
 \cfrac{
 F_{L_3}/F_{L_6}
 }{1}
 \cadd
 \cdots
\end{align*}

\noindent
Letting $\omega$ be
the tail of the contraction,
starting at $n=4$,
we can rewrite it as:
\begin{align*}
 \frac{1+\sqrt{5}}{2}
 = 2 -
 \cfrac{1}
 {3-
 \cfrac{1}{2+
 \frac{2}{3+3\omega}
 }}
\end{align*}

\noindent
We can "absorb" the
leading terms on the
right hand side in to
the left hand side as follows:
\begin{align*}
 \frac{1+\sqrt{5}}{2}
 = 2 -
 \cfrac{1}
 {3-
 \cfrac{1}{2+
 \frac{2}{3+3\omega}
 }}
 \hspace{0.5cm}
 &\Rightarrow
 \hspace{0.5cm}
 \frac{3-\sqrt{5}}{2}
 =
 \cfrac{1}
 {3-
 \cfrac{1}{2+
 \frac{2}{3+3\omega}
 }}\\
 \hspace{0.5cm}
 &\Rightarrow
 \hspace{0.5cm}
 \frac{3+\sqrt{5}}{2}
 =
 3-
 \cfrac{1}{2+
 \frac{2}{3+3\omega}
 }\\
 \hspace{0.5cm}
 &\Rightarrow
 \hspace{0.5cm}
 \frac{3-\sqrt{5}}{2}
 =
 \cfrac{1}{2+
 \frac{2}{3+3\omega}
 }\\
 \hspace{0.5cm}
 &\Rightarrow
 \hspace{0.5cm}
 \frac{3+\sqrt{5}}{2}
 =
 2+\frac{2}{3+3\omega}\\
 &\Rightarrow
 \hspace{0.5cm}
 \frac{\sqrt{5}-1}{2}
 =
 \frac{2}{3+3\omega}\\
 &\Rightarrow
 \hspace{0.5cm}
 \frac{\sqrt{5}+1}{2}
 =\frac{3+3\omega}{2}\\
 &\Rightarrow
 \hspace{0.5cm}
 \frac{\sqrt{5}+1}{3}
 =1+ \omega
\end{align*}

\noindent
This gives the following.
\begin{align*}
 \frac{1+\sqrt{5}}{3}
 = 1 +
 \cfrac{
 F_{L_1}/F_{L_4}
 }{1}
 \cminus
 &\cfrac{
 F_{L_2}/F_{L_5}
 }{1}
 \cadd
 \cfrac{
 F_{L_3}/F_{L_6}
 }{1}
 \cadd\\
 &\cfrac{
 F_{L_4}/F_{L_7}
 }{1}
 \cminus
 \cfrac{
 F_{L_5}/F_{L_8}
 }{1}
 \cadd
 \cfrac{
 F_{L_6}/F_{L_9}
 }{1}
 \cadd
 \cdots
\end{align*}

\noindent
We can calculate the
numerators and denominators
of the contraction
in order to verify they
approach $1+\sqrt{5}/3$.

\bgroup
\def\arraystretch{1.0}
\begin{table}[H]
  \begin{center}
    \begin{tabular}{l|c|c|c|c|c|c}
      i & 0 & 1 & 2 & 3 & 4 &
      $\cdots$ \\
      \hline
      $E_i$ &
      $2$   &
      $5$   &
      $8$   &
      $34$  &
      $233$  &
      $\cdots$
      \\
      \hline
      $F_i$ &
      $1$   &
      $3$   &
      $5$   &
      $21$   &
      $144$   &
      $\cdots$
    \end{tabular}
    \caption{Numerators and
    denominators
    for $f_{i} \mapsto f_{L_{i+1}}$
    contraction.}
  \end{center}
\end{table}
\egroup
\end{example}

\end{document}

