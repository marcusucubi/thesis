#include <vector>
#include <iostream>
#include "objects.h"
using namespace std;

int gcd(int a, int b) {
	while (a != 0 && b != 0) {
		int c = b;
		b = a % b;
		a = c;
	}
	return a + b;
}

Matrix
Matrix::operator =(const Matrix &m) {
	_d.r1 = m.d().r1;
	_d.r2 = m.d().r2;
	return *this;
}

Rational Rational::reduce() const {
	const int c = gcd(_n, _d);
	return Rational(_n/c, _d/c);
	return (*this);
}

Rational Rational::operator*(const Rational &m) const {
	Rational r(_n*m.n(), _d*m.d());
	return r.reduce();
}

Rational Rational::operator /(const Rational &m) const {
	Rational r(m.d(), m.n());
	return (*this)*r;
}

Rational Rational::operator+(const Rational &m) const {
	Rational r(_n*m.d() + _d*m.n(), _d*m.d());
	return r.reduce();
}

Rational Rational::operator -(const Rational &m) const {
	Rational r1(*this);
	Rational r2(-m._n,m._d);
	return r1+r2;
}

Matrix
Matrix::operator *(const Matrix &m) const {
	return Matrix({
		{_d.r1.c1 * m._d.r1.c1 + _d.r1.c2 * m._d.r2.c1,
		 _d.r1.c1 * m._d.r1.c2 + _d.r1.c2 * m._d.r2.c2},
		{_d.r2.c1 * m._d.r1.c1 + _d.r2.c2 * m._d.r2.c1,
		 _d.r2.c1 * m._d.r1.c2 + _d.r2.c2 * m._d.r2.c2}
	});
}

const Matrix Matrix::inverse() const {
	return Matrix();
}


