#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include "objects.h"
#include "printing.h"
using namespace std;

vector<int> buildFibis(int test) {
	vector<int> fibis;
	vector<int> result;
	fibis.push_back(0);
	fibis.push_back(1);
	int last = 1;
	while(last < test) {
		int n_1 = fibis[fibis.size()-1];
		int n_2 = fibis[fibis.size()-2];
		last = n_1 + n_2;
		fibis.push_back(last);
		result.push_back(last);
	}
	return result;
}

vector<int> buildLucus(int test) {
	vector<int> values;
	vector<int> result;
	values.push_back(2);
	values.push_back(1);
	result.push_back(1);
	int last = 1;
	while(last < test) {
		int n_1 = values[values.size()-1];
		int n_2 = values[values.size()-2];
		last = n_1 + n_2;
		values.push_back(last);
		result.push_back(last);
	}
	return result;
}

vector<int> buildPells(int test) {
	vector<int> fibis;
	fibis.push_back(0);
	fibis.push_back(1);
	int last = 1;
	while(last < test) {
		int n_1 = fibis[fibis.size()-1];
		int n_2 = fibis[fibis.size()-2];
		last = 2*n_1 + n_2;
		fibis.push_back(last);
	}
	return fibis;
}

class FibiFilter : public FilterObject {
public:
	bool include(int test) {
		vector<int> fibis = buildFibis(test+1);
		return find(begin(fibis), end(fibis), test) != fibis.end();
	}
};

class LucusFilter : public FilterObject {
public:
	bool include(int test) {
		vector<int> fibis = buildLucus(test);
		return find(begin(fibis), end(fibis), test) != fibis.end();
	}
};

class PellFilter : public FilterObject {
public:
	bool include(int test) {
		vector<int> pells = buildPells(test);
		return find(begin(pells), end(pells), test) != pells.end();
	}
};

class ModFilter : public FilterObject {
public:
	ModFilter(int mod) : _mod(mod) {}
	bool include(int test) {
		return test % _mod == 0;
	}
private:
	const int _mod;
};

class OffsetFilter : public FilterObject {
public:
	OffsetFilter(int offset) : _offset(offset) {}
	bool include(int test) {
		return test >= _offset;
	}
private:
	const int _offset;
};

class NSquareFilter : public FilterObject {
public:
	bool include(int test) {
		for(int i = 0; i < 20; i++) {
			if (test == i * i) {
				return true;
			}
		}
		return false;
	}
};

class Filter2 : public FilterObject {
public:
	bool include(int test) {
		switch(test) {
		case 1:
		case 2:
		case 4:
		case 8:
		case 16:
		case 32:
		case 64:
		case 128:
			return true;
		default:
			return false;
		}
	}
};

void show_usage(std::string name) {
	std::cerr
		<< "Usage: "
		<< name
		<< " options:\n"
		<< "\t-input,\t\tNumber to generate the input fraction\n"
		<< "\t-size,\t\tNumber of convergents to generate\n"
		<< "\t-filter,\tUsed to generate the subsequence\n"
		<< "\t-fibi,\tUse a Fibinacci filter\n"
		<< "\t-pell,\tUse a Pell filter\n"
		<< "\t-lucus,\tUse a Pell filter\n"
		<< "\t-2,\tUse a 2^i filter\n"
		<< "\t-n2,\tUse a n^2 filter\n"
		<< "\t-offset,\tUse offset filter\n"
		<< "\t-inc,\tUse an incrementing input fraction\n"
		<< endl;
}

int main(int argc, char **argv) {

	string fractionNumber = "1";
	string subNumber = "1";
	string sizeNumber = "10";
	string offsetNumber = "10";
	bool fibiFilter = false;
	bool lucusFilter = false;
	bool pellFilter = false;
	bool useOffsetFilter = false;
	bool filter2 = false;
	bool useInc = false;
	bool useNSquareFilter = false;
	for (int i = 1; i < argc; ++i) {
		std::string arg = argv[i];
		if ((arg == "-h") || (arg == "--help")) {
			show_usage(argv[0]);
			return 0;
		} else if ((arg == "-i") || (arg == "-input")) {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				fractionNumber = argv[++i]; // Increment 'i' so we don't get the argument as the next argv[i].
			} else { // Uh-oh, there was no argument to the destination option.
				std::cerr << "--fraction option requires one argument."
						<< std::endl;
				return 1;
			}
		} else if ((arg == "-f") || (arg == "-filter")) {
			if (i + 1 < argc) {
				subNumber = argv[++i];
			} else {
				std::cerr << "--filter option requires one argument."
						<< std::endl;
				return 1;
			}
		} else if ((arg == "-o") || (arg == "-offset")) {
			if (i + 1 < argc) {
				offsetNumber = argv[++i];
				useOffsetFilter = true;
			} else {
				std::cerr << "--offset option requires one argument."
						<< std::endl;
				return 1;
			}
		} else if (arg == "-size") {
			if (i + 1 < argc) {
				sizeNumber = argv[++i];
			} else {
				std::cerr << "--size option requires one argument."
						<< std::endl;
				return 1;
			}
		} else if (arg == "-fibi") {
			fibiFilter = true;
		} else if (arg == "-lucus") {
			lucusFilter = true;
		} else if (arg == "-pell") {
			pellFilter = true;
		} else if (arg == "-2") {
			filter2 = true;
		} else if (arg == "-n2") {
			useNSquareFilter = true;
		} else if (arg == "-inc") {
			useInc = true;
		}
	}

	const int SIZE = std::stoi(sizeNumber);
	const int sub = std::stoi(subNumber);
	const int offset = std::stoi(offsetNumber);

	CSimplePeriodicFraction f = CSimplePeriodicFraction( { },
			{ std::stoi(fractionNumber) });
	if (useInc) {
		f = CSimplePeriodicFraction( { },
			{ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20 });
	}
	cout << endl;
//	printMatrixes(f);

	cout << endl;
	cout << "Fraction:" << endl;
	print(cout, f, SIZE);
	cout << endl;

	cout << "Convergents:" << endl;
	Convergents a = ForwardMapping().buildConvergentArray(f, SIZE);
	print(a, "A_i", "B_i");
	cout << endl;

	ModFilter modFilter(sub);
	OffsetFilter offsetFilter(offset);
	NSquareFilter nSquareFilter;
	FibiFilter fibi;
	LucusFilter lucus;
	PellFilter pell;
	Filter2 f2;
	FilterObject* pFilter = &modFilter;
	if (fibiFilter) {
		pFilter = &fibi;
	} else if (lucusFilter) {
		pFilter = &lucus;
	} else if (pellFilter) {
		pFilter = &pell;
	} else if (filter2) {
		pFilter = &f2;
	} else if (useOffsetFilter) {
		pFilter = &offsetFilter;
	} else if (useNSquareFilter) {
		pFilter = &nSquareFilter;
	}

	cout << "Subsequence:" << endl;
	print(a.filter(pFilter), "C_i", "D_i");
	cout << endl;

	cout << "Contraction:" << endl;
	CFraction cont = InverseMapping().buildContraction(f, pFilter, SIZE);
	cout << cont << endl;

	cout << "Convergents:" << endl;
	Convergents e = ForwardMapping().buildConvergentArray(cont, SIZE);
	print(e, "E_i", "F_i");
	cout << endl;
}
