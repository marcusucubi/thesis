#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>

#include "printing.h"
using namespace std;

ostream& operator<<(ostream& os, const Row& r) {
	os << '|' << setw(4) << r.c1 << ' ' <<
			setw(4) << r.c2 << '|';
	return os;
}

ostream& operator<<(ostream& os, const Data& d) {
	os << d.r1 << endl << d.r2 << endl;
	return os;
}

ostream& operator<<(ostream& os, const vector<int>& a) {
	os << '[';
	for(const int& i : a) {
		os << i << ' ';
	}
	os << ']';
	return os;
}

ostream& operator<<(ostream& os, const Matrix& m) {
	os << m.d() << endl;
	return os;
}

ostream& operator<<(ostream& os, const CSimplePeriodicFraction& f) {
	os << '[';
	for(const int& i : f.nonrepeat()) {
		os << i << ' ';
	}
	if (f.repeat().size() > 0) {
		os << "(repeat) ";
		for(const int& i : f.repeat()) {
			os << i << ' ';
		}
	}
	os << ']';
	return os;
}

void print(const Convergents& conv, string title_1, string title_2) {
	cout << "|  i  |";
	for (const Convergent &c : conv.a()) {
		cout << setw(5) << c.index << '|';
	}
	cout << endl;
	cout << "| " << title_1 << " |";
	for (const Convergent &c : conv.a()) {
		cout << setw(5) << c.A << '|';
	}
	cout << endl;
	cout << "| " << title_2 << " |";
	for (const Convergent &c : conv.a()) {
		cout << setw(5) << c.B << '|';
	}
	cout << endl;
}

void print(ostream& os, CSimplePeriodicFraction f, int size) {
	os << "|  i  |";
	for(int i = 0; i < size; i++) {
		os << setw(4) << i << '|';
	}
	os << endl;
	os << "| a_i |";
	for(int i = 0; i < size; i++) {
		os << setw(4) << 1 << '|';
	}
	os << endl;
	os << "| b_i |";
	for(int i = 0; i < size; i++) {
		os << setw(4) << f[i] << '|';
	}
	os << endl;
}

void printMatrixes(CSimplePeriodicFraction f) {
	ForwardMapping mapping;
	vector<Matrix> a = mapping.buildMatrixArray(f, 8);
	for (const Matrix &m : a) {
		cout << m.d().r1 << " ";
	}
	cout << endl;
	for (const Matrix &m : a) {
		cout << m.d().r2 << " ";
	}
	cout << endl;
}

ostream& operator<<(ostream& os, const Rational& r) {
	double x = (double)r.n()/(double)r.d();
	double intPart, fractPart;
	fractPart = modf(x, &intPart);
	if (fractPart == 0) {
		os << setw(4) << x;
	} else {
		os << setw(4) << r.n() << '/' << r.d();
	}
	return os;
}

ostream& operator<<(ostream& os, const CFraction& f) {
	os << "|  i  |";
	for(int i = 0; i < (int)f.a().size(); i++) {
		os << setw(4) << i << '|';
	}
	os << endl;
	os << "| c_i |";
	for(const Element& e : f.a()) {
		os << setw(4) << e.c << '|';
	}
	os << endl;
	os << "| d_i |";
	for(const Element& e : f.a()) {
		os << setw(4) << e.d << '|';
	}
	os << endl;
	return os;
}

