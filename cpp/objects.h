#ifndef OBJECTS_H_
#define OBJECTS_H_
#include <vector>
#include <functional>
#include "matrix.h"
using namespace std;

struct Convergent {
	Rational index, A, B;
};

struct FilterObject {
	virtual bool include(int index) = 0;
	virtual ~FilterObject(){}
};

class Convergents {
public:
	Convergents(vector<Convergent> v) : _a(v) {}
	Convergents filter(FilterObject* pFilter);
	Convergent operator[](const int pos) const;
	const vector<Convergent> a() const {return _a;}
private:
	vector<Convergent> _a;
};

class CSimplePeriodicFraction {
public:
	CSimplePeriodicFraction(vector<int> non, vector<int> r)
	: _nonrepeat(non), _repeat(r) {}
	int operator[](const int pos) const;
	vector<int> to(const int& size);
	const vector<int> nonrepeat() const {return _nonrepeat;}
	const vector<int> repeat() const {return _repeat;}
private:
	vector<int> _nonrepeat;
	vector<int> _repeat;
};
struct Element {
	Rational c, d;
};

class CFraction {
public:
	CFraction(vector<Element> a) : _a(a) {}
	Element operator[](const int index) const;
	const vector<Element> a() const {return _a;}
	vector<Element> to(const int& size);
private:
	vector<Element> _a;
};

class ForwardMapping {
public:
	vector<Matrix> buildMatrixArray(CSimplePeriodicFraction f, const int& size);
	Convergents buildConvergentArray(CSimplePeriodicFraction f, const int& size);
	vector<Matrix> buildMatrixArray(CFraction f, const int& size);
	Convergents buildConvergentArray(CFraction f, const int& size);
};

class InverseMapping {
public:
	Rational calculate_a(const Convergents& conv, int index);
	Rational calculate_b(const Convergents& conv, int index);
	CFraction buildContraction(
			CSimplePeriodicFraction f,
			FilterObject* pFilter, int size);
};

#endif /* OBJECTS_H_ */
