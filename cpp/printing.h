#ifndef PRINTING_H_
#define PRINTING_H_

#include <iostream>
#include "objects.h"
using namespace std;

ostream& operator<<(ostream& os, const Row& r);
ostream& operator<<(ostream& os, const Data& d);
ostream& operator<<(ostream& os, const vector<int>& a);
ostream& operator<<(ostream& os, const Matrix& m);
ostream& operator<<(ostream& os, const CSimplePeriodicFraction& f);
ostream& operator<<(ostream& os, const CFraction& f);
ostream& operator<<(ostream& os, const Rational& r);

void print(ostream& os, CSimplePeriodicFraction f, int size);
void print(const Convergents& conv, string title_1, string title_2);
void printMatrixes(CSimplePeriodicFraction f);


#endif /* PRINTING_H_ */
