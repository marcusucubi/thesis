#include <vector>
#include <iostream>
#include "objects.h"
using namespace std;

Convergents
Convergents::filter(FilterObject* pFilter) {
	vector<Convergent> r;
	int index = 0;
	int sub_index = 0;
	for (const Convergent &c : _a) {
		if (pFilter->include(index)) {
			Convergent c2 = c;
			c2.index = sub_index;
			r.push_back(c2); // @suppress("Invalid arguments")
			sub_index++;
		}
		index++;
	}
	return Convergents(r);
}

Convergent Convergents::operator[](const int index) const {
	if (index == -1) {
		return {-1, 1, 0};
	}
	return _a[index];
}

int CSimplePeriodicFraction::operator[](const int pos) const {
	int i = 0;
	for(const int& v: _nonrepeat) {
		if (i == pos) return v;
		i++;
	}
	while(i <= pos) {
		for(const int& v: _repeat) {
			if (i == pos) return v;
			i++;
		}
	}
	return 0;
}

vector<Element> CFraction::to(const int& size) {
	vector<Element> a;
	for(int i = 0; i < size && i < (int)_a.size(); i++) {
		a.push_back((*this)[i]);
	}
	return a;
}

vector<int> CSimplePeriodicFraction::to(const int& size) {
	vector<int> a;
	for(int i = 0; i < size; i++) {
		a.push_back((*this)[i]);
	}
	return a;
}

vector<Matrix> ForwardMapping::buildMatrixArray(
		CSimplePeriodicFraction f, const int& size) {
	vector<Matrix> r;
	Matrix id;
	Matrix& accum = id;
	vector<int> a = f.to(size);
	for (const int& i : a) {
		const Data d = {{i,1}, {1,0}};
		Matrix m(d);
		accum = m * accum;
		r.push_back(accum); // @suppress("Invalid arguments")
	}
	return r;
}

vector<Matrix> ForwardMapping::buildMatrixArray(
		CFraction f, const int& size) {
	vector<Matrix> r;
	Matrix id;
	Matrix& accum = id;
	vector<Element> a = f.to(size);
	int i = 0;
	for (const Element& e : a) {
		const Data d = {{e.d,e.c}, {1,0}};
		Matrix m(d);
		accum = m * accum;
		r.push_back(accum); // @suppress("Invalid arguments")
		i++;
	}
	return r;
}

Convergents ForwardMapping::buildConvergentArray(
		CSimplePeriodicFraction f, const int& size) {
	vector<Convergent> r;
	vector<Matrix> a = buildMatrixArray(f, size);
	int index = 0;
	for (const Matrix& m : a) {
		const Convergent f = {index, m.d().r1.c1, m.d().r1.c2};
		r.push_back(f); // @suppress("Invalid arguments")
		index++;
	}
	return Convergents(r);
}

Convergents ForwardMapping::buildConvergentArray(
		CFraction f, const int& size) {
	vector<Convergent> r;
	vector<Matrix> a = buildMatrixArray(f, size);
	int index = 0;
	for (const Matrix& m : a) {
		const Convergent f = {index, m.d().r1.c1, m.d().r1.c2};
		r.push_back(f); // @suppress("Invalid arguments")
		index++;
	}
	return Convergents(r);
}

Element CFraction::operator[](const int index) const {
	return _a[index];
}

Rational InverseMapping::calculate_a(const Convergents& c, int index) {

	if (index == 0) {
		return Rational(c[0].B);
	}

	Rational c_n = c[index].A;
	Rational c_n_1 = c[index-1].A;
	Rational d_n = c[index].B;
	Rational d_n_1 = c[index-1].B;
	Rational delta_1 = c_n*d_n_1-c_n_1*d_n;

	Rational c_n_2 = c[index-2].A;
	Rational d_n_2 = c[index-2].B;
	Rational delta_2 = c_n_1*d_n_2-c_n_2*d_n_1;
	return delta_1.negate() / delta_2;
}

Rational InverseMapping::calculate_b(const Convergents& c, int index) {

	if (index == 0) {
		return Rational(c[0].A);
	}

	Rational c_n_2 = c[index-2].A;
	Rational c_n = c[index].A;
	Rational d_n_2 = c[index-2].B;
	Rational d_n = c[index].B;
	Rational delta_1 = c_n*d_n_2-c_n_2*d_n;

	Rational c_n_1 = c[index-1].A;
	Rational d_n_1 = c[index-1].B;
	Rational delta_2 = c_n_1*d_n_2-c_n_2*d_n_1;
	return delta_1 / delta_2;
}

CFraction
InverseMapping::buildContraction(
		CSimplePeriodicFraction f,
		FilterObject* pFilter,
		int size) {

	vector<Element> elements;
	ForwardMapping mapping;
	Convergents array = mapping.buildConvergentArray(f, size);
	array = array.filter(pFilter);
	for(int i = 0; i < (int)array.a().size(); i++) {
		Rational a = calculate_a(array, i);
		Rational b = calculate_b(array, i);
		Element e = {a, b};
		elements.push_back(e);
	}

	return CFraction(elements);
}

