#ifndef MATRIX_H_
#define MATRIX_H_
#include <vector>
#include <functional>
using namespace std;

struct Rational {
	Rational() : _n(0), _d(1) {}
	Rational(const Rational& r) : _n(r._n), _d(r._d) {}
	Rational(const int& num) : _n(num), _d(1) {}
	Rational(const int& num, const int& denom) : _n(num), _d(denom) {}
	Rational operator =(const Rational &m) {
		_n = m._n; _d = m._d;
		return *this;
	}
	Rational operator *(const Rational &m) const;
	Rational operator /(const Rational &m) const;
	Rational operator +(const Rational &m) const;
	Rational operator -(const Rational &m) const;
	Rational negate() const {return Rational(-_n,_d);}
	Rational reduce() const;
	int n() const {return _n;}
	int d() const {return _d;}
private:
	int _n, _d;
};
struct Row {
	Rational c1, c2;
};
struct Data {
	Row r1, r2;
};

class Matrix {
public:
	Matrix() : _d({{1,0},{0,1}}) {}
	Matrix(Data data) : _d(data) {}
	Matrix(const Matrix& m) : _d(m._d) {}
	Matrix operator =(const Matrix &m);
	Matrix operator *(const Matrix &m) const;
	const Matrix inverse() const;
	const Data d() const {return _d;}
private:
	Data _d;
};

#endif /* MATRIX_H_ */
