\chapter{Proof of Theorem
\ref{thm:allSimpleConverge}}
\label{simplechap}

In this chapter we introduce
sequences of matrices that will
prove useful in the rest of
this thesis, and
we will give standard proofs
for Theorem
\ref{thm:allSimpleConverge}
\cite{hardy}.

\begin{definition}
Given a continued fraction, define
\begin{align}
  t_0 &=
  \left[ {\begin{array}{cc}
   b_0 & 1 \\
   1   & 0 \\
  \end{array} } \right],
  \hspace{0.5cm}
  t_n =
  \left[ {\begin{array}{cc}
   b_n & a_n \\
   1 & 0   \\
  \end{array} } \right]
  \label{equ:definitiont}
\end{align}
for $n \ge 1$.
Let
\begin{align}
  T_n &= t_n t_{n-1} \cdots t_1 t_0.
  \label{equ:tttT}
\end{align}
\end{definition}

\begin{thm}
Given a continued fraction
with numerator $A_n$ and
denominator $B_n$,
\begin{align}
  T_n =
  \left[ {\begin{array}{cc}
   A_{n}   & B_{n}   \\
   A_{n-1} & B_{n-1} \\
  \end{array} } \right].
  \label{equ:definitionT}
\end{align}
\label{thm:definitionT}
\end{thm}
\begin{proof}
Consider,
\begin{align*}
  T_0 = t_0 =
  \left[ {\begin{array}{cc}
   b_0 & 1 \\
   1   & 0 \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   A_{0}   & B_{0}   \\
   A_{-1} & B_{-1} \\
  \end{array} } \right],
  \,\text{and}
\end{align*}
\begin{align}
\label{equ:tNotationBaseCase}
  T_1 = t_1 T_0 =
  \left[ {\begin{array}{cc}
   b_1 & a_1 \\
   1   & 0   \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b_0 & 1 \\
   1   & 0 \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   b_0 b_1 + a_1 & b_1 \\
   b_0           & 1   \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   A_1 & B_1 \\
   A_0 & B_0 \\
  \end{array} } \right].
\end{align}
Let's use \eqref{equ:tNotationBaseCase}
for our base case.
Now suppose the theorem is true.
Then,
\begin{align*}
  T_{n+1} = t_{n+1} T_n
  &=
  \left[ {\begin{array}{cc}
   b_{n+1} & a_{n+1} \\
   1   & 0   \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   A_n     & B_n     \\
   A_{n-1} & B_{n-1} \\
  \end{array} } \right] \\
  &=
  \left[ {\begin{array}{cc}
   b_{n+1}A_{n} + a_{n+1}A_{n-1} &
     b_{n+1}B_{n} + a_{n+1}B_{n-1} \\
   A_{n}   & B_{n} \\
  \end{array} } \right]\\
  &=
  \left[ {\begin{array}{cc}
   A_{n+1} & B_{n+1}     \\
   A_{n}   & B_{n} \\
  \end{array} } \right].
\end{align*}
By induction, we have proven the result.
\end{proof}

The inverse continued fraction
 mapping, equation
 \eqref{equ:inverse},
 is derived by applying
 Crammer's rule to
 equation \eqref{equ:mapping}
 to solve for $a_n$ and
 $b_n$.
We can also derive it using
definition
\ref{equ:definitiont}.
Since
$T_n = t_n \cdots t_0 =
t_n\, T_{n-1}$,
we have
$t_n = T_n (T_{n-1})^{-1}$.
We can write this equation
as,
\begin{align*}
  \left[ {\begin{array}{cc}
   b_{n} & a_{n} \\
   1     & 0     \\
  \end{array} } \right]
  &=
  \frac{1}{A_{n-1}B_{n-2}-
    A_{n-2}B_{n-1}}
  \left[ {\begin{array}{cc}
   A_{n}   & B_{n}   \\
   A_{n-1} & B_{n-1} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
    B_{n-2} & -B_{n-1} \\
   -A_{n-2} &  A_{n-1} \\
  \end{array} } \right].
\end{align*}
Computing the top row
in this matrix product
gives the equations
desired.
\vs

From now to the end of the
chapter we will assume all
continued fractions are simple.
Given a simple continued
fraction, the
continued fraction mapping
is given by
 \begin{align}
  \left[ {\begin{array}{cc}
   A_{-1} & A_0 \\
   B_{-1} & B_0 \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   1 & b_0  \\
   0 & 1 \\
  \end{array} } \right]
  \text{ and }
  \hspace{0.4cm}
  \left[ {\begin{array}{cc}
   A_{n} \\
   B_{n} \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   A_{n-2} & A_{n-1} \\
   B_{n-2} & B_{n-1} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   1 \\
   b_n \\
  \end{array} } \right].
  \label{equ:AB}
\end{align}
From equation \eqref{equ:AB} we obtain,
\begin{align}
  A_n = A_{n-2} + b_n A_{n-1}
  \hspace{0.4cm}
  \text{ and }
  \hspace{0.4cm}
  B_n = B_{n-2} + b_n B_{n-1}.
\end{align}

\begin{thm}
\begin{align}
A_n B_{n-1} - A_{n-1} B_n =(-1)^{n+1}
\label{equ:odddif4}
\end{align}
\end{thm}
\begin{proof}
 Take the determinant of
 both sides of
 $T_n = t_n \cdots t_0$.
\end{proof}

\begin{cor}
\begin{align}
A_{n-1} B_{n-2} - A_{n-2} B_{n-1}
=(-1)(A_{n} B_{n-1} - A_{n-1} B_{n})
\label{equ:goDown}
\end{align}
\end{cor}

\begin{thm}
\begin{align}
A_n B_{n-2} - A_{n-2} B_n
=b_n (-1)^{n}
\label{equ:evendif3}
\end{align}
\end{thm}

\begin{proof}
Consider
\begin{align}
  \left[ {\begin{array}{cc}
   A_{n}   & B_{n}   \\
   A_{n-2} & B_{n-2} \\
  \end{array} } \right]
  &=
  \left[ {\begin{array}{cc}
   b_n A_{n-1} + A_{n-2} &
   b_n B_{n-1} + B_{n-2}     \\
   A_{n-2}   & B_{n-2} \\
  \end{array} } \right]\\
  &=
  \left[ {\begin{array}{cc}
   b_n & 1 \\
   0   & 1 \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   A_{n-1} & B_{n-1} \\
   A_{n-2} & B_{n-2} \\
  \end{array} } \right].
  \label{equ:test}
\end{align}
Taking the determinant of
both sides of \eqref{equ:test}
we get,
\begin{align*}
 A_n B_{n-2} - B_n A_{n-2}.
 =
 b_n (A_{n-1} B_{n-2} - A_{n-2} B_{n-1}).
\end{align*}
Applying equation \eqref{equ:odddif4}
and equation \eqref{equ:goDown}, we get,
\begin{align*}
 A_n B_{n-2} - B_n A_{n-2}.
 =
 b_n (-1)^n.
\end{align*}
\end{proof}

\begin{thm}
\label{thm:evenAreIncreasing}
The even
convergents $f_{2n}$
increase strictly with
$n$, while the
odd convergents
$f_{2n+1}$ decrease
strictly.
\end{thm}

\begin{proof}
Consider,
\begin{align*}
f_n - f_{n-2}
&=
\frac{A_n}{B_n} -
\frac{A_{n-2}}{B_{n-2}} \\
&=
\frac
  {A_n B_{n-2} - A_{n-2} B_n}
  {B_{n} B_{n-2}} \\
&=
\frac{(-1)^n b_n}{B_{n-2}B_n}.
& \text{Using equation}\,
\eqref{equ:evendif3}.
\end{align*}
Since $b_n$ is a positive
integer, $B_n$ will be positive.
Therefore $f_n - f_{n-2}$
will have the sign of $(-1)^n$.
This proves the theorem.
\end{proof}

\begin{thm}
\label{thm:oddGreaterThanEven}
Every odd convergent
is greater that any
even convergent.
\end{thm}

\begin{proof}
Consider,
\begin{align}
f_n - f_{n-1}
&= \notag
\frac{A_n}{B_n} -
\frac{A_{n-1}}{B_{n-1}} \\
&= \notag
\frac
  {A_n B_{n-1} - A_{n-1} B_n}
  {B_{n} B_{n-1}} \\
&=
\frac
  {(-1)^{n-1}}
  {B_{n-1}B_{n}}.
  & \text{Using equation}\,
  \eqref{equ:odddif4}.
\label{equ:fnMinusFn1}
\end{align}
Since $b_n$ is a positive
integer, $B_n$ will be positive.
Therefore $f_n - f_{n-1}$
has the sign $(-1)^{n-1}$, so that
\begin{align}
\label{equ:OddGreaterEven}
f_{2m+1} > f_{2m}.
\end{align}
If the theorem were
false, we should have
$f_{2m+1} \le f_{2\mu}$
for some pair $m, \mu$.
If $\mu < m$, then,
after Theorem
\eqref{thm:evenAreIncreasing},
$f_{2m+1} < f_{2m}$,
and if $\mu > m$,
then $f_{2\mu+1} < f_{2\mu}$.
Either inequality
contradicts equation
\eqref{equ:OddGreaterEven}.

\end{proof}

\begin{lem}
\label{thm:BGreaterThanN}
$B_n \ge n$
with inequality when
$n > 3$.
\end{lem}
\begin{proof}
$B_0 = 1$,
$B_1 = b_1 \ge 1$.
If $n \ge 2$, then
\begin{align*}
 B_n = b_n B_{n-1} + B_{n-2}
 \ge B_{n-1}+1,
\end{align*}
so that $B_n > B_{n-1}$
and $B_n \ge n$.
If $n > 3$, then
\begin{align*}
 B_n \ge
 B_{n-1} + B_{n-2}
 > B_{n-1}+1 \ge n,
\end{align*}
and so $B_n > n$.
\end{proof}

\begin{proof}[
Proof of Theorem
\eqref{thm:allSimpleConverge}]

Let
\begin{align}
 [ b_0, \, b_1, \, b_2, \, \dots ]
 \label{equ:infiniteSimple}
\end{align}
be an infinite simple
continued fraction.
Let
\begin{align*}
 f_n =
 \frac{A_n}{B_n}
\end{align*}
be the $n$th convergent
of \eqref{equ:infiniteSimple}.

By Theorem
\eqref{thm:evenAreIncreasing},
the even convergents form
an increasing sequence and
the odd convergents form
a decreasing sequence.
By Theorem
\eqref{thm:oddGreaterThanEven},
every even convergent
is less that $f_1$,
so that the increasing
sequence of even convergents
is bounded above.
Also every odd convergent
is greater that $f_0$,
so that the decreasing
sequence of odd
convergents is bounded
below.
Therefore the even
convergents tend
a limit, say $\sigma_1$,
and the odd convergents
tend a limit say
$\sigma_2$,
with $\sigma_1 \le \sigma_2$.

By Theorem
\eqref{thm:oddGreaterThanEven}
and Lemma
\eqref{thm:BGreaterThanN}
\begin{align*}
 \left|
 \frac{A_{2n}}{B_{2n}}-
 \frac{A_{2n-1}}{B_{2n-1}}
 \right|
 =
 \frac{1}{B_{2n}B_{2n-1}}
 \le
 \frac{1}{2n(2n-1)}
 \longrightarrow 0,
\end{align*}
so that $\sigma_1 =
\sigma_2 = f$.
\end{proof}


