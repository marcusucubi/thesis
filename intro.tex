\chapter{Introduction}
\label{introchap}

This thesis provides an
explanation of how certain
interesting continued
fraction formulas arrive.
For example, in
\cite{bowman} Bowman
proved:
\begin{align}
 \cfrac{1+\sqrt{5}}{2}
 = 1+
 \cfrac
 {
   F_{F_1}/F_{F_4}
 }
 {
 1-\cfrac
   {
     F_{F_2}/F_{F_5}
   }
   {
   1+\cfrac
   {
     F_{F_3}/F_{F_6}
   }
   {
     1+\cfrac
     {
       F_{F_4}/F_{F_7}
     }
     {
       1-\cfrac
       {
         F_{F_5}/F_{F_8}
       }
       {
         1+\cfrac
         {
           F_{F_6}/F_{F_9}
         }
         {
               \ddots
         }
       }
     }
   }
  }
}
\label{equ:one}
\end{align}
and
\begin{align}
 \cfrac{1+\sqrt{5}}{3}
 = 1+
 \cfrac
 {
   F_{L_1}/F_{L_4}
 }
 {
 1-\cfrac
   {
     F_{L_2}/F_{L_5}
   }
   {
   1+\cfrac
   {
     F_{L_3}/F_{L_6}
   }
   {
     1+\cfrac
     {
       F_{L_4}/F_{L_7}
     }
     {
       1-\cfrac
       {
         F_{L_5}/F_{L_8}
       }
       {
         1+\cfrac
         {
           F_{L_6}/F_{L_9}
         }
         {
           1+\cfrac
           {
             F_{L_7}/F_{L_{10}}
           }
           {
               \ddots
           }
         }
       }
     }
   }
  }
}.
\label{equ:two}
\end{align}

\newpage

In the previous formulas,
$F_n$ and $L_n$ represent
the $n$th Fibonacci and
Lucus numbers respectively.
The Fibonacci numbers are
defined by the following
recursive formula:
\begin{align*}
F_0 &= 0, \hspace{0.5cm} F_1 = 1,\\
F_n &= F_{n-1} + F_{n-2}
\hspace{0.5cm}
\text{for}
\hspace{0.5cm}
n \ge 2,
\end{align*}
and the Lucus numbers are
defined by the following
recursive formula:
\begin{align*}
L_0 &= 2, \hspace{0.5cm} L_1 = 1,\\
L_n &= L_{n-1} + L_{n-2}
\hspace{0.5cm}
\text{for}
\hspace{0.5cm}
n \ge 2.
\end{align*}

In the introduction, we provide
basic definitions and theorems
on simple continued fractions.
The introduction should give the reader
a high-level view of the theory.
Chapter two defines
useful matrices and uses
them to prove Theorem \ref{thm:allSimpleConverge}.
In Chapter three, we provide methods
to determine the limit for a few simple
continued fractions.
Chapter four calculates some examples of
continued fraction contractions
including equations
\eqref{equ:one} and
\eqref{equ:two}.
Also, an extended version
of the matrices from Chapter two
is presented in Chapter four.
This extended version is used by the
more general formulas presented
in the appendix.
Finally, in Chapter five, we
prove the formulas from the appendix.

\newpage
\begin{definition}
A
\textbf{continued fraction}
is an ordered pair
\begin{align}
 ((\{ a_n \}, \{ b_n \} ), \{ f_n \}),
\end{align}
where $\{ a_n \}_1^{\infty} $
and $\{ b_n \}_0^{\infty} $
are given sequences of complex
numbers, $a_n \ne 0$, and
where $\{ f_n \}$ is the sequence of
extended complex numbers, given
by
\begin{align}
 f_n = S_n(0),
\label{equ:f_S}
\end{align}
for $n \ge 0$, where
\begin{align}
\label{equ:S}
 S_0(\omega) = s_0(\omega),
 \hspace{0.5cm}
 S_n(\omega)=S_{n-1}(s_n(\omega))
\end{align}
for $n \ge 1$,
\begin{align}
\label{equ:s}
 s_0(\omega)=b_0+\omega,
 \hspace{0.3cm}
 s_n(\omega)=\frac{a_n}{b_n+\omega},
\end{align}
for $n \ge 1$ \cite{lorentzen}.
If $a_n = 1$ for $n \ge 1$
and $b_n$ is a positive
integer for $n \ge 1$,
then we say the continued
fraction is \textbf{simple}.
\end{definition}

\vs
\newpage
\begin{definition}
The
\textbf{continued fraction mapping}
is the function \textbf{CF}
mapping a pair

\noindent
$(\{ a_n \}, \{ b_n \})$
onto the sequence $\{ f_n \}$
defined by \eqref{equ:f_S},
\eqref{equ:S}, and
\eqref{equ:s}.
The numbers $a_n$ and $b_n$
are called the
\textbf{$n$th partial numerators}
and \textbf{denominators}.
The number
\[
S_n(0)=
b_0+ \cfrac{a_1}{b_1 +
\cfrac{a_2}{b_2 +
\cfrac{a_3}{
\ddots+
\cfrac{a_n}{b_n}
}}}
\]
is called the $n$th
\textbf{approximant}.
\end{definition}

\noindent
We shall use the more
convenient way to
write approximants:
\[
b_0 +
\frac{a_1}{b_1} \cadd
\frac{a_2}{b_2} \cadd
\frac{a_3}{b_3} \cadd
\cdots \cadd
\frac{a_n}{b_n}.
\]

\noindent
The first few approximants are:
\begin{align*}
 f_0 = b_0,
 \hspace{0.5cm}
 f_1 = \frac{b_0b_1+a_1}{b_1},
 \hspace{0.5cm}
 f_2 =
 \frac{b_0b_1b_2+b_0a_2+a_1b_2}
 {b_1b_2+a_2}.
\end{align*}

\noindent
The approximants can be written
as fractions:
\begin{align*}
 f_n=\frac{A_n}{B_n}
\end{align*}
for $n \ge 0$. We will
call $A_n$ and $B_n$
$n$th numerator and denominator.

\noindent
If we define
\begin{align}
  \left[ {\begin{array}{cc}
   A_{-1} & A_0 \\
   B_{-1} & B_0 \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   1 & b_0 \\
   0 & 1   \\
  \end{array} } \right]
  \label{equ:preMapping}
\end{align}
then the following recurrence
relation holds:
\begin{align}
  \left[ {\begin{array}{cc}
   A_{n} \\
   B_{n} \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   A_{n-2} & A_{n-1} \\
   B_{n-2} & B_{n-1} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   a_n \\
   b_n \\
  \end{array} } \right]
  \label{equ:mapping}
\end{align}
for $n \ge 1$.
We will denote
\eqref{equ:preMapping}
and \eqref{equ:mapping}
by $\Alg$.

\begin{definition}
 The
 \textbf{inverse continued fraction
 mapping},
 denoted $\AlgI$,
 is a function mapping a set of ordered pairs
 $\{ (C_n,D_n) \} $ $(n \ge 0)$
 onto the set
 $\{d_0, (c_n,d_n)\}$ $(n \ge 1)$
 provided that
 $A_n B_{n-1}- A_{n-1} B_n \ne 0$,
 $(n \ge 0)$
 and given by,
 $d_0 = C_0/D_0$ and
\begin{align}
  c_n=
  \frac{
    \left| {\begin{array}{cc}
     C_{n} & C_{n-1} \\
     D_{n} & D_{n-1} \\
    \end{array} } \right|
  } {
    \left| {\begin{array}{cc}
     C_{n-2} & C_{n-1} \\
     D_{n-2} & D_{n-1} \\
    \end{array} } \right|
  }
\text{, and }\:
d_n=
  \frac{
    \left| {\begin{array}{cc}
     C_{n-2} & C_{n} \\
     D_{n-2} & D_{n} \\
    \end{array} } \right|
  } {
    \left| {\begin{array}{cc}
     C_{n-2} & C_{n-1} \\
     D_{n-2} & D_{n-1} \\
    \end{array} } \right|
  }
  \hspace{0.2cm} \text{ for } n \ge 1.
  \label{equ:inverse}
\end{align}
\end{definition}

\begin{remark}
If we define
$\Delta_n = C_nD_{n-1}-C_{n-1}D_n$,
then we have the following alternative
expression for equation
\eqref{equ:inverse},
\begin{align}
 c_n = \frac{-\Delta_n}{\Delta_{n-1}}
 \hspace{0.5cm} \text{and} \hspace{0.5cm}
 d_n = \frac{C_nD_{n-2}-C_{n-2}D_n}
 {\Delta_{n-1}}.
\end{align}
\end{remark}

\begin{definition}
 Given a mapping
 $f_i \mapsto f_{n_i}$
 such that $n_i < n_{i+1}$
 and $n_0 \ge 0$,
 the \textbf{contraction}
 of a continued fraction
 is a function
 mapping a continued
 fraction onto another
 continued fraction
 defined by
 $\AlgI\, \circ\,
 f_i \mapsto f_{n_i}
 \,\circ\, \Alg$.
\end{definition}

A contraction
may be visualized by a
square diagram. This
is called a commutative
diagram because the
composites along both
paths from the upper
left to the upper right
are equal \cite{maclane}.

\begin{equation*}
\begin{tikzcd}
  [column sep=huge,
   row sep=huge]
  \{ a_i, b_i \}
    \arrow[r, "\Contrac"]
    \arrow[d, "\Alg"]
    &
  \{ c_i, d_i \}
  \\
  \frac{A_i}{B_i}
    \arrow[r, "f_i \mapsto f_{n_i}"  ]
    &
  \frac{C_i}{D_i}
    \arrow[u, "\AlgI"]
\end{tikzcd}
\end{equation*}

\begin{definition}
 \label{def:converge}
 An infinite continued fraction
 \textbf{converges}
 to a number $f$ if
 its sequence of approximants
 converges to $f$.
\end{definition}

\begin{thm}
\label{thm:allSimpleConverge}
 Every infinite simple continued
 fraction converges.
\end{thm}

A proof is given in the next chapter.

\begin{thm}
\label{thm:contractionsConverge}
 Every contraction of a convergent
 continued fraction converges
 to the same number as
 the original continued
 fraction.
\end{thm}

\begin{proof}
 The result follows directly
 from Definition \ref{def:converge}
 and that
 every subsequence of a
 convergent sequence
 converges to the same
 limit as the original sequence.
\end{proof}

\vs
\begin{example}
 Let's calculate the
 contraction of \GoldenF
 with the subsequence
 generated by
 $f_i \mapsto f_{2i}$.
 To make computation
 easier, we display
 the continued fraction
 in a table.

\begin{table}[H]
  \begin{center}
    \begin{tabular}{l|c|c|c|c|c|c|c|c}
      i&0&1&2&3&4&5&
      $\cdots$ & n \\
      \hline
      $a_i$ &
      1 & 1 & 1 & 1 & 1 & 1 &
      $\cdots$ & 1 \\
      \hline
      $b_i$ &
      1 & 1 & 1 & 1 & 1 & 1 &
      $\cdots$ & 1
    \end{tabular}
    \caption{$[1,1,\dots]$.}
    \label{tab:continuedFractionGoldenF}
  \end{center}
\end{table}
\noindent
We apply the \Alg mapping.
\begin{table}[H]
  \begin{center}
    \begin{tabular}{l|c|c|c|c|c|c|c|c|c}
      i&-1&0&1&2&3&4&5&
      $\dots$ & n \\
      \hline
      $A_i$ &
      $1$ & $1$ & $2$ & $3$ &
      $5$ & $8$ & $13$ &
      $\cdots$ & $F_{n+2}$ \\
      \hline
      $B_i$ &
      $0$ & $1$ & $1$ & $2$ &
      $3$ & $5$ & $8$ &
      $\cdots$ & $F_{n+1}$
    \end{tabular}
    \caption{
    Numerators and denominators of
    $[1, 1, \dots]$.}
  \end{center}
\end{table}

\noindent
Let's write the table again
using Fibonacci Numbers.
\begin{table}[H]
  \begin{center}
    \begin{tabular}{l|c|c|c|c|c|c|c|c|c}
      i&-1&0&1&2&3&4&5&
      $\dots$ & n \\
      \hline
      $A_i$ &
      $F_1$ & $F_2$ & $F_3$ &
      $F_4$ & $F_5$ & $F_6$ &
      $F_7$ &
      $\cdots$ & $F_{n+2}$ \\
      \hline
      $B_i$ &
      $F_0$ & $F_1$ & $F_2$ &
      $F_3$ & $F_4$ & $F_5$ &
      $F_6$ &
      $\cdots$ & $F_{n+1}$
    \end{tabular}
    \caption{
    Numerators and denominators of
    $[1, 1, \dots]$
    (with Fibonacci numbers).}
    \label{tab:partialGoldenF}
  \end{center}
\end{table}


\noindent
We generate the
subsequence using
$f_i \mapsto f_{2i}$.
\begin{table}[H]
  \begin{center}
    \begin{tabular}{l|c|c|c|c|c|c|c|c|c}
      i&-1&0&1&2&3&4&5&
      $\cdots$ & n \\
      \hline
      $C_i$ &
      $F_1$ & $F_2$ & $F_4$ &
      $F_6$ & $F_8$ & $F_{10}$ &
      $F_{12}$ &
      $\cdots$ & $F_{2n+2}$ \\
      \hline
      $D_i$ &
      $F_0$ & $F_1$ & $F_3$ &
      $F_5$ & $F_7$ & $F_9$ &
      $F_{11}$ &
      $\cdots$ & $F_{2n+1}$
    \end{tabular}
    \caption{
    $f_i \mapsto f_{2i}$
    subsequence.}
  \end{center}
\end{table}

\noindent
We apply \AlgI.
We can use D’Ocagne’s Identity,
$F_m F_{n+1} - F_n F_{m+1}
= (-1)^n F_{m-n}$,
to calculate
$c_1$ and $d_1$.
\begin{align*}
  c_1=
  \frac{
    \left| {\begin{array}{cc}
     F_4 & F_2 \\
     F_3 & F_1 \\
    \end{array} } \right|
  } {
    \left| {\begin{array}{cc}
     F_1 & F_2 \\
     F_0 & F_1 \\
    \end{array} } \right|
  }=
  \frac{F_4F_1-F_2F_3}
  {F_1F_1-F_0F_2}
  =
  \frac{(-1)(-1)^1 F_{3-1}}
  {(-1)^0 F_{1-0}}
  =
  \frac{F_2}{F_1}
\end{align*}
\begin{align*}
  d_1=
  \frac{
    \left| {\begin{array}{cc}
     F_1 & F_4 \\
     F_0 & F_3 \\
    \end{array} } \right|
  } {
    \left| {\begin{array}{cc}
     F_1 & F_2 \\
     F_0 & F_1 \\
    \end{array} } \right|
  }=
  \frac{F_3F_1-F_0F_4}
  {F_1F_1-F_0F_2}
  =
  \frac{(-1)^0 F_{3-0}}
  {(-1)^0 F_{1-0}}
  =
  \frac{F_3}{F_1}
\end{align*}

\noindent
Continuing this way gives us
the following contraction.

\begin{table}[H]
  \begin{center}
    \begin{tabular}{l|c|c|c|c|c|c|c}
      i & 0 & 1 & 2 & 3 & 4 & 5 &
      $\cdots$  \\
      \hline
      $c_i$ &
      X & $F_2/F_1$ &
      $-F_2/F_2$ & $-F_2/F_2$ &
      $-F_2/F_2$ & $-F_2/F_2$ &
      $\cdots$ \\
      \hline
      $d_i$ &
      $F_2/F_1$ & $F_3/F_1$ &
      $F_4/F_2$ & $F_4/F_2$ &
      $F_4/F_2$ & $F_4/F_2$ &
      $\cdots$
    \end{tabular}
    \caption{
    $f_i \mapsto f_{2i}$
    contraction
    (with Fibonacci numbers).}
  \end{center}
\end{table}

\noindent
Notice that a pattern starts
at column 2.
We find the numerator of
$c_n$ for $n \ge 2$:
\begin{align*}
 F_{2n+2} F_{2(n-1)+1}-
 F_{2n+1} F_{2(n-1)+2}
 &=
 (-1)(F_{2n+1} F_{2(n-1)+2}-
 F_{2(n-1)+1} F_{2n+2})\\
 &=
 (-1)(-1)^{2(n-1)+1}
 F_{(2n+1-(2(n-1)+1))}\\
 &=
 (-1)^{2(n-1)}
 F_{(2n+1-(2(n-1)+1))}\\
 &=(-1)^{2(n-1)} F_2.
\end{align*}

\noindent
We find the numerator of
$d_n$ for $n \ge 2$:
\begin{align*}
 F_{2n+1} F_{2(n-2)+2}-
 F_{2n+2} F_{2(n-2)+1}
 &=
 (-1)^{2(n-2)+1}
 F_{(2n+1-(2(n-2)+1))}\\
 &=
 (-1)^{2(n-2)+1}F_4.
\end{align*}

\noindent
We find the denominator for
both $c_n$ and $d_n$
for $n \ge 2$:
\begin{align*}
 F_{2(n-1)+1} F_{2(n-2)+2}-
 F_{2(n-2)+1} F_{2(n-1)+2}
 &=
 (-1)^{2(n-2)+1}
 F_{(2(n-1)+1)-(2(n-2)+1)}\\
 &=
 (-1)^{2(n-2)+1}F_2.
\end{align*}

\noindent
Therefore,
\begin{align*}
c_n =
-\cfrac{F_2}{F_2}
\hspace{0.5cm}
\text{and}
\hspace{0.5cm}
d_n =
\cfrac{F_4}{F_2}
\end{align*}
for $n \ge 2$.

\noindent
We will learn in
chapter \ref{chp:convergence}
that \GoldenF converges
to \GoldenR.
Since we are contracting
\GoldenF,
Theorem \ref{thm:contractionsConverge}
implies that the contraction
also converges to \GoldenR.
Therefore, we can write:
\begin{align*}
 \frac{1+\sqrt{5}}{2}
 = 1 +
 \cfrac{1}{2}
 \cminus
 \cfrac{1}{3}
 \cminus
 \cfrac{1}{3}
 \cminus
 \cfrac{1}{3}
 \cminus
 \cfrac{1}{3}
 \cminus
 \cdots
\end{align*}

\noindent
Letting $\omega$ be
the tail of the contraction,
starting at $n=2$,
we can rewrite it as:
\begin{align*}
 \frac{1+\sqrt{5}}{2}
 = 1 +
 \cfrac{1}
 {2+\omega}
\end{align*}

\noindent
We can "absorb" the
leading terms on the
right hand side in to
the left hand side as follows:
\begin{align*}
 \frac{1+\sqrt{5}}{2}
 = 1 +
 \cfrac{1}
 {2+\omega}
 \hspace{0.5cm}
 &\Rightarrow
 \hspace{0.5cm}
 \frac{\sqrt{5}-1}{2}
 = \cfrac{1}{2+\omega}
 \hspace{0.5cm}
 \Rightarrow
 \hspace{0.5cm}
 \frac{1+\sqrt{5}}{2}
 = 2+\omega
\end{align*}

\noindent
This gives the following.
\begin{align*}
 \frac{1+\sqrt{5}}{2}
 = 2 -
 \cfrac{1}{3}
 \cminus
 \cfrac{1}{3}
 \cminus
 \cfrac{1}{3}
 \cminus
 \cfrac{1}{3}
 \cminus
 \cdots
\end{align*}

\noindent
We can calculate the
numerators and denominators
of the contraction
in order to verify they
approach \GoldenR.

\bgroup
\def\arraystretch{1.0}
\begin{table}[H]
  \begin{center}
    \begin{tabular}
    {l|c|c|c|c|c|c|c|c|}
      i & 0 & 1 & 2 & 3 &
      4 & 5 & 6 \\
      \hline
      $E_i$ &
      $2$   &
      $5$   &
      $13$   &
      $34$  &
      $89$  &
      $233$  &
      $610$
      \\
      \hline
      $F_i$ &
      $1$   &
      $3$   &
      $8$   &
      $21$   &
      $55$   &
      $144$   &
      $377$
    \end{tabular}
    \caption{Numerators and
    denominators
    for $f_{i} \mapsto f_{2i}$
    contraction.}
  \end{center}
\end{table}
\egroup

\label{example:evenContrationOf111}
\end{example}


The key takeaway from this
chapter is the contraction
function preserves
convergency.
So since all simple
continued fractions
converge, all contractions
of simple continued
fractions converge.




