\documentclass{article}

\usepackage{amsfonts}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{epsfig}
\usepackage{fullpage}
\usepackage{comment}
\usepackage{dcolumn}
\usepackage[yyyymmdd,hhmmss]{datetime}
\usepackage{listings}
\usepackage{color}
\usepackage{xcolor}

\newcommand{\vs}{\vspace{0.2in}}
\newcommand{\vvs}{\vspace{0.1in}}
\newcommand{\aaa}{\alpha}
\newcommand{\bbb}{\beta}
\newcommand{\K}{\operatornamewithlimits{K}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\CC}{\mathbb{\hat{C}}}

\newcommand{\condots}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{\dots}}}
\newcommand{\cadd}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{+}}}
\newcommand{\cminus}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{-}}}
\newcommand{\prf}{\underline{proof}}
\newcommand{\theorem}[1]{\underline{#1}}

\setlength{\parindent}{0cm}
%\renewcommand{\familydefault}{\ttdefault}

\begin{document}

\vs


%------------------------------------------------
%------------------------------------------------
%                  SECTION 4
%------------------------------------------------
%------------------------------------------------
%----------------------------------
%---------   Convergence   --------
%----------------------------------
\newpage

\textbf{Convergence}
\vs

Recall what convergence 
means for an infinite sum.
If $\{ t_n \}$ be a sequence of complex
numbers, then the following 
series
\[
\sum^{\infty}_{n=1} t_n =
t_1 + t_2 + \cdots + t_n + \cdots
\]
can be consider as
a sequence $T_n$ of
\emph{partial sums}

\[
T_n = \sum^{n}_{k=1} t_k.
\]

We can write this recursively,
\[
T_{n+1} = T_{n} + t_{n+1}.
\]

We define convergence of the series,
to be convergence of the sequence of 
partial sums $\{T_n\}$. 
We denote this by 
\[
\sum^{\infty}_{n=1}t_n = T.
\]
\vs

This general idea of convergence 
can be applied to continued
fractions.

Consider the sequence $\{ f_n \}$
defined by

\[
f_1 = a_1 \text{, }
f_2 = \frac{a_1}{1+a_2}\text{, }
f_3 = \frac{a_1}{
        1+\frac{a_2}{1+a_3}
      },
\]

and generally

\[ 
f_n = 
\cfrac{a_1}{1 +
\cfrac{a_2}{1 +
\cfrac{a_3}{\ddots + 
\cfrac{a_{n-1}}
{1 + a_n}
}}} 
\]

We have constructed a continued
fraction from a sequence in the
same way we constructed a 
series from a sequence.
Just as we thought about the
convergence of the series
in terms of convergence of
the partial sums,
we can also think about convergence
of the partial continued fractions.
We say the continued fraction
converges to a number $L$,
if the series of approximates
converges to $L$.
\vs

%----------------------------------
%------ END Convergence  END ------
%----------------------------------




\newpage
We use the following notation
to denote the infinite 
continued fraction,
\[ \label{eqn:simple_1} \tag{1.1.5}
\K^{\infty}_{n = 1}(a_n/1)=
\cfrac{a_1}{1 +
\cfrac{a_2}{1 +
\cfrac{a_3}{\ddots}}} 
\]

Quite similarly we can construct,
from any sequence $\{ b_n \}$
of complex numbers, a continued
fraction
\[ \label{eqn:simple_2} \tag{1.1.6}
\K^{\infty}_{n = 1}(1/b_n)=
\cfrac{1}{b_1 +
\cfrac{1}{b_2 +
\cfrac{1}{\ddots}}} 
\]
or from two sequences,
$\{ a_n \}$ and $\{ b_n \}$ of
complex numbers, where all
$a_n \ne 0$, a continued fraction
\[ \label{eqn:simple_12} \tag{1.1.7}
\K^{\infty}_{n = 1}(a_n/b_n)=
\cfrac{a_1}{b_1 +
\cfrac{a_2}{b_2 +
\cfrac{a_3}{\ddots}}} 
\]
\ref{eqn:simple_1} and \ref{eqn:simple_2}
are special cases of \ref{eqn:simple_12}.
In the particular case when \ref{eqn:simple_2}
all $b_n$ are natural numbers we get the
regular continued fraction, well known in
number theory, the one coming from the
Euclidean algorithm.  

Let us take a look at the common pattern in the 
three cases: series, products and continued
fractions.  In all three cases the construction can
described in the following way:
We have a sequence $\{ \phi_k \}$ of mappings
from $\C$ into $\CC$.

By composition we construct a new sequence
$\{ \Phi_n \}$ of mappings
\[
\Phi_1=\phi_1,
\hspace{0.5cm}
\Phi_n=\Phi_{n-1} \circ \phi_n =
\phi_1 \circ \phi_2 \circ \cdots \circ \phi_n.
\]
In all three cases there is a fixed complex 
number $c$, by means of which convergence
is defined: as convergence of
$\{ \Phi_n(c) \}$ for that particular $c$.

For series we have 
\[
\phi_k(w)=w+t_k,
\]
and the partial sums are
\[
\Phi_n(0) = \phi_1 \circ \phi_2 \circ
\dots \circ \phi_n(0) = 
t_1 + t_2 + \cdots + t_n,
\]
i.e. here we have $c=0$.

For continued fractions \eqref{eqn:simple_12}
we have 
\[
\phi_k(w)=\frac{a_k}{b_k + w},
\]
and the approximants are 
\[ 
\Phi_n(0) = 
\cfrac{a_1}{b_1 +
\cfrac{a_2}{b_2 +
\cfrac{a_3}{\ddots + 
\cfrac{a_{n}}
  {b_n}
}}} 
\]
i.e. here we have $c=0$.
\newpage

%------------------------------------------
%-------------- Definition ----------------
%------------------------------------------

\textbf{Definition}
A continued fraction is an ordered pair
\[ \label{eqn:cf_1} \tag{1.2.1}
(( \{ a_n \}, \{ b_n \}), \{ f_n \}),
\]
where $\{ a_n \}_1^{\infty} $ and
$\{ b_n \}^{\infty}_0$ are given sequences 
of complex numbers, $a_n \ne 0$, and where
$\{ f_n \}$ is the sequence of extended complex numbers,
given by 
\[ \label{eqn:cf_2} \tag{1.2.2}
f_n = S_n(0), 
\hspace{0.5cm}
n=0,1,2,3,\dots,
\]
where
\[ \label{eqn:cf_3a} \tag{1.2.3a}
S_0(w)=s_0(w),
\hspace{0.5cm}
S_n(w)=S_{n-1}(s_n(w)),
\hspace{0.5cm}
n=1,2,3,\dots,
\]
\[ \label{eqn:cf_3b} \tag{1.2.3b}
s_0(w)=b_0+w,
\hspace{0.5cm}
s_n(w)=\frac{a_n}{b_n+w},
\hspace{0.5cm}
n=1,2,3,\dots,
\]

The continued fraction algorithm is
the function $\K$ mapping a pair
$(\{ a_n \}, \{ b_n \})$ onto the
sequence $\{ f_n \}$ defined by
\eqref{eqn:cf_2}, \eqref{eqn:cf_3a}
and \eqref{eqn:cf_3b}.
Here the numbers $a_n$ and $b_n$ are
called the $n$th partial numerators
and denominators. A common name is 
element. The number
\[ 
S_n(0) = 
\cfrac{a_1}{b_1 +
\cfrac{a_2}{b_2 +
\cfrac{a_3}{\ddots + 
\cfrac{a_{n}}
  {b_n}
}}} 
\]
is called the $n$th approximate.
Several more convenient ways of writing
the approximants are introduced in the
literature on continued fractions.
We shall here use:
\[
S_n(0) = b_0 + 
\frac{a_1}{b_1}\cadd
\frac{a_2}{b_2}\cadd 
\dots
\frac{a_n}{b_n},
\]
and more generally
\[
S_n(w) = b_0 + 
\frac{a_1}{b_1}\cadd
\frac{a_2}{b_2}\cadd 
\dots
\frac{a_n}{b_n + w}.
\]

Convergence of a continued fraction to 
an extended complex number $f$ means
convergence of $\{ f_n \}$ to $f$, in 
which case we write
\[
f = b_0 + 
\frac{a_1}{b_1}\cadd
\frac{a_2}{b_2}\cadd 
\dots
\frac{a_n}{b_n}
\dots,
\]
or
\[
f=b_0+\K^{\infty}_{n=1}\frac{a_n}{b_n}
\]

%------------------------------------------
%---------- END Definition END ------------
%------------------------------------------

In computing numerically the value of a 
continued fraction the approximants,
in particular those of high order,
are essential. The first ones are:
\[
f_0=b_0,
\hspace{0.5cm}
f_1=\frac{b_0b_1+a_1}{b_1},
\hspace{0.5cm}
f_2=
  \frac{b_0 b_1 b_2 + b_0 a_2 + a_1 b_2}
       {b_1 b_2 + a_2}.
\]
A straightforward computation without cancellation
leads to fractions for the approximants:
\[
f_n=\frac{A_n}{B_n},
\hspace{0.5cm}
n=0,1,2,\dots
\]
If we define 
 \[
  \left[ {\begin{array}{cc}
   A_{-1} \\
   B_{-1} \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   1 \\
   0 \\
  \end{array} } \right],
\hspace{0.5cm}
  \left[ {\begin{array}{cc}
   A_{0} \\
   B_{0} \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   b_0 \\
   0 \\
  \end{array} } \right],
\]
the following is easily proved by induction:

\[
S_n(w) = \frac{A_n + A_{n-1} w}
              {B_n + B_{n-1} w},
\]
\newpage
where the recurrence relation
 \[
  \left[ {\begin{array}{cc}
   A_{n} \\
   B_{n} \\
  \end{array} } \right]
  =
  b_n
  \left[ {\begin{array}{cc}
   A_{n-1} \\
   B_{n-1} \\
  \end{array} } \right]
  +
  a_n
  \left[ {\begin{array}{cc}
   A_{n-2} \\
   B_{n-2} \\
  \end{array} } \right]
\]
for $n=1,2,3, \dots$ holds.
Observe that $S_n$, being a composition
of non-singular linear fractional
transformations
\[
s_k(w) = \frac{a_k}{b_k + w},
\]
is itself a non-singular linear
fractional transformation.
We have in particular
\[
S_n(0) = f_n = \frac{A_n}{B_n},
\hspace{0.5cm}
S_n(\infty) = f_{n-1}=
  \frac{A_{n-1}}{B_{n-1}}
\]
We shall here call $A_n$ and $B_n$,
$n$th canonical numerator and
denominator (sometimes just numerator
and denominator).  An important property
of the numbers $A_n$ and $B_n$ is the
determinate formula
\[
A_n B_{n-1} - A_{n-1}B_{n}=
(-1)^{n-1} \prod_{k=1}^{n} a_k.
\]


%************************************************
%   END             THEORY               END
%************************************************




\end{document}

