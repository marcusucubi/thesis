\documentclass{article}

\usepackage{amsfonts}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{epsfig}
\usepackage{fullpage}
\usepackage{comment}
\usepackage{dcolumn}
\usepackage[yyyymmdd,hhmmss]{datetime}
\usepackage{listings}
\usepackage{color}
\usepackage{xcolor}
\usepackage{amsthm}

\newcommand{\vs}{\vspace{0.2in}}
\newcommand{\vvs}{\vspace{0.1in}}
\newcommand{\aaa}{\alpha}
\newcommand{\bbb}{\beta}
\newcommand{\K}{\operatornamewithlimits{K}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\CC}{\mathbb{\hat{C}}}

\newcommand{\condots}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{\dots}}}
\newcommand{\cadd}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{+}}}
\newcommand{\cminus}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{-}}}
\newcommand{\prf}{\underline{proof}}
\newcommand{\theorem}[1]{\underline{\textbf{#1}}}

\newtheorem{lem}{Lemma}
\newtheorem{thm}{Theorem}
\newtheorem*{cor}{Corollary}
\theoremstyle{definition}
\newtheorem{defn}{Definition}[section]

\newtheorem*{LL}{Little Lemma}
\newtheorem*{BL}{Big Lemma}
\newtheorem*{LC}{Little Corollary}
\newtheorem*{BC}{Big Corollary}
\newtheorem*{bernoulli}{Bernoulli's Theorem}
\newtheorem*{general}{General Contraction Theorem}

\setlength{\parindent}{0cm}
%\renewcommand{\familydefault}{\ttdefault}

\numberwithin{equation}{section}
\newcounter{ctr:theorem}

\begin{document}

\vs


%--------------------------------------------
%--------------------------------------------
%                  SECTION
%--------------------------------------------
%-------------------------------------------
\section{Contractions}
\subsection{Modern theory of continued fractions}

%********************************************
%  ***             THEORY               ***
%********************************************

We can generalize the definition of simple continued fractions by allowing the partial numerator and denominator to take on any complex value.
A general continued fraction is constructed
from two sequences
$\{ a_n \}$ and $\{ b_n \}$ of
complex numbers.
For example, if $n = 3$ then

\[ 
\K^{3}_{n = 1}(a_n/b_n)=
b_0+ \cfrac{a_1}{b_1 +
\cfrac{a_2}{b_2 +
\cfrac{a_3}{b_3}}}
\]

Consider the following sequence
of functions.
\[
\phi_0(w) = b_0+w,
\hspace{0.5cm}
\phi_1(w) = \frac{a_1}{b_1 + w},
\hspace{0.5cm}
\phi_2(w) = \frac{a_2}{b_2 + w},
\hspace{0.5cm}
\phi_3(w) = \frac{a_3}{b_3 + w},
\]

(These functions are called linear fractional
transformations, or M\"obis functions,
in complex analysis, and 
they have many useful properties which we
will mention later.)
\vs

The continued fraction can
be constructed by 
composing the $\phi$'s.
\begin{align*}
\phi_0 \circ \phi_1 \circ
\phi_2 \circ \phi_3(0)
&=
\phi_0 \circ \phi_1 \circ
\phi_2 
\left(
  \frac{a_3}{b_3}
\right) \\
&=
\phi_0 \circ \phi_1 
\left(
  \cfrac{a_2}{b_2 + 
  \cfrac{a_3}{b_3}}
\right) \\
&=
\phi_0 
\left(
  \cfrac{a_1}{b_1 +
  \cfrac{a_2}{b_2 +
  \cfrac{a_3}{b_3}}}
\right)\\
&=
  b_0+
  \cfrac{a_1}{b_1 +
  \cfrac{a_2}{b_2 +
  \cfrac{a_3}{b_3}}}
\end{align*}

We also introduce a new symbol 
$\Phi_n$ to represent the 
composition of $n + 1$ 
transformations 
$\phi_i$; that is,

\[
\Phi_3(0) = 
\phi_0 \circ \phi_1 \circ
\phi_2 \circ \phi_3(0)
=
  b_0+
  \cfrac{a_1}{b_1 +
  \cfrac{a_2}{b_2 +
  \cfrac{a_3}{b_3}}}.
\]
\newpage


Each of functions $\phi_i$ is a linear 
fractional transformation.
A linear fractional transformation has 
the form
\begin{align}
f(z) = \frac{a+bz}{c+dz}
\end{align}
where $z$ is a complex variable, 
and $a, b, c, d$ are 
complex constants 
such that $c + dz \ne 0$
and $ad \ne bc$. 
Linear fractional transformation 
maintain symmetry. 
For example, in the complex plane, a linear fractional transformation takes lines or circles onto lines or circles.
A linear fractional transformation is a
type of transformation that is a 
composition of dilation, translations, inversions, and rotations.
The composition of two linear fractional 
transformations is also a
linear fractional transformation.
Therefore,
\[
\Phi_n = 
\phi_0 \circ \phi_1 \circ \dots \circ \phi_n
\]
is a linear fractional transformation.
\vs

%------------------------------------------
%-------------- Definition ----------------
%------------------------------------------

\begin{defn}
A \textbf{continued fraction} is
an ordered pair
\begin{align} 
\label{eqn:cf_1} 
(( \{ a_n \}, \{ b_n \}), \{ f_n \}),
\end{align}
where $\{ a_n \}_1^{\infty} $ and
$\{ b_n \}^{\infty}_0$ are given sequences 
of complex numbers, $a_n \ne 0$, and where
$\{ f_n \}$ is the sequence of extended complex numbers,
given by 
\begin{align} 
\label{eqn:cf_2} 
f_n = S_n(0), 
\hspace{0.5cm}
n=0,1,2,3,\dots,
\end{align}
where
\begin{align}
\label{eqn:cf_3a} 
S_0(w)=s_0(w),
\hspace{0.5cm}
S_n(w)=S_{n-1}(s_n(w)),
\hspace{0.5cm}
n=1,2,3,\dots,
\end{align}
\begin{align}
\label{eqn:cf_3b} 
s_0(w)=b_0+w,
\hspace{0.5cm}
s_n(w)=\frac{a_n}{b_n+w},
\hspace{0.5cm}
n=1,2,3,\dots,
\end{align}

\end{defn}

The continued fraction algorithm is
the function $\K$ mapping a pair
$(\{ a_n \}, \{ b_n \})$ onto the
sequence $\{ f_n \}$ defined by
\ref{eqn:cf_2}, \ref{eqn:cf_3a}
and \ref{eqn:cf_3b}.
Here the numbers $a_n$ and $b_n$ are
called the $n$th partial numerators
and denominators. A common name is 
element. The number
\[ 
S_n(0) = 
\cfrac{a_1}{b_1 +
\cfrac{a_2}{b_2 +
\cfrac{a_3}{\ddots + 
\cfrac{a_{n}}
  {b_n}
}}} 
\]
is called the $n$th approximate.
Several more convenient ways of writing
the approximants are introduced in the
literature on continued fractions.
We shall here use:
\begin{align}
S_n(0) = b_0 + 
\frac{a_1}{b_1}\cadd
\frac{a_2}{b_2}\cadd 
\dots
\frac{a_n}{b_n},
\end{align}
and more generally
\begin{align}
S_n(w) = b_0 + 
\frac{a_1}{b_1}\cadd
\frac{a_2}{b_2}\cadd 
\dots
\frac{a_n}{b_n + w}.
\end{align}
\newpage

%------------------------------------------
%---------- END Definition END ------------
%------------------------------------------

Convergence of a continued fraction to 
an extended complex number $f$ means
convergence of $\{ f_n \}$ to $f$, in 
which case we write
\begin{align}
f = b_0 + 
\frac{a_1}{b_1}\cadd
\frac{a_2}{b_2}\cadd 
\condots \cadd
\frac{a_n}{b_n} \cadd
\condots,
\end{align}
or
\begin{align}
f=b_0+\K^{\infty}_{n=1}\frac{a_n}{b_n}
\end{align}
\vs

The definition of continued fraction uses two sequences of complex numbers.  We can modify the definition to define a continued fraction segment that starts at $v$ and continues for $\lambda$ elements.

\begin{defn}
 Given sequences $\{ a_i \}$,
 $\{ b_i \} $ and integers $v$ and
 $\lambda$, a continued fraction
 \textbf{segment} is a contuned fraction
 that defined on the subsequence of
 $\{ a_i \}_{i=v}^{\lambda}$ and
 $\{ b_i \}_{i=v}^{\lambda}$.
\end{defn}


%------------------------------------------
%---------- Canonical  Num/Denom ----------
%------------------------------------------
\subsection{The Canonical Numerator
and Denominator}

Define 
\begin{align}
\label{equ:ab_equation}
  \left[ {\begin{array}{cc}
   A_{n} \\
   B_{n} \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   A_{n-2} & A_{n-1}  \\
   B_{n-2} & B_{n-1} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   a_{n} \\
   b_{n} \\
  \end{array} } \right]
\end{align}
with
\begin{align} 
\label{equ:ab_start}
  \left[ {\begin{array}{cc}
   A_{-1} & A_0 \\
   B_{-1} & B_0 \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   1 & b_0 \\
   0 & 1 \\
  \end{array} } \right],
\end{align}
for $n \in \mathbb{N}$.
\vs

Also define
\begin{align}
\Delta_n = 
  \left| {\begin{array}{cc}
   A_{n} & A_{n-1}  \\
   B_{n} & B_{n-1} \\
  \end{array} } \right|
= A_{n} B_{n-1} - A_{n-1} B_{n}.  
\end{align}


\vs
Then we have:

\begin{align}
S_n(w) = \frac{A_n + A_{n-1} w}
              {B_n + B_{n-1} w},
\end{align}
\vs

In particular,
\begin{align}
S_n(0) = f_n = \frac{A_n}{B_n},
\hspace{0.5cm}
S_n(\infty) = f_{n-1}=
  \frac{A_{n-1}}{B_{n-1}}
\end{align}
$A_n$ and $B_n$ are called the
$n$th canonical numerator and
denominator.  
\vs

An important property
of the numbers $A_n$ and $B_n$ is the
determinate formula
\begin{align} \label{equ:determinant}
\Delta_{n}=
A_n B_{n-1} - A_{n-1}B_{n}
=
(-1)^{n-1} \prod_{k=1}^{n} a_k.
\end{align}

For a continued fraction segment,
the numerators and denominators
become
\begin{align} \label{equ:segment}
 \frac{P_{v,\lambda}}{Q_{v,\lambda}}
 =
 b_{\lambda} +
 \frac{a_{\lambda+1}}{b_{\lambda+1}}
 \cadd
 \frac{a_{\lambda+2}}{b_{\lambda+2}}
 \cadd
 \frac{a_{\lambda+3}}{b_{\lambda+3}}
 \cadd
 \condots
 \cadd
 \frac{a_{\lambda+v}}{b_{\lambda+v}}
\end{align}

The determinate formula for
segments is then:
\begin{align}
 P_{n,\lambda} Q_{n-1,\lambda}
 -
 P_{n-1,\lambda} Q_{n,\lambda}
 =
 (-1)^{n+1} a_{\lambda+1}
 a_{\lambda+2} \cdots a_{\lambda+n}.
\end{align}


%----------------------------------------
%--- END  Canonical  Num/Denom   END ----
%----------------------------------------

%----------------------------------
%---------   Convergence   --------
%----------------------------------
\subsection{Convergence}

Recall what convergence 
means for an infinite sum.
If $\{ t_n \}$ be a sequence of complex
numbers, then the following 
series
\[
\sum^{\infty}_{n=1} t_n =
t_1 + t_2 + \cdots + t_n + \cdots
\]
can be consider as
a sequence $T_n$ of
\emph{partial sums}

\[
T_n = \sum^{n}_{k=1} t_k.
\]

We can write this recursively,
\[
T_{n+1} = T_{n} + t_{n+1}.
\]

We define convergence of the series,
to be convergence of the sequence of 
partial sums $\{T_n\}$. 
We denote this by 
\[
\sum^{\infty}_{n=1}t_n = T.
\]
\vs

This general idea of convergence 
can be applied to continued
fractions.
Consider the sequence $\{ f_n \}$
defined by

\[
f_1 = a_1 \text{, }
f_2 = \frac{a_1}{1+a_2}\text{, }
f_3 = \frac{a_1}{
        1+\frac{a_2}{1+a_3}
      },
\]

and generally

\[ 
f_n = 
\cfrac{a_1}{1 +
\cfrac{a_2}{1 +
\cfrac{a_3}{\ddots + 
\cfrac{a_{n-1}}
{1 + a_n}
}}} 
\]

We have constructed a continued
fraction from a sequence in the
same way we constructed a 
series from a sequence.
Just as we thought about the
convergence of the series
in terms of convergence of
the partial sums,
we can also think about convergence
of the partial continued fractions.
We say the continued fraction
converges to a number $L$,
if the series of approximates
converges to $L$.
\vs

%----------------------------------
%------ END Convergence  END ------
%----------------------------------


%********************************************
%   END             THEORY               END
%********************************************

\subsection{Linear Algebra and
Continued Fractions}

We have already seen the connection between continued fractions, the Fibonacci numbers, and the golden ratio.  Recall that we created this connection by first defining approximates using a reoccurrence relation.  Then calculating the canonical numerators and denominators for the simplest continued fraction, the golden ratio, we found the Fibonacci numbers.  The following lemmas will create a new connection between the matrix multiplication algorithms and the calculation of a partial fraction's partial numerators and denominators.

%********************************************
%                  Little Lemma          
%********************************************
\begin{LL}

\begin{align}
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n}
=   
  \left[ {\begin{array}{cc}
   F_{n+1} & F_n  \\
   F_n & F_{n-1}  \\
  \end{array} } \right]
\end{align}
\end{LL}

\begin{proof}
\vs

For our base case we have,
\[
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n}
=   
  \left[ {\begin{array}{cc}
   F_2 & F_1  \\
   F_1 & F_0  \\
  \end{array} } \right]
\]
Then if we assume the result, we have
\begin{align*}
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n+1}
&=
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n}
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]\\
&=
  \left[ {\begin{array}{cc}
   F_{n+1} & F_n  \\
   F_n & F_{n-1}  \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right] \\
&=   
  \left[ {\begin{array}{cc}
   F_{n+1}+F_n & F_{n+1} \\ 
   F_n+F_{n-1} & F_{n}   \\
  \end{array} } \right]  \\
&=   
  \left[ {\begin{array}{cc}
   F_{n+2} & F_{n+1} \\ 
   F_{n+1} & F_{n}   \\
  \end{array} } \right] \\
\end{align*}
By induction we have proved the result.\\
\end{proof}
\vs
%************************************************
%        END       Little Lemma      END    
%************************************************

We know we can write the canonical numerators 
and denominators of the golden ratio as
\[
\phi_{\text{golden}} = 1+ 
\frac{1}{1} \cadd
\frac{1}{1} \cadd
\dots =
\frac{F_{n+2}}{F_{n+1}}.
\]
This shows a fundamental connection 
between continued fractions and
the Fibonacci numbers.
The reader may wonder if we can generalize 
the little lemma. We can.
\vs

%************************************************
%                  Big Lemma          
%************************************************
\begin{BL}

\begin{align}
  \prod_{i=1}^{n}
  \left[ {\begin{array}{cc}
   b_{i-1} & a_i  \\
   1 & 0 \\
  \end{array} } \right]
=   
  \left[ {\begin{array}{cc}
   A_{n} & a_{n+1} A_{n-1}  \\
   B_{n} & a_{n+1} B_{n-1} \\
  \end{array} } \right]
\end{align}
\end{BL}

\begin{proof}

For our base case we have,
\[
  \prod_{i=1}^{2}
  \left[ {\begin{array}{cc}
   b_{i-1} & a_i  \\
   1 & 0 \\
  \end{array} } \right]
=   
  \left[ {\begin{array}{cc}
   b_0 & a_1  \\
   1 & 1 \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b_1 & a_2  \\
   1 & 0 \\
  \end{array} } \right]
=   
  \left[ {\begin{array}{cc}
   b_0 b_1 + a_1 & (a_2) b_0  \\
   b_1 & (a_2) \\
  \end{array} } \right].
=   
  \left[ {\begin{array}{cc}
   A_2 & (a_2) A_1  \\
   B_2 & (a_2) B_1 \\
  \end{array} } \right].
\]

Then if we assume the result, we have
\begin{align*}
  \prod_{i=1}^{n+1}
  \left[ {\begin{array}{cc}
   b_{i-1} & a_i  \\
   1 & 0 \\
  \end{array} } \right]
&=   
  \prod_{i=1}^{n}
  \left[ {\begin{array}{cc}
   b_{i-1} & a_i  \\
   1 & 0 \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b_{n+1} & a_{n+2}  \\
   1 & 0 \\
  \end{array} } \right] \\
&=   
  \left[ {\begin{array}{cc}
   A_n & a_{n+1}A_{n-1}  \\
   B_n & a_{n+1}B_{n-1} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b_{n+1} & a_{n+2}  \\
   1 & 0 \\
  \end{array} } \right] \\
&=   
  \left[ {\begin{array}{cc}
   A_n b_{n+1} + a_{n+1} A_{n-1} & a_{n+2}A_{n}  \\
   B_n b_{n+1} + a_{n+1} B_{n-1} & a_{n+2}B_{n}  \\
  \end{array} } \right] \\
&=   
  \left[ {\begin{array}{cc}
   A_{n+1} & a_{n+2}A_{n}  \\
   B_{n+1} & a_{n+2}B_{n}  \\
  \end{array} } \right]. \\
\end{align*}
By induction we have proved the result.\\
\end{proof}

\vs
%********************************************
%        END    Big Lemma      END    
%********************************************


%*******************************************
%            Little Corollary          
%********************************************
\begin{LC}
\begin{align}
F_{m+n} = 
F_{n+1}F_m + F_n F_{m-1} =
F_n F_{m+1} + F_{n-1} F_m
\end{align}
\end{LC}

\begin{proof}

By the little lemma.
\begin{align*}
  \left[ {\begin{array}{cc}
   1 & 1 \\
   1 & 0 \\
  \end{array} } \right]^{n+m}
=
  \left[ {\begin{array}{cc}
   F_{(m+n)+1} & F_{m+n}   \\
   F_{m+n}   & F_{(m+n)-1} \\
  \end{array} } \right] \\
\end{align*}

By the associativity of matrix multiplication
\begin{align*}
  \left[ {\begin{array}{cc}
   1 & 1 \\
   1 & 0 \\
  \end{array} } \right]^{n+m}
&=
  \left[ {\begin{array}{cc}
   1 & 1 \\
   1 & 0 \\
  \end{array} } \right]^{n}
  \left[ {\begin{array}{cc}
   1 & 1 \\
   1 & 0 \\
  \end{array} } \right]^{m} \\
&=
  \left[ {\begin{array}{cc}
   F_{n+1} & F_{n}   \\
   F_{n}   & F_{n-1} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   F_{m+1} & F_{m}   \\
   F_{m}   & F_{m-1} \\
  \end{array} } \right] \\
&=
  \left[ {\begin{array}{cc}
   F_{n+1} F_{m+1} + F_{n}   F_{m} & 
   F_{n+1} F_{m}   + F_{n}   F_{m-1} \\
   F_{n}   F_{m+1} + F_{n-1} F_{m} & 
   F_{n} F_{m}     + F_{n-1} F_{m-1} \\
  \end{array} } \right] \\
\end{align*}

From the lower left and upper right
matrix elements we have the result.
\end{proof}

%********************************************
%        END    Little Corollary       END    
%********************************************

%*******************************************
%            Big Corollary          
%*******************************************
\begin{BC}
\begin{align} \label{equ:bq1}
P_{n+m,\lambda} 
&=
  P_{n, \lambda} P_{m-1,\lambda+n+1} +
  a_{\lambda+n+1} P_{n-1, \lambda}
  Q_{m-1,\lambda+n+1},
\end{align}
\begin{align} \label{equ:bq2}
Q_{n+m,\lambda}
&=
  Q_{n, \lambda} P_{m-1,\lambda+n+1} +
  a_{\lambda+n+1} Q_{n-1, \lambda}
  Q_{m-1,\lambda+n+1}. 
\end{align}
\end{BC}

\begin{proof}

By the associativity of matrices,
\begin{align}
\prod^{m+n}_{k=0}
  \left( {\begin{array}{cc}
   b_{\lambda +k} &F_{\lambda + k +1}   \\
   1   & 0 \\
  \end{array} } \right) 
=
\prod^{n}_{i=0}
  \left( {\begin{array}{cc}
   b_{\lambda +i} & a_{\lambda + i +1}   \\
   1   & 0 \\
  \end{array} } \right) 
\prod^{m}_{j=0}
  \left( {\begin{array}{cc}
   b_{\lambda +n+j} & a_{\lambda + n+j+1}   \\
   1   & 0 \\
  \end{array} } \right) 
\end{align}

By equations \eqref{equ:segment}
and the Big Lemma we can
write this as

\begin{align*}
  \left( {\begin{array}{cc}
   P_{n+m, \lambda} & a_{\lambda +n+m+1} 
                      P_{n+m-1, \lambda}   \\
   Q_{n+m, \lambda}  & a_{\lambda +n+m+1} 
                       Q_{n+m-1, \lambda} \\
  \end{array} } \right) 
=
  \left( {\begin{array}{cc}
   P_{n, \lambda} & a_{\lambda +n+1} 
                      P_{n-1, \lambda}   \\
   Q_{n, \lambda}  & a_{\lambda +n+1} 
                       Q_{n-1, \lambda} \\
  \end{array} } \right) \\
  \left( {\begin{array}{cc}
   P_{m-1, \lambda+n+1} & 
      a_{\lambda +n+m+1} P_{m-2, \lambda+n+1}   \\
   Q_{m-1, \lambda+n+1}  & 
      a_{\lambda +n+m+1} Q_{m-2, \lambda+n+1} \\
  \end{array} } \right) 
\end{align*}
\vs

Computing the first column of the
matrix product gives the result.
\end{proof}

%************************************************
%        END   Big Corollary     END   
%************************************************
\vs

The partial fraction $d_0+ \K(c_n/d_n)$
is a contraction of $b_0+\K(a_n/b_n)$
if its classical approximants 
$\{ g_n \}$ form a subsequence of the 
classical approximants $\{ f_n \}$
of $b_0 + \K(c_n/d_n)$

\vs
%************************************************
%               Theorem Bernoulli          
%************************************************
\begin{bernoulli}

The sequences 
$\{ A_n \}^{\infty}_{n=-1}$
and
$\{ B_n \}^{\infty}_{n=-1}$
of complex numbers are the 
canonical numerators and
denominators of some continued
fraction $b_0 + \K(a_n/b_n)$
if and only if

\begin{align} \label{equ:bernoulli1}
A_{-1}=B_{0}=1,\,
B_{-1}=0,\,
\Delta_n=A_n B_{n-1} - B_{n}A_{n-1} \ne 0
\end{align}

for all $n \in \mathbb{N}$.
If (4.1.1) holds,
then $b_0+\K(a_n/b_n)$ is
uniquely determined by
\[
b_0 = A_0,\,
b_1 = B_1,\,
a_1 = A_1 - A_0 B_1 \,,
\]
\begin{align} \label{equ:bernoulli2}
a_n=-\frac{\Delta_n}{\Delta_{n-1}},\,
b_n=
\frac
  {A_nB_{n-2} - B_n A_{n-2}}
  {\Delta_{n-1}}
  \hspace{0.2cm} \text{ for } n \ge 2.
\end{align}
\end{bernoulli}

\begin{proof}

Suppose $b_o + \K(a_n/b_n)$ is 
a continued fraction.
Then by the determinant formula
\eqref{equ:determinant} and the initial
conditions \eqref{equ:ab_start},
\eqref{equ:bernoulli1} holds.
\vs

On the other hand, suppose we have sequences 
$\{ A_n \}$ and $\{ B_n \}$ and they 
satisfy equation \eqref{equ:bernoulli1}.
Then $a_n$ and $b_n$ are solutions of the
system 
\begin{align}
b_n A_{n-1} + a_n A_{n-2} = A_n, \hspace{1cm} 
b_n B_{n-1} + a_n B_{n-2} = B_n
\end{align}
of linear equations.
By applying Crammers rule, we get
equation \eqref{equ:bernoulli2}.
Since the determinant of the 
system is $\Delta_{n-1} \ne 0$,
the solution is unique.
\end{proof}

%*******************************************
%         END   Theorem 4.1    END      
%*******************************************

We could use a subsequence of $\{ a_n \}$
and $\{ b_n \}$ which gives the following
corollary.
\vs

%************************************************
%             Corollary Bernoulli             
%************************************************

\begin{cor}

Given two strictly
increasing sequences $\{ n_i\}_{i \ge 0}$
 of integers with $n_0 \ge 0$,
the sequences 
$\{ A_{n_i} \}^{\infty}_{n=-1}$
and
$\{ B_{n_i} \}^{\infty}_{n=-1}$
of complex numbers are the 
canonical numerators and
denominators of some continued
fraction $b_0 + \K(a_{n_i}/b_{n_i})$
if and only if

\begin{align} \label{equ:bernoulli1}
A_{-1}=B_{0}=1,\,
B_{-1}=0,\,
\Delta_{n_i}=A_n B_{n-1} - B_{n_i}A_{{n_i}-1} \ne 0
\end{align}

for all $n \in \mathbb{N}$.
If (4.1.1) holds,
then $b_0+\K(a_n/b_n)$ is
uniquely determined by
\[
b_0 = A_0,\,
b_1 = B_1,\,
a_1 = A_1 - A_0 B_1 \,,
\]
\begin{align} \label{equ:bernoulli2}
a_{n_i}=-\frac{\Delta_{n_i}}{\Delta_{{n_i}-1}},\,
b_{n_i}=
\frac
  {A_{n_i}B_{{n_i}-2} - B_{n_i} A_{{n_i}-2}}
  {\Delta_{{n_i}-1}}
  \hspace{0.2cm} \text{ for } i \ge 2.
\end{align}
\end{cor}
\vs

%***************************************
%   END    Corollary Bernoulli    END
%***************************************

\begin{general}

Let $\{n_i\}$ be a strictly increasing sequence
of integers with $n_0 \ge 0$.
Then
\[ \tag{4.1.1}
\frac{P_{n_i}}{Q_{n_i}}=
\delta_0 + 
\frac{\gamma_1}{\delta_1}\cadd
\frac{\gamma_2}{\delta_2}\cadd
\condots\cadd
\frac{\gamma_i}{\delta_i},
\]
where $\delta_0 = P_{n_0}/Q_{n_0}$,
$\delta_1 = Q_{n_1}$.

\[
\gamma_1=(-1)^{n_0} a_1a_2
\cdots a_{n_{0}+1}
\frac{Q_{n_1-n_0-1,n_0+1} } { Q_{n_0} }
\]
\[
\gamma_2=(-1)^{n_1-n_0 -1} a_{n_0 + 2} a_{n_0 + 3}
\cdots a_{n_{1}+1}
\frac{Q_{n_0} Q_{n_2-n_1-1, n_{1}+1} } 
{ Q_{n_{1}-n_{0}-1, n_0+1 } }
\]
\[
\gamma_2=
\frac{Q_{n_2-n_0-1,n_0+1} } 
     {Q_{n_1-n_0-1,n_0+1} }
\]
and for $v > 2$,
\begin{align*}
\gamma_v=(-1)^{n_{v-1} n_{v-2} -1} 
         &a_{n_{v-2} + 2} 
         a_{n_{v-2} + 3} \cdots
         a_{n_{v-1}+1}
\cdots a_{n_{1}+1} \\
&\cdot \frac{
    Q_{n_{v}-n_{v-1}-1, n_{v-1}+1 }  
  } 
  { 
    Q_{n_{v-1}-n_{v-2}-1, n_{v-2}+1 } 
  }
\end{align*}
and
\[
\delta_v = \frac
  {
    Q_{n_{v} - n_{v-2}-1, n_{v-1}+1 }
  }
  {
    Q_{n_{v-1} - n_{v-2}-1, n_{v-1}+1 }
  }
\]

Moreover, for $i > 0$, $P_{n_i}$ (resp. $Q_{n_i}$)
is the ith classical numerator (resp. denominator)
of the continued fraction in equation (4.1).
This remains true for $i=0$, if the 0th
classical denominator on the right is taken
to be $Q_{n_0}$, instead of 1.
\end{general}

\begin{proof}
Apply equations \eqref{equ:bq1} and
\eqref{equ:bq2} to equation
\eqref{equ:bernoulli2}.
\end{proof}
\vs

\begin{cor}(Even Contraction)

The canonical contraction of 
$b_0 + \K(a_n/b_n)$ with
\[
C_k = A_{2k},
\hspace{0.5cm}
D_k=b_{2k}
\hspace{0.5cm}
\text{for } k=0,1,2,\dots
\]
exists if and only if $b_{2k} \ne 0$
for $k=1,2,3,\dots,$ and is then given by
\[
b_0+
\frac{b_2 a_1}{b_2 b_2 +a_2}\cminus
\frac
  {a_2 a_3 b_4 / b_2}
  {a_4+b_3 b_4 + a_3 b_4/b_2}\cminus
\frac
  {a_2 a_5 a_6 / b_4}
  {a_6+ b_5 b_6 + a_5 b_6 / b_4}\cminus
  \cdots
\]
\end{cor}
\vs

\begin{cor} (Odd Contraction)

The canonical contraction of
$b_0 + \K(a_n/b_n)$ with
$C_0 = A_1/B_1$, $D_0=1$ and
\[
C_k = A_{2k+1},
\hspace{0.5cm}
D_k = B_{2k+1}
\hspace{0.5cm}
\text{ for } k=1,2,3 \cdots 
\]
exists if and only if
$b_{2k+1} \ne 0$ for $k = 0,1,2 \dots$,
and is given by
\[
\frac{b_0 b_1 + a_1}{b_1}-
\frac
  {a_1 a_2 b_2 / b_1}
  {b_1(a_3 + b_2 b_3) + a_2 b_3 }\cminus
\frac
  {a_3 a_4 b_5 b_1/b_3}
  {a_5+b_4 b_5 + a_4 b_5/b_3}\cminus
\frac
  {a_5 a_6 b_7/b_5}
  {a_7 + b_6 b_7+ a_6 b_7/b_5}\cminus
  \cdots
\]
\end{cor}
\vs

\subsection{F-Contraction}
\vs

We have used to even and odd approximates 
to generate the formulas for
the even and odd contractions.
We can also use a Fibonacci
sequence to generate an 
\emph{F-contraction}.
\vs

Lets apply theorem 4.2 to the 
most simple infinite continued
fraction, the golden ratio, to 
generate an F-contraction.
\vs

Recall that the golden ratio has the 
following continued fraction,
\[
\frac{1 + \sqrt{5}}{2}=
1+
\frac{1}{1}\cadd
\frac{1}{1}\cadd 
\frac{1}{1}\cadd 
\frac{1}{1}\cadd 
\dots
\]
and the classical numerator and
denominators become shifted 
Fibonacci's.
\[
P_n = F_{n+2}= \{ 1, 2, 3, 5, \dots \} 
\text{ for } n=0,1,2, \dots
\]
\[
Q_n = F_{n+1}= \{ 1, 1, 2, 3, 5, \dots \} 
\text{ for } n=0,1,2, \dots.
\]
Also since all segments are the same.
i.e. $Q_{\nu,\lambda} = F_{\nu+1}$.
\vs

These observations should make applying 4.2
relatively painless.  
\vs

We can increase this
simplification even more by using the 
Fibonacci sequence for $\{ n_i \}$.
We will call this the
\emph{F-contracted} continued
fraction.
\vs

So using the Golden Ratio lemma
the side of equation 4.1.1 becomes,
\[
\lim_{n \to \infty}
\frac
  {P_{n_i}}
  {Q_{n_i}} = 
\frac
  {F_{F_n+2}}
  {F_{F_n+1}} =
\frac{1+\sqrt{5}}{2}
\]

Furthermore, since $F_n = F_{n-1} + F_{n-2}$,
the differences $n_{\nu} - n_{\nu-1}$ and
$n_{\nu} - n_{\nu-2}$
that occur in the 4.2 will be the
shifted Fibonacci. 
(i.e. $F_{n-2} = F_n - F_{n-1}$ and
$F_{n-1} = F_n - F_{n-2}$)
\vs

Lets compute the first few elements
of the F-contracted continued fraction
for the golden ratio.

\[
\delta_0 +
\frac{\gamma_1}{\delta_1}
\cadd
\frac{\gamma_2}{\delta_2}
\cadd
\frac{\gamma_3}{\delta_3}
\cdots
\]

\[
\delta_0 =
\frac{P_{n_0}}{Q_{n_0}} =
\frac{P_{F_0}}{Q_{F_0}} =
\frac{F_{F_{0}+2}}{F_{F_{0}+1}} =
\frac{F_{F_2}}{F_{F_1}} = 1
\]

\[
\delta_1 =
Q_{n_1} = Q_{F_1}
=F_{F_1 + 1} = F_2 = 1
\]

\begin{align*}
\gamma_1
&=(-1)^{n_0} 
a_1 a_2 \cdots a_{n_0 +1}
\frac
  {Q_{n_1 -n_0 - 1, n_0 +1}}
  {Q_{n_0}} \\
&= (-1)^{n_0} 
\frac
  {Q_{n_1 -n_0 - 1, n_0 +1}}
  {Q_{n_0}} 
  & \text{since } a_i = 1.
  \\
&= (-1)^{F_0} 
\frac
  {Q_{F_1 -F_0 - 1, F_0 +1}}
  {Q_{F_0}} 
  & \text{since } n_i = F_i.
  \\
&=  
\frac
  {Q_{F_1 -F_0 }}
  {Q_{F_0}} 
  & \text{since } Q_{\nu,\lambda}
                  = F_{\nu+1}.
  \\
&=  
\frac
  {Q_{F_{-1}}}
  {Q_{F_0}} 
  & \text{since } F_1 = F_0 + F_{-1}.
  \\
&=  
\frac
  {F_{F_{-1}+1}}
  {F_{F_0+1}} 
  & \text{since } Q_i = F_{i+1}.
  \\
&=  
\frac
  {F_{2}}
  {F_{1}} 
= 1
\end{align*}

\begin{align*}
\gamma_2
&=(-1)^{n_1 - n_0 - 1} 
a_{n_0 +2} a_{n_0 +3} \cdots a_{n_1 +1}
\frac
  {Q_{n_0} Q_{n_2 -n_1 - 1, n_1 +1}}
  {Q_{n_1 -n_0 - 1, n_0 +1}} \\
&=(-1)^{n_1 - n_0 - 1} 
\frac
  {Q_{n_0} Q_{n_2 -n_1 - 1, n_1 +1}}
  {Q_{n_1 -n_0 - 1, n_0 +1}} \\
&=(-1)^{F_1 - F_0 - 1} 
\frac
  {Q_{F_0} Q_{F_2 -F_1 - 1, F_1 +1}}
  {Q_{F_1 -F_0 - 1, F_0 +1}} \\
&=
\frac
  {Q_{F_0} Q_{F_2 -F_1 - 1, F_1 +1}}
  {Q_{F_1 -F_0 - 1, F_0 +1}} \\
&=
\frac
  {Q_{F_2 -F_1 - 1, F_1 +1}}
  {Q_{F_1 -F_0 - 1, F_0 +1}} \\
&=
\frac
  {Q_{F_2 -F_1}}
  {Q_{F_1 -F_0}} 
=
\frac
  {Q_{F_0}}
  {Q_{F_{-1}}} 
=
\frac
  {F_{F_0 +1}}
  {F_{F_{-1} +1}} 
=
\frac
  {F_{1}}
  {F_{2}} 
= 1
\end{align*}


\begin{align*}
\delta_2
&= 
\frac
  {Q_{n_2 -n_0 - 1, n_0 +1}}
  {Q_{n_1 -n_0 - 1, n_0 +1}} \\
&= 
\frac
  {Q_{n_2 -n_0}}
  {Q_{n_1 -n_0}} 
= 
\frac
  {Q_{F_2 -F_0}}
  {Q_{F_1 -F_0}} 
= 
\frac
  {Q_{F_2}}
  {Q_{F_1}} 
= 
\frac
  {F_{F_2 +1}}
  {F_{F_1 +1}} 
= 
\frac
  {F_2}
  {F_2} =1
\end{align*}

\begin{align*}
\gamma_3
&=(-1)^{n_2 - n_1 - 1} 
a_{n_1 +2} a_{n_0 +3} \cdots a_{n_2 +1}
\frac
  {Q_{n_3 -n_2 - 1, n_2 +1}}
  {Q_{n_2 -n_1 - 1, n_1 +1}} \\
&=(-1)^{F_2 - F_1 - 1} 
\frac
  {Q_{F_3 -F_2 - 1, F_2 +1}}
  {Q_{F_2 -F_1 - 1, F_1 +1}} \\
&=(-1) 
\frac
  {Q_{0, F_2 +1}}
  {Q_{-1, F_1 +1}} 
=(-1) 
\frac
  {Q_{1}}
  {Q_{0}} 
=(-1) 
\frac
  {F_{2}}
  {F_{1}} 
= -1
\end{align*}

\begin{align*}
\delta_3
&= 
\frac
  {Q_{n_3 -n_1 - 1, n_1 +1}}
  {Q_{n_2 -n_1 - 1, n_1 +1}} \\
&= 
\frac
  {Q_{n_3 -n_1}}
  {Q_{n_2 -n_1}} 
= 
\frac
  {Q_{F_3 -F_1}}
  {Q_{F_2 -F_1}} 
= 
\frac
  {Q_{1}}
  {Q_{0}} 
= 
\frac
  {F_{2}}
  {F_{1}} 
= 1
\end{align*}

\begin{align*}
\gamma_4
&=(-1)^{n_3 - n_2 - 1} 
a_{n_2 +2} a_{n_1 +3} a_{n_0 +1}
\frac
  {Q_{n_4 -n_3 - 1, n_3 +1}}
  {Q_{n_3 -n_2 - 1, n_2 +1}} \\
&=(-1)^{F_3 - F_2 - 1} 
\frac
  {Q_{F_4 -F_3 - 1, F_3 +1}}
  {Q_{F_3 -F_2 - 1, F_2 +1}} 
= 
\frac
  {Q_{0, 2}}
  {Q_{0, 2}} 
= 1
\end{align*}

\begin{align*}
\delta_4
&= 
\frac
  {Q_{n_4 -n_2 - 1, n_2 +1}}
  {Q_{n_3 -n_2 - 1, n_2 +1}} \\
&= 
\frac
  {Q_{n_4 -n_2}}
  {Q_{n_3 -n_2}} 
= 
\frac
  {Q_{F_4 -F_2}}
  {Q_{F_3 -F_2}} 
= 
\frac
  {Q_{2}}
  {Q_{1}} 
= 
\frac
  {F_{3}}
  {F_{2}} 
= 2
\end{align*}


\[
1 +
\frac{1}{1}
\cadd
\frac{1}{1}
\cminus
\frac{1}{1}
\cadd
\frac{1}{2}
\cdots
\]


\end{document}

