\chapter{Simple Continued Fractions}	% chapter 1
\label{simplechap2}


%********************************************
%  ***             THEORY               ***
%********************************************

A general continued fraction is constructed
from two sequences
$\{ a_n \}$ and $\{ b_n \}$ of
complex numbers.
For example, if $n = 3$ then

\[
\K^{3}_{n = 1}(a_n/b_n)=
b_0+ \cfrac{a_1}{b_1 +
\cfrac{a_2}{b_2 +
\cfrac{a_3}{b_3}}}.
\]

Consider the following sequence
of functions,
\[
\phi_0(w) = b_0+w,
\hspace{0.5cm}
\phi_1(w) = \frac{a_1}{b_1 + w},
\hspace{0.5cm}
\phi_2(w) = \frac{a_2}{b_2 + w},
\hspace{0.5cm}
\phi_3(w) = \frac{a_3}{b_3 + w}.
\]

The continued fraction can
be constructed by
composing the $\phi$'s.
\begin{align*}
\phi_0 \circ \phi_1 \circ
\phi_2 \circ \phi_3(0)
&=
\phi_0 \circ \phi_1 \circ
\phi_2
\left(
  \frac{a_3}{b_3}
\right) \\
&=
\phi_0 \circ \phi_1
\left(
  \cfrac{a_2}{b_2 +
  \cfrac{a_3}{b_3}}
\right) \\
&=
\phi_0
\left(
  \cfrac{a_1}{b_1 +
  \cfrac{a_2}{b_2 +
  \cfrac{a_3}{b_3}}}
\right)\\
&=
  b_0+
  \cfrac{a_1}{b_1 +
  \cfrac{a_2}{b_2 +
  \cfrac{a_3}{b_3}}}
\end{align*}

We also introduce a new symbol
$\Phi_n$ to represent the
composition of $n + 1$
transformations
$\phi_i$; that is,

\[
\Phi_3(0) =
\phi_0 \circ \phi_1 \circ
\phi_2 \circ \phi_3(0)
=
  b_0+
  \cfrac{a_1}{b_1 +
  \cfrac{a_2}{b_2 +
  \cfrac{a_3}{b_3}}}.
\]

%------------------------------------------
%-------------- Definition ----------------
%------------------------------------------

\begin{defn}
A \textbf{continued fraction} is
an ordered pair
\begin{align}
\label{eqn:cf_1}
(( \{ a_n \}, \{ b_n \}), \{ f_n \}),
\end{align}
where $\{ a_n \}_1^{\infty} $ and
$\{ b_n \}^{\infty}_1$ are given sequences
of complex numbers, $a_n \ne 0$, and where
$\{ f_n \}$ is the sequence of extended complex numbers,
given by
\begin{align}
\label{eqn:cf_2}
f_n = S_n(0),
\hspace{0.5cm}
n=0,1,2,3,\dots,
\end{align}
where
\begin{align}
\label{eqn:cf_3a}
S_0(w)=s_0(w),
\hspace{0.5cm}
S_n(w)=S_{n-1}(s_n(w)),
\hspace{0.5cm}
n=1,2,3,\dots,
\end{align}
\begin{align}
\label{eqn:cf_3b}
s_0(w)=b_0+w,
\hspace{0.5cm}
s_n(w)=\frac{a_n}{b_n+w},
\hspace{0.5cm}
n=1,2,3,\dots,
\end{align}

\end{defn}

The continued fraction algorithm is
the function $\K$ mapping a pair
$(\{ a_n \}, \{ b_n \})$ onto the
sequence $\{ f_n \}$ defined by
\eqref{eqn:cf_2}, \eqref{eqn:cf_3a}
and \eqref{eqn:cf_3b}.
Here the numbers $a_n$ and $b_n$ are
called the $n$th partial numerators
and denominators. A common name is
element.

\begin{defn}
 The \textbf{$n$th approximate} is given by
\[
S_n(0) =
\cfrac{a_1}{b_1 +
\cfrac{a_2}{b_2 +
\cfrac{a_3}{\ddots +
\cfrac{a_{n}}{b_n}
}}}
\]
\end{defn}

The following notation will
also be used:
\begin{align}
S_n(0) = b_0 +
\frac{a_1}{b_1}\cadd
\frac{a_2}{b_2}\cadd
\dots\cadd
\frac{a_n}{b_n},
\end{align}

%------------------------------------------
%---------- END Definition END ------------
%------------------------------------------

Convergence of a continued fraction to
an extended complex number $f$ means
convergence of $\{ f_n \}$ to $f$, in
which case we write
\begin{align}
f = b_0 +
\frac{a_1}{b_1}\cadd
\frac{a_2}{b_2}\cadd
\condots \cadd
\frac{a_n}{b_n} \cadd
\condots,
\end{align}
or
\begin{align}
f=b_0+\K^{\infty}_{n=1}\frac{a_n}{b_n}
\end{align}

The first few approximants are:
\begin{align} \label{equ:firstfs}
 f_0 = b_0 \,,
 \hspace{0.5cm}
 f_1 = \frac{b_0 b_1 + a_1}{b_1}\, ,
 \hspace{0.5cm}
 f_2 = \frac
   {b_0 b_1 b_2 + b_0 a_2 + a_1 b_2}
   {b_1 b_2 + a_2}
\end{align}

%------------------------------
%--- Canonical  Num/Denom -----
%------------------------------
\subsection{The Canonical Numerator
and Denominator}

\begin{defn}
 The \textbf{Canonical Numerator} $A_n$ and
 \textbf{Canonical Denominator} $B_n$ are
 defined as follows
\begin{align}
\label{equ:ab_equation}
  \left[ {\begin{array}{cc}
   A_{n} \\
   B_{n} \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   A_{n-2} & A_{n-1}  \\
   B_{n-2} & B_{n-1} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   a_{n} \\
   b_{n} \\
  \end{array} } \right]
\end{align}
with
\begin{align}
\label{equ:ab_start}
  \left[ {\begin{array}{cc}
   A_{-1} & A_0 \\
   B_{-1} & B_0 \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   1 & b_0 \\
   0 & 1 \\
  \end{array} } \right],
\end{align}
for $n \in \mathbb{N}$.
\end{defn}

Equation \eqref{equ:ab_equation}
allows us to move from
the continued fraction
to the approximants.
\vs
\begin{center}
\begin{tikzcd}
  [column sep=large,
   row sep=large]
  b_n + K(a_n/b_n)
    \arrow[r, "\eqref{equ:ab_equation}"]
    &
  \frac{A_n}{B_n}
\end{tikzcd}
\end{center}
\vs


\begin{defn}
The \textbf{determinate formula}
is defined as follows
\begin{align} \label{equ:deltadef}
\Delta_n =
  \left| {\begin{array}{cc}
   A_{n} & A_{n-1}  \\
   B_{n} & B_{n-1} \\
  \end{array} } \right|
= A_{n} B_{n-1} - A_{n-1} B_{n}.
\end{align}
\end{defn}

Then we have:
\begin{align} \label{equ:defab}
S_n(0) = f_n = \frac{A_n}{B_n}
=
  \frac{a_1}{b_1}\cadd
\cdots\cadd
  \frac{a_n}{b_n}.
\end{align}
The determinate formula has
the following value.
\begin{align} \label{equ:determinant}
\Delta_{n}=
A_n B_{n-1} - A_{n-1}B_{n}
=
(-1)^{n-1} \prod_{k=1}^{n} a_k.
\end{align}

For a continued fraction segment,
the numerators and denominators
become
\begin{align} \label{equ:segment}
 \frac{A_{v,\lambda}}{B_{v,\lambda}}
 =
 b_{\lambda} +
 \frac{a_{\lambda+1}}{b_{\lambda+1}}
 \cadd
 \frac{a_{\lambda+2}}{b_{\lambda+2}}
 \cadd
 \frac{a_{\lambda+3}}{b_{\lambda+3}}
 \cadd
 \condots
 \cadd
 \frac{a_{\lambda+v}}{b_{\lambda+v}}
\end{align}

The determinate formula for
segments is then:
\begin{align}
 A_{n,\lambda} B_{n-1,\lambda}
 -
 A_{n-1,\lambda} B_{n,\lambda}
 =
 (-1)^{n+1} a_{\lambda+1}
 a_{\lambda+2} \cdots a_{\lambda+n}.
\end{align}


%------------------------------------
%- END  Canonical  Num/Denom   END --
%------------------------------------

%------------------------------------
%------ Fibonacci Sequence ----------
%------------------------------------
\subsection{Fibonacci Sequence}

\begin{defn}
 The \textbf{Fibonacci Sequence} $(F_n)$ is defined
 by the recurrence relation
\begin{align*}
F_0 = 1, \hspace{0.3cm}
F_1 = 1,
\end{align*}
and
\begin{align*}
F_n = F_{n-1} + F_{n-2}.
\end{align*}
\end{defn}
for $n > 1$.

We will use $\phi^{golden}$ to
represent the golden ratio and
$K^{golden}$ to represent
the corresponding continued
fraction,
\begin{align} \label{equ:goldenK}
 K^{golden} =
 1 + \frac{1}{1}
 \cadd \frac{1}{1}
 \cadd \frac{1}{1}
 \condots \hspace{0.4cm}
 \text{ and } \hspace{0.4cm}
 K^{golden}_n =
 1 +
 \underbrace{
   \frac{1}{1}
   \cadd \frac{1}{1}
   \cadd \condots
   \cadd \frac{1}{1}
 }_{n \, \text{times}}
\end{align}

Using \eqref{equ:ab_equation} and
\eqref{equ:ab_start} we find that
\begin{align} \label{equ:goldenfibi}
  K^{golden}_n=
  \frac{A_n}{B_n} =
  \frac{F_{n+2}}{F_{n+1}}
\end{align}

The Fibonacci numbers have the following
closed-form expression,
\begin{align} \label{equ:fibialpha}
 F_n =
 \frac{\alpha^n - \beta^n}
      {\alpha - \beta}
 \hspace{0.4cm}
 \text{ where }
 \alpha = \frac{1+\sqrt{5}}{2},
 \hspace{0.4cm}
 \beta = \frac{1-\sqrt{5}}{2}.
\end{align}

%************************************
%   END       THEORY            END
%************************************
