
\chapter{contractions}
\label{contractionschap}

%--------------------------------------------
%--------------------------------------------
%                  SECTION
%--------------------------------------------
%-------------------------------------------
\section{Contractions of
Continued Fractions}

\subsection{Linear Algebra and
Continued Fractions}

We have already seen the connection between continued fractions, the Fibonacci sequence, and the golden ratio.  Recall that we created this connection by first defining approximates using a reoccurrence relation.
Then we found the Fibonacci numbers by calculating the canonical numerators and denominators for $K^{golden}$.
The following lemmas will create a new connection between matrix multiplication and the calculation of a canonical numerators and denominators.

%********************************************
%                  Little Lemma
%********************************************
\begin{LL}

\begin{align}
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n}
=
  \left[ {\begin{array}{cc}
   F_{n+1} & F_n  \\
   F_n & F_{n-1}  \\
  \end{array} } \right]
\end{align}
\end{LL}

\begin{proof}
\vs

For our base case we have,
\[
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n}
=
  \left[ {\begin{array}{cc}
   F_2 & F_1  \\
   F_1 & F_0  \\
  \end{array} } \right]
\]
Then if we assume the result, we have
\begin{align*}
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n+1}
&=
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n}
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]\\
&=
  \left[ {\begin{array}{cc}
   F_{n+1} & F_n  \\
   F_n & F_{n-1}  \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right] \\
&=
  \left[ {\begin{array}{cc}
   F_{n+1}+F_n & F_{n+1} \\
   F_n+F_{n-1} & F_{n}   \\
  \end{array} } \right]  \\
&=
  \left[ {\begin{array}{cc}
   F_{n+2} & F_{n+1} \\
   F_{n+1} & F_{n}   \\
  \end{array} } \right] \\
\end{align*}
By induction we have proved the result.\\
\end{proof}
\vs
%************************************************
%        END       Little Lemma      END
%************************************************

We know we can write the canonical numerators
and denominators of the golden ratio as
\[
\phi_{\text{golden}} = 1+
\frac{1}{1} \cadd
\frac{1}{1} \cadd
\dots =
\frac{F_{n+2}}{F_{n+1}}.
\]
This shows a fundamental connection
between continued fractions and
the Fibonacci numbers.
The reader may wonder if we can generalize
the little lemma. We can.

%************************************************
%                  Big Lemma
%************************************************
\begin{BL}

\begin{align}
  \prod_{i=1}^{n+1}
  \left[ {\begin{array}{cc}
   b_{i-1} & a_i  \\
   1 & 0 \\
  \end{array} } \right]
=
  \left[ {\begin{array}{cc}
   A_{n} & a_{n+1} A_{n-1}  \\
   B_{n} & a_{n+1} B_{n-1} \\
  \end{array} } \right]
\end{align}
\end{BL}

\begin{proof}

For our base case we have,
\[
  \prod_{i=1}^{2}
  \left[ {\begin{array}{cc}
   b_{i-1} & a_i  \\
   1 & 0 \\
  \end{array} } \right]
=
  \left[ {\begin{array}{cc}
   b_0 & a_1  \\
   1 & 0 \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b_1 & a_2  \\
   1 & 0 \\
  \end{array} } \right]
=
  \left[ {\begin{array}{cc}
   b_0 b_1 + a_1 & (a_2) b_0  \\
   b_1 & (a_2) \\
  \end{array} } \right].
=
  \left[ {\begin{array}{cc}
   A_1 & (a_2) A_0  \\
   B_1 & (a_2) B_0 \\
  \end{array} } \right].
\]

Then if we assume the result, we have
\begin{align*}
  \prod_{i=1}^{n+1}
  \left[ {\begin{array}{cc}
   b_{i-1} & a_i  \\
   1 & 0 \\
  \end{array} } \right]
&=
  \prod_{i=1}^{n}
  \left[ {\begin{array}{cc}
   b_{i-1} & a_i  \\
   1 & 0 \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b_{n+1} & a_{n+2}  \\
   1 & 0 \\
  \end{array} } \right] \\
&=
  \left[ {\begin{array}{cc}
   A_n & a_{n+1}A_{n-1}  \\
   B_n & a_{n+1}B_{n-1} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b_{n+1} & a_{n+2}  \\
   1 & 0 \\
  \end{array} } \right] \\
&=
  \left[ {\begin{array}{cc}
   A_n b_{n+1} + a_{n+1} A_{n-1} & a_{n+2}A_{n}  \\
   B_n b_{n+1} + a_{n+1} B_{n-1} & a_{n+2}B_{n}  \\
  \end{array} } \right] \\
&=
  \left[ {\begin{array}{cc}
   A_{n+1} & a_{n+2}A_{n}  \\
   B_{n+1} & a_{n+2}B_{n}  \\
  \end{array} } \right]. \\
\end{align*}
By induction we have proved the result.\\
\end{proof}

%********************************************
%        END    Big Lemma      END
%********************************************

%*******************************************
%            Little Corollary
%********************************************
\begin{LC}
\begin{align}
F_{m+n} =
F_{n+1}F_m + F_n F_{m-1} =
F_n F_{m+1} + F_{n-1} F_m
\end{align}
\end{LC}

\begin{proof}

By the little lemma.
\begin{align*}
  \left[ {\begin{array}{cc}
   1 & 1 \\
   1 & 0 \\
  \end{array} } \right]^{n+m}
=
  \left[ {\begin{array}{cc}
   F_{(m+n)+1} & F_{m+n}   \\
   F_{m+n}   & F_{(m+n)-1} \\
  \end{array} } \right] \\
\end{align*}

By the associativity of matrix multiplication
\begin{align*}
  \left[ {\begin{array}{cc}
   1 & 1 \\
   1 & 0 \\
  \end{array} } \right]^{n+m}
&=
  \left[ {\begin{array}{cc}
   1 & 1 \\
   1 & 0 \\
  \end{array} } \right]^{n}
  \left[ {\begin{array}{cc}
   1 & 1 \\
   1 & 0 \\
  \end{array} } \right]^{m} \\
&=
  \left[ {\begin{array}{cc}
   F_{n+1} & F_{n}   \\
   F_{n}   & F_{n-1} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   F_{m+1} & F_{m}   \\
   F_{m}   & F_{m-1} \\
  \end{array} } \right] \\
&=
  \left[ {\begin{array}{cc}
   F_{n+1} F_{m+1} + F_{n}   F_{m} &
   F_{n+1} F_{m}   + F_{n}   F_{m-1} \\
   F_{n}   F_{m+1} + F_{n-1} F_{m} &
   F_{n} F_{m}     + F_{n-1} F_{m-1} \\
  \end{array} } \right] \\
\end{align*}

From the lower left and upper right
matrix elements we have the result.
\end{proof}

%********************************************
%        END    Little Corollary       END
%********************************************

%*******************************************
%            Big Corollary
%*******************************************
\begin{BC}
\begin{align} \label{equ:bq1}
P_{n+m,\lambda}
&=
  P_{n, \lambda} P_{m-1,\lambda+n+1} +
  a_{\lambda+n+1} P_{n-1, \lambda}
  Q_{m-1,\lambda+n+1},
\end{align}
\begin{align} \label{equ:bq2}
Q_{n+m,\lambda}
&=
  Q_{n, \lambda} P_{m-1,\lambda+n+1} +
  a_{\lambda+n+1} Q_{n-1, \lambda}
  Q_{m-1,\lambda+n+1}.
\end{align}
\end{BC}

\begin{proof}

By the associativity of matrices,
\begin{align}
\prod^{m+n}_{k=0}
  \left( {\begin{array}{cc}
   b_{\lambda +k} &F_{\lambda + k +1}   \\
   1   & 0 \\
  \end{array} } \right)
=
\prod^{n}_{i=0}
  \left( {\begin{array}{cc}
   b_{\lambda +i} & a_{\lambda + i +1}   \\
   1   & 0 \\
  \end{array} } \right)
\prod^{m}_{j=0}
  \left( {\begin{array}{cc}
   b_{\lambda +n+j} & a_{\lambda + n+j+1}   \\
   1   & 0 \\
  \end{array} } \right)
\end{align}

By equations \eqref{equ:segment}
and the Big Lemma we can
write this as

\begin{align*}
  \left( {\begin{array}{cc}
   P_{n+m, \lambda} & a_{\lambda +n+m+1}
                      P_{n+m-1, \lambda}   \\
   Q_{n+m, \lambda}  & a_{\lambda +n+m+1}
                       Q_{n+m-1, \lambda} \\
  \end{array} } \right)
=
  \left( {\begin{array}{cc}
   P_{n, \lambda} & a_{\lambda +n+1}
                      P_{n-1, \lambda}   \\
   Q_{n, \lambda}  & a_{\lambda +n+1}
                       Q_{n-1, \lambda} \\
  \end{array} } \right) \\
  \left( {\begin{array}{cc}
   P_{m-1, \lambda+n+1} &
      a_{\lambda +n+m+1} P_{m-2, \lambda+n+1}   \\
   Q_{m-1, \lambda+n+1}  &
      a_{\lambda +n+m+1} Q_{m-2, \lambda+n+1} \\
  \end{array} } \right)
\end{align*}
\vs

Computing the first column of the
matrix product gives the result.
\end{proof}

%*******************************
%   END   Big Corollary     END
%*******************************

\begin{thm}
\begin{align} \label{equ:seg_det}
 P_{n,\lambda} Q_{n+m,\lambda}
 -
 P_{n+m, \lambda} Q_{n, \lambda}
 =
 (-1)^{n+1}
 a_{\lambda+1} \cdots
 a_{\lambda+n+1}
 Q_{m-1,\lambda+n+1}
\end{align}
\begin{proof}
 Multiply \eqref{equ:bq1}
 by $Q_{n,\lambda}$
 and \eqref{equ:bq2} by
 $P_{n,\lambda}$.
 Take the difference between
 the resulting equations.
 Use the determinate formula
 for seqments to get the result.
\end{proof}

\end{thm}


Equation \eqref{equ:ab_equation}
allows us to move from
the continued fraction
to the approximants.
\begin{center}
\begin{tikzcd}
  [column sep=large,
   row sep=large]
  b_n + K(a_n/b_n)
    \arrow[r, "\eqref{equ:ab_equation}"]
    &
  \frac{A_n}{B_n}
\end{tikzcd}
\end{center}

The following theorem
(Bernoulli's Theorem)
will allow us to move back.

\begin{center}
\begin{tikzcd}
  [column sep=large,
   row sep=large]
  b_n + K(a_n/b_n)
    &
  \frac{A_n}{B_n}
    \arrow[l, "Bernoulli"]
\end{tikzcd}
\end{center}

%*********************************
%        Theorem Bernoulli
%*********************************
\begin{bernoulli}

The sequences
$\{ A_n \}^{\infty}_{n=-1}$
and
$\{ B_n \}^{\infty}_{n=-1}$
of complex numbers are the
canonical numerators and
denominators of some continued
fraction $b_0 + \K(a_n/b_n)$
if and only if

\begin{align} \label{equ:def1}
A_{-1}=B_{0}=1,\,
B_{-1}=0,\,
\Delta_n=A_n B_{n-1} - B_{n}A_{n-1} \ne 0
\end{align}

for all $n \in \mathbb{N}$.
If \eqref{equ:def1} holds,
then $b_0+\K(a_n/b_n)$ is
uniquely determined by
\[
b_0 = A_0,\,
b_1 = B_1,\,
a_1 = A_1 - A_0 B_1 \,,
\]
\begin{align} \label{equ:def2}
a_n=-\frac{\Delta_n}{\Delta_{n-1}},\,
b_n=
\frac
  {A_nB_{n-2} - B_n A_{n-2}}
  {\Delta_{n-1}}
  \hspace{0.2cm} \text{ for } n \ge 2.
\end{align}
\end{bernoulli}

\begin{proof}

Suppose $b_o + \K(a_n/b_n)$ is
a continued fraction.
Then by the determinant formula
\eqref{equ:determinant} and the initial
conditions \eqref{equ:ab_start},
\eqref{equ:def1} holds.
\vs

On the other hand, suppose we have sequences
$\{ A_n \}$ and $\{ B_n \}$ and they
satisfy equation \eqref{equ:def1}.
Then $a_n$ and $b_n$ are solutions of the
system
\begin{align}
b_n A_{n-1} + a_n A_{n-2} = A_n, \hspace{1cm}
b_n B_{n-1} + a_n B_{n-2} = B_n
\end{align}
of linear equations.
By applying Crammers rule, we get
equation \eqref{equ:def2}.
Since the determinant of the
system is $\Delta_{n-1} \ne 0$,
the solution is unique.
\end{proof}

%*********************************
%    END   Theorem     END
%*********************************

\begin{defn}
The partial fraction $d_0+ \K(c_n/d_n)$
is a \textbf{contraction} of
$b_0+\K(a_n/b_n)$
if its classical approximants
$\{ g_n \}$ form a subsequence of the
classical approximants $\{ f_n \}$
of $b_0 + \K(c_n/d_n)$.
\end{defn}

\begin{center}
\begin{tikzcd}
  [column sep=large,
   row sep=large]
  b_n + K(a_n/b_n)
    \arrow[r, "contraction"]
    \arrow[d, "approximate"]
    &
  d_0 + K(c_n/d_n)
    \arrow[d, "approximate"]
  \\
  \{ f_n \} = \frac{A_n}{B_n}
    \arrow[r, "subsequence"]
    \arrow[u, "Bernoulli"]
    &
  \{ g_n \} = \frac{C_n}{D_n}
    \arrow[u, "Bernoulli"]
\end{tikzcd}
\end{center}
\vs

The next theorem defines
the elements of a
contracted continued fraction
in terms of the original
continued fraction.
The stratagy for the following
proof is to
start with an object
in the contracted continued
fraction
$d_0 + K(c_n/d_n)$.
For example $d_0$.
Second, move it down
to the approximants
(using equation \eqref{equ:def2}).
For example $d_0 = D_0$.
Third, move it to the left
using the subsequence
formula.
In this case, the even
elements of the sequence.
For example $D_0 = B_0$.
Finally, move it up to
the original continued
fraction,
using Bernoulli's theorem.
For example $B_0 = b_0$.



%*********************************
%        Even Contraction
%*********************************
\begin{thm}(Even Contraction) \label{cor:even}

The canonical contraction of
$b_0 + \K(a_n/b_n)$ with
\begin{align}
C_k = A_{2k}, \label{equ:evendef}
\hspace{0.5cm}
D_k=B_{2k}
\hspace{0.5cm}
\text{for } k=0,1,2,\dots
\end{align}
exists if and only if $b_{2k} \ne 0$
for $k=1,2,3,\dots,$ and is then given by
\[
b_0+
\frac{b_2 a_1}{b_2 b_1 +a_2}\cminus
\frac
  {a_2 a_3 b_4 / b_2}
  {a_4+b_3 b_4 + a_3 b_4/b_2}\cminus
\frac
  {a_4 a_5 a_6 / b_4}
  {a_6+ b_5 b_6 + a_5 b_6 / b_4}\cminus
  \cdots
\]
\end{thm}

\begin{proof}
We have
\begin{align*}
d_0
 &= D_0 = B_0 = b_0
 && \text{(as desired)}\\
c_1
 &= C_1 - C_0 D_1
    &&
    \text{ equation }
    \eqref{equ:def2} \\
 &=
 A_2 - A_0 B_2
    &&
    \text{ equation }
    \eqref{equ:evendef} \\
 &=
 (b_0 b_1 b_2 + b_0 a_2 + a_1 b_2)
 - (b_0) (b_1 b_2 + a_2)
    &&
    \text{ equation }
    \eqref{equ:firstfs} \\
 &= b_2 a_1
    &&
    \text{ (as desired) }\\
d_1
 &= D_1
    &&
    \text{ equation }
    \eqref{equ:def2} \\
 &=
 B_2
    &&
    \text{ equation }
    \eqref{equ:evendef} \\
 &=
 b_2 b_1 + a_2
    &&
    \text{ equation }
    \eqref{equ:firstfs}
    \text{ (as desired) }\\
\end{align*}

$c_n = -\Delta_n/\Delta_{n-1}$,
where
\begin{align*}
 \Delta_n
 &=
 C_n D_{n-1} - D_n C_{n-1}
 =
 A_{2n} B_{2n-2} - B_{2n} A_{2n-2} \\
 &=
 -B^{(2n-1)}_1
 \prod^{2n-1}_{j=1}(-a_i)
 =
 -b_{2n}
 \prod^{2n-1}_{j=1}
 (-a_j).
\end{align*}

Finally, we have $d_n = P_n/\Delta_{n-1}$,
where
\begin{align*}
 P_n
 &=
 C_n D_{n-2} - D_{n} C_{n-2}
 =
 A_{2n} B_{2n-4} - B_{2n} A_{2n-4} \\
 &=
 -B_{3}^{(2n-3)}
 \prod_{j=1}^{2n-3} (-a_j) \\
 &=
 -[b_{2n}(b_{2n-1} b_{2n-2}
 + a_{2n-1}) +
 a_{2n} b_{2n-2}]
 \prod_{j=1}^{2n-3}(-a_j).
\end{align*}
\end{proof}
%*********************************
%  END   Even Contraction    END
%*********************************

The next theorem
provides a general formula
for the
contracted continued fraction
$d_0 + K(c_n/d_n)$ in terms of the orginal continued fraction
$b_0 + K(a_n/b_n)$.
Since the contraction may
not start at the
first approximate,
the contraction is
a segment.


\begin{general} \label{thm:general}

Let $\{n_k\}$ be a strictly increasing sequence
of integers with $n_0 \ge 0$.

The canonical contraction of
$b_0 + \K(a_n/b_n)$ with
\begin{align}
C_k = A_{n_k}, \label{equ:subdef}
\hspace{0.5cm}
D_k=B_{n_k}
\hspace{0.5cm}
\text{for } k=0,1,2,\dots
\end{align}
exists if and only if
$b_{n_k} \ne 0$
for $k=1,2,3,\dots,$ and is then given by

\begin{align}
\frac{C_k}{D_k}=
\frac{A_{n_k}}{B_{n_k}}=
\delta_0 +
\frac{\gamma_1}{\delta_1}\cadd
\frac{\gamma_2}{\delta_2}\cadd
\condots\cadd
\frac{\gamma_k}{\delta_k},
\end{align}
where $\delta_0 = A_{n_0}/B_{n_0}$,
$\delta_1 = B_{n_1}$.

\[
\gamma_1=(-1)^{n_0} a_1 a_2
\cdots a_{n_{0}+1}
\frac{B_{n_1-n_0-1,n_0+1} }
     {B_{n_0} }
\]
\[
\gamma_2=(-1)^{n_1-n_0 -1}
         a_{n_0 + 2} a_{n_0 + 3}
\cdots a_{n_{1}+1}
\frac{B_{n_0}
      B_{n_2-n_1-1, n_{1}+1} }
      { B_{n_{1}-n_{0}-1, n_0+1 } }
\]
\[
\delta_2=
\frac{B_{n_2-n_0-1,n_0+1} }
     {B_{n_1-n_0-1,n_0+1} }
\]
and for $v > 2$,
\begin{align*}
\gamma_v=(-1)^{n_{v-1} n_{v-2} -1}
         &a_{n_{v-2} + 2}
         a_{n_{v-2} + 3} \cdots
         a_{n_{v-1}+1} \\
&\cdot \frac{
    B_{n_{v}-n_{v-1}-1, n_{v-1}+1 }
  }
  {
    B_{n_{v-1}-n_{v-2}-1, n_{v-2}+1 }
  }
\end{align*}
and
\[
\delta_v = \frac
  {
    B_{n_{v} - n_{v-2}-1, n_{v-2}+1 }
  }
  {
    B_{n_{v-1} - n_{v-2}-1, n_{v-2}+1 }
  }
\]

Moreover, for $i > 0$, $A_{n_i}$ (resp. $B_{n_i}$)
is the ith classical numerator (resp. denominator)
of the continued fraction in equation (4.1).
This remains true for $i=0$, if the 0th
classical denominator on the right is taken
to be $B_{n_0}$, instead of 1.
\end{general}

\begin{proof}
TODO

$c_k = -\Delta_k/\Delta_{k-1}$,
where
\begin{align*}
 \Delta_k
 &=
 C_k D_{k-1} - D_k C_{k-1}
 =
 A_{n_k} B_{n_k-1} -
 B_{n_k-1} A_{n_k} \\
 &=
 (-1)^{n+1}
 a_{\lambda+1} \cdots
 a_{\lambda+n+1}
 B_{m-1,\lambda+n+1}
 & \text{ equation } \eqref{equ:seg_det} \\
 \Delta_{k-1}
 &=
 C_k D_{k-1} - D_k C_{k-1}
 =
 A_{n_k} B_{n_k-1} -
 B_{n_k-1} A_{n_k} \\
 &=
 (-1)^{n}
 a_{\lambda+1} \cdots
 a_{\lambda+n+1}
 B_{m-1,\lambda+n+1}
 & \text{ equation } \eqref{equ:seg_det}
\end{align*}


\end{proof}

\begin{cor} (Fibinacci Contraction)
 Let $n_0 = m_0 \ge 0$ and for
 $v \ge 1$ put
 $n_v = m_0 + m_1 + \cdots + m_v$,
 where $m_i \in \mathbb{Z}^{+}$
 for $i \ge 1$.
 Then for $i \ge 1$,
\begin{align*}
 \frac{(-1)^{m_{1}-1} F_{m_0}
        F_{n_{i+1} - m_0 - m_1}}
      {F_{n_{i+1}-m_0}}
&=
\frac{(-1)^{m_1 -1} F_{m_0} F_{m_2} }
     { F_{m_1 + m_2}} \\
&\cadd
 \frac{(-1)^{m_2 -1} F_{m_1} F_{m_3} }
     { F_{m_2 + m_3} }
\cadd
\condots
\cadd
\frac{(-1)^{m_i -1} F_{m_{i-1}} F_{m_i +1}}
     {F_{m_i + m_{i+1}} }
\end{align*}

\end{cor}
