\documentclass{article}

\usepackage{amsfonts}
\usepackage{amscd}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{epsfig}
\usepackage{fullpage}
\usepackage{comment}
\usepackage{dcolumn}
\usepackage[yyyymmdd,hhmmss]{datetime}
\usepackage{listings}
\usepackage{color}
\usepackage{xcolor}
\usepackage{amsthm}
\usepackage{tikz-cd}

\newcommand{\vs}{\vspace{0.2in}}
\newcommand{\vvs}{\vspace{0.1in}}
\newcommand{\aaa}{\alpha}
\newcommand{\bbb}{\beta}
\newcommand{\K}{\operatornamewithlimits{K}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\CC}{\mathbb{\hat{C}}}

\newcommand{\condots}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{\dots}}}
\newcommand{\cadd}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{+}}}
\newcommand{\cminus}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{-}}}
\newcommand{\prf}{\underline{proof}}
\newcommand{\theorem}[1]{\underline{\textbf{#1}}}

\newtheorem{lem}{Lemma}
\newtheorem{thm}{Theorem}
\newtheorem{cor}{Corollary}
\theoremstyle{definition}
\newtheorem{defn}{Definition}[section]

\newtheorem*{LL}{Little Lemma}
\newtheorem*{BL}{Big Lemma}
\newtheorem*{LC}{Little Corollary}
\newtheorem*{BC}{Big Corollary}
\newtheorem*{bernoulli}{Bernoulli's Theorem}
\newtheorem*{general}{General Contraction Theorem}
\newtheorem*{IRF}{Index-Reduction Formula}
\newtheorem*{absorption}{Absorption Lemma}
\newtheorem*{golden}{Golden Ratio Lemma}
\newtheorem*{transformation}{Transformation Lemma}

\setlength{\parindent}{0cm}

\numberwithin{equation}{section}
\newcounter{ctr:theorem}

\begin{document}

\begin{titlepage}
    \begin{center}
        \vspace*{1cm}

        \LARGE
        \textbf{Fibonacci Contractions of Continued Fractions}

        \vspace{0.5cm}
        \LARGE

        \vspace{1.5cm}

        \textbf{Nick Davis}

        \vfill

        A thesis presented for the degree of\\
        Master of Science in mathematics

        \vspace{0.8cm}


       \Large
        Department of Mathematical Sciences\\
        Northern Illinois University\\
        United States\\
        May 8, 2023

    \end{center}
\end{titlepage}

\tableofcontents
\newpage

\section{Introduction}

If a sequence converges, all subsequences converge, and all convergent subsequences converge to the same limit. General continued fractions can produce a sequence of complex numbers called the approximates of the continued fraction. If the sequence of approximates converges to a limit, the continued fraction is said to converge to the limit. This paper will use this knowledge to generate new sequences that converge to well-known limit points, such as the golden ratio.

\section{Basics}
%********************************************
%  ***             THEORY               ***
%********************************************

A general continued fraction is constructed
from two sequences
$\{ a_n \}$ and $\{ b_n \}$ of
complex numbers.
For example, if $n = 3$ then

\[
\K^{3}_{n = 1}(a_n/b_n)=
b_0+ \cfrac{a_1}{b_1 +
\cfrac{a_2}{b_2 +
\cfrac{a_3}{b_3}}}.
\]

Consider the following sequence
of functions,
\[
\phi_0(w) = b_0+w,
\hspace{0.5cm}
\phi_1(w) = \frac{a_1}{b_1 + w},
\hspace{0.5cm}
\phi_2(w) = \frac{a_2}{b_2 + w},
\hspace{0.5cm}
\phi_3(w) = \frac{a_3}{b_3 + w}.
\]

The continued fraction can
be constructed by
composing the $\phi$'s.
\begin{align*}
\phi_0 \circ \phi_1 \circ
\phi_2 \circ \phi_3(0)
&=
\phi_0 \circ \phi_1 \circ
\phi_2
\left(
  \frac{a_3}{b_3}
\right) \\
&=
\phi_0 \circ \phi_1
\left(
  \cfrac{a_2}{b_2 +
  \cfrac{a_3}{b_3}}
\right) \\
&=
\phi_0
\left(
  \cfrac{a_1}{b_1 +
  \cfrac{a_2}{b_2 +
  \cfrac{a_3}{b_3}}}
\right)\\
&=
  b_0+
  \cfrac{a_1}{b_1 +
  \cfrac{a_2}{b_2 +
  \cfrac{a_3}{b_3}}}
\end{align*}

We also introduce a new symbol
$\Phi_n$ to represent the
composition of $n + 1$
transformations
$\phi_i$; that is,

\[
\Phi_3(0) =
\phi_0 \circ \phi_1 \circ
\phi_2 \circ \phi_3(0)
=
  b_0+
  \cfrac{a_1}{b_1 +
  \cfrac{a_2}{b_2 +
  \cfrac{a_3}{b_3}}}.
\]

%------------------------------------------
%-------------- Definition ----------------
%------------------------------------------

\begin{defn}
A \textbf{continued fraction} is
an ordered pair
\begin{align}
\label{eqn:cf_1}
(( \{ a_n \}, \{ b_n \}), \{ f_n \}),
\end{align}
where $\{ a_n \}_1^{\infty} $ and
$\{ b_n \}^{\infty}_1$ are given sequences
of complex numbers, $a_n \ne 0$, and where
$\{ f_n \}$ is the sequence of extended complex numbers,
given by
\begin{align}
\label{eqn:cf_2}
f_n = S_n(0),
\hspace{0.5cm}
n=0,1,2,3,\dots,
\end{align}
where
\begin{align}
\label{eqn:cf_3a}
S_0(w)=s_0(w),
\hspace{0.5cm}
S_n(w)=S_{n-1}(s_n(w)),
\hspace{0.5cm}
n=1,2,3,\dots,
\end{align}
\begin{align}
\label{eqn:cf_3b}
s_0(w)=b_0+w,
\hspace{0.5cm}
s_n(w)=\frac{a_n}{b_n+w},
\hspace{0.5cm}
n=1,2,3,\dots,
\end{align}

\end{defn}

The continued fraction algorithm is
the function $\K$ mapping a pair
$(\{ a_n \}, \{ b_n \})$ onto the
sequence $\{ f_n \}$ defined by
\eqref{eqn:cf_2}, \eqref{eqn:cf_3a}
and \eqref{eqn:cf_3b}.
Here the numbers $a_n$ and $b_n$ are
called the $n$th partial numerators
and denominators. A common name is
element.

\begin{defn}
 The \textbf{$n$th approximate} is given by
\[
S_n(0) =
\cfrac{a_1}{b_1 +
\cfrac{a_2}{b_2 +
\cfrac{a_3}{\ddots +
\cfrac{a_{n}}{b_n}
}}}
\]
\end{defn}

The following notation will
also be used:
\begin{align}
S_n(0) = b_0 +
\frac{a_1}{b_1}\cadd
\frac{a_2}{b_2}\cadd
\dots\cadd
\frac{a_n}{b_n},
\end{align}

%------------------------------------------
%---------- END Definition END ------------
%------------------------------------------

Convergence of a continued fraction to
an extended complex number $f$ means
convergence of $\{ f_n \}$ to $f$, in
which case we write
\begin{align}
f = b_0 +
\frac{a_1}{b_1}\cadd
\frac{a_2}{b_2}\cadd
\condots \cadd
\frac{a_n}{b_n} \cadd
\condots,
\end{align}
or
\begin{align}
f=b_0+\K^{\infty}_{n=1}\frac{a_n}{b_n}
\end{align}

The first few approximants are:
\begin{align} \label{equ:firstfs}
 f_0 = b_0 \,,
 \hspace{0.5cm}
 f_1 = \frac{b_0 b_1 + a_1}{b_1}\, ,
 \hspace{0.5cm}
 f_2 = \frac
   {b_0 b_1 b_2 + b_0 a_2 + a_1 b_2}
   {b_1 b_2 + a_2}
\end{align}

%------------------------------
%--- Canonical  Num/Denom -----
%------------------------------
\subsection{The Canonical Numerator
and Denominator}

\begin{defn}
 The \textbf{Canonical Numerator} $A_n$ and
 \textbf{Canonical Denominator} $B_n$ are
 defined as follows
\begin{align}
\label{equ:ab_equation}
  \left[ {\begin{array}{cc}
   A_{n} \\
   B_{n} \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   A_{n-2} & A_{n-1}  \\
   B_{n-2} & B_{n-1} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   a_{n} \\
   b_{n} \\
  \end{array} } \right]
\end{align}
with
\begin{align}
\label{equ:ab_start}
  \left[ {\begin{array}{cc}
   A_{-1} & A_0 \\
   B_{-1} & B_0 \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   1 & b_0 \\
   0 & 1 \\
  \end{array} } \right],
\end{align}
for $n \in \mathbb{N}$.
\end{defn}

Equation \eqref{equ:ab_equation}
allows us to move from
the continued fraction
to the approximants.
\vs
\begin{center}
\begin{tikzcd}
  [column sep=large,
   row sep=large]
  b_n + K(a_n/b_n)
    \arrow[r, "\eqref{equ:ab_equation}"]
    &
  \frac{A_n}{B_n}
\end{tikzcd}
\end{center}
\vs


\begin{defn}
The \textbf{determinate formula}
is defined as follows
\begin{align} \label{equ:deltadef}
\Delta_n =
  \left| {\begin{array}{cc}
   A_{n} & A_{n-1}  \\
   B_{n} & B_{n-1} \\
  \end{array} } \right|
= A_{n} B_{n-1} - A_{n-1} B_{n}.
\end{align}
\end{defn}

Then we have:
\begin{align} \label{equ:defab}
S_n(0) = f_n = \frac{A_n}{B_n}
=
  \frac{a_1}{b_1}\cadd
\cdots\cadd
  \frac{a_n}{b_n}.
\end{align}
The determinate formula has
the following value.
\begin{align} \label{equ:determinant}
\Delta_{n}=
A_n B_{n-1} - A_{n-1}B_{n}
=
(-1)^{n-1} \prod_{k=1}^{n} a_k.
\end{align}

For a continued fraction segment,
the numerators and denominators
become
\begin{align} \label{equ:segment}
 \frac{A_{v,\lambda}}{B_{v,\lambda}}
 =
 b_{\lambda} +
 \frac{a_{\lambda+1}}{b_{\lambda+1}}
 \cadd
 \frac{a_{\lambda+2}}{b_{\lambda+2}}
 \cadd
 \frac{a_{\lambda+3}}{b_{\lambda+3}}
 \cadd
 \condots
 \cadd
 \frac{a_{\lambda+v}}{b_{\lambda+v}}
\end{align}

The determinate formula for
segments is then:
\begin{align}
 A_{n,\lambda} B_{n-1,\lambda}
 -
 A_{n-1,\lambda} B_{n,\lambda}
 =
 (-1)^{n+1} a_{\lambda+1}
 a_{\lambda+2} \cdots a_{\lambda+n}.
\end{align}


%------------------------------------
%- END  Canonical  Num/Denom   END --
%------------------------------------

%------------------------------------
%------ Fibonacci Sequence ----------
%------------------------------------
\subsection{Fibonacci Sequence}

\begin{defn}
 The \textbf{Fibonacci Sequence} $(F_n)$ is defined
 by the recurrence relation
\begin{align*}
F_0 = 1, \hspace{0.3cm}
F_1 = 1,
\end{align*}
and
\begin{align*}
F_n = F_{n-1} + F_{n-2}.
\end{align*}
\end{defn}
for $n > 1$.

We will use $\phi^{golden}$ to
represent the golden ratio and
$K^{golden}$ to represent
the corresponding continued
fraction,
\begin{align} \label{equ:goldenK}
 K^{golden} =
 1 + \frac{1}{1}
 \cadd \frac{1}{1}
 \cadd \frac{1}{1}
 \condots \hspace{0.4cm}
 \text{ and } \hspace{0.4cm}
 K^{golden}_n =
 1 +
 \underbrace{
   \frac{1}{1}
   \cadd \frac{1}{1}
   \cadd \condots
   \cadd \frac{1}{1}
 }_{n \, \text{times}}
\end{align}

Using \eqref{equ:ab_equation} and
\eqref{equ:ab_start} we find that
\begin{align} \label{equ:goldenfibi}
  K^{golden}_n=
  \frac{A_n}{B_n} =
  \frac{F_{n+2}}{F_{n+1}}
\end{align}

The Fibonacci numbers have the following
closed-form expression,
\begin{align} \label{equ:fibialpha}
 F_n =
 \frac{\alpha^n - \beta^n}
      {\alpha - \beta}
 \hspace{0.4cm}
 \text{ where }
 \alpha = \frac{1+\sqrt{5}}{2},
 \hspace{0.4cm}
 \beta = \frac{1-\sqrt{5}}{2}.
\end{align}

%************************************
%   END       THEORY            END
%************************************

%-----------------------------------
%-----------------------------------
%             SECTION
%-----------------------------------
%-----------------------------------
\section{Preliminary Results}

%***********************************
% ***      Theorem IRF          ***
%***********************************
\begin{IRF}

If $a+b = c+d$, then
\begin{align} \label{equ:irf}
F_a F_b - F_c F_d
=
(-1)^r
(F_{a-r} F_{b-r} - F_{c-r} F_{d-r})
\end{align}

where $F_n$ represents the $n^{th}$
Fibonacci number and $r \in \mathbb{Z}$.
\vs
\end{IRF}

\begin{proof}

First, lets use equation
\eqref{equ:fibialpha}
to find an expression for
$F_a F_b - F_c F_d$
in terms of $\aaa$'s and $\bbb$'s.
\begin{align*}
F_a F_b - F_c F_d
&=\frac{1}{(\aaa-\bbb)^2}
    [(\aaa^a-\bbb^a)(\aaa^b-\bbb^b)-
     (\aaa^c-\bbb^c)(\aaa^d-\bbb^d)] \\
&=\frac{1}{(\aaa-\bbb)^2}
    [(\aaa^{a+b} -\aaa^a\bbb^b
        -\aaa^b\bbb^a + \bbb^{a+b})
    -(\aaa^{c+d} -\aaa^c\bbb^d
        -\aaa^d\bbb^c + \bbb^{c+d}) \\
&=\frac{1}{(\aaa-\bbb)^2}
    [(\aaa^{a+b} - \aaa^{c+d})
     (\bbb^{a+b} - \bbb^{c+d})
   (-\aaa^a\bbb^b + \aaa^b\bbb^a +
     \aaa^c\bbb^d - \aaa^d\bbb^c ) \\
&=\frac{1}{(\aaa-\bbb)^2}
   [-\aaa^a\bbb^b + \aaa^b\bbb^a +
     \aaa^c\bbb^d - \aaa^d\bbb^c]
\end{align*}
The last step used $a+b = c+d$.
Now, lets convert
$F_{a-r} F_{b-r} - F_{c-r} F_{d-r}$
to an expression that
contains $F_a F_b - F_c F_d$.
\begin{align*}
F_{a-r} F_{b-r} - F_{c-r} F_{d-r}
&=\frac{1}{(\aaa-\bbb)^2}
    [(\aaa^{a-r}-\bbb^{a-r})
     (\aaa^{b-r}-\bbb^{b-r})-
     (\aaa^{c-r}-\bbb^{c-r})
     (\aaa^{d-r}-\bbb^{d-r})] \\
&=\frac{1}{(\aaa-\bbb)^2}
    [(\aaa^{a+b-2r}
     -\aaa^{a-r}\bbb^{b-r}
     -\aaa^{b-r}\bbb^{a-r}
     +\bbb^{a+b-2r}) \\
     &-(\aaa^{c+d-2r}
     -\aaa^{c-r}\bbb^{d-r}
     -\aaa^{d-r}\bbb^{c-r}
     +\bbb^{c+d-2r}) \\
&=\frac{1}{(\aaa-\bbb)^2}
    ((\aaa^{a+b-2r}
     -\aaa^{c+d-2r})
     +(\bbb^{a+b-2r}
     -\bbb^{c+d-2r})) \\
  &(-\aaa^{a-r}\bbb^{b-r}
    +\aaa^{b-r}\bbb^{a-r}
    +\aaa^{c-r}\bbb^{d-r}
    -\aaa^{d-r}\bbb^{c-r}) \\
&=\frac{1}{(\aaa-\bbb)^2}
   (-\aaa^{a-r}\bbb^{b-r}
    +\aaa^{b-r}\bbb^{a-r}
    +\aaa^{c-r}\bbb^{d-r}
    -\aaa^{d-r}\bbb^{c-r}) \\
&=\frac{1}{(\aaa-\bbb)^2}
   (-(\aaa\bbb)^{-r} \aaa^{a}\bbb^{b}
    +(\aaa\bbb)^{-r} \aaa^{b}\bbb^{a}
    +(\aaa\bbb)^{-r} \aaa^{c}\bbb^{d}
    -(\aaa\bbb)^{-r} \aaa^{d}\bbb^{c}) \\
&=\frac{(\aaa\bbb)^{-r}}{(\aaa-\bbb)^2}
   (-\aaa^{a}\bbb^{b}
    +\aaa^{b}\bbb^{a}
    +\aaa^{c}\bbb^{d}
    -\aaa^{d}\bbb^{c}) \\
&=\frac{(-1)^r}{(\aaa-\bbb)^2}
   (-\aaa^{a}\bbb^{b}
    +\aaa^{b}\bbb^{a}
    +\aaa^{c}\bbb^{d}
    -\aaa^{d}\bbb^{c}) \\
&=(-1)^r
   (F_a F_b - F_c F_d)
\end{align*}
And this is the result
we were looking for.
\end{proof}


%************************************************
%    END          Theorem IRF              END
%************************************************

%************************************************
%  ***       Theorem IRF (Corollary)    ***
%************************************************

\begin{cor}

Let $k \in \mathbb{Z}$ and let $g_v$
be any sequence of integers with
$g_{v-1} \ne g_v$. Then,

\begin{align}
F_{k+g_{v+1}}=
\frac{F_{g_{v+1} - g_{v-1}}} {F_{g_v-g_{v-1}}}
F_{k+g_{v}}
+
\frac{(-1)^{g_v-g_{v-1}-1} F_{g_{v+1} -g_v} }
{F_{g_v-g_{v-1}}}
F_{k+g_{v-1}}.
\end{align}
\end{cor}

\begin{proof}

First, define the following,
\begin{align*}
a &= g_{v} - g_{v-1} \\
b &= k + g_{v+1} \\
c &= k + g_{v} \\
d &= g_{v+1} - g_{v-1}
\end{align*}

Notice that
$a + b =
k - g_{v-1} + g_{v} + g_{v+1}
= c + d$, so that $a,b,c,$
and $d$ satisfy
the hypothesis for the theorem.

Letting $r = a$, we have
\begin{align*}
&F_a F_b - F_c F_d
=
(-1)^r
(F_{a-r} F_{b-r} - F_{c-r} F_{d-r}) \\
\Rightarrow
&F_{b} = \frac{F_d}{F_a} F_c
      + (-1)^{r}
\frac{F_{a-r} F_{b-r} -
      F_{c-r} F_{d-r}}{F_a} \\
\Rightarrow
&F_{b} = \frac{F_d}{F_a} F_c
      + (-1)^{r}
\frac{- F_{c-r} F_{d-r}}{F_a} &
\text {since } a-r = 0 \\
\end{align*}

And preforming the substitutions,
we get the result.
\end{proof}

%************************************************
%     END    Theorem IRF (Corollary)    END
%************************************************

%************************************************
%  ***            Theorem 3.2               ***
%************************************************
\begin{thm}
\begin{align} \label{equ:fibi1}
\frac{F_{F_{v+2}-1}}{F_{F_{v+2}}}=
\frac{1}{1}\cadd
\frac{F_{F_2}/F_{F_1}}{F_{F_3}/F_{F_1}}\cadd
\frac{F_{F_3}/F_{F_2}}{F_{F_4}/F_{F_2}}\cminus
\cdots\cadd
\frac
{(-1)^{F_{v-1}-1} F_{F_v}/F_{F_{v-1}}}
{F_{F_{v+1}} / F_{F_{v-1}} },
\end{align}
\end{thm}

\begin{proof}

We will use induction on $v$.
If $v = 2$, then the left-hand side is

\[
\frac{F_{F_{v+2}-1}}{F_{F_{v+2}}}=
\frac{F_{F_{4}-1}}{F_{F_{4}}}=
\frac{F_{3-1}}{F_{3}}=
\frac{F_{2}}{F_{3}}=
\frac{2}{3}.
\]

And the right-hand side is

\[
\frac{1}{1}\cadd
\frac{F_{F_2}/F_{F_1}}{F_{F_3}/F_{F_1}}
=
\frac{1}{1}\cadd
\frac{F_{1}/F_{1}}{F_{2}/F_{1}}
=
\frac{1}{1}\cadd
\frac{1}{2}
=
\frac{1}{1+\frac{1}{2}}
=\frac{2}{3}
\]

So $v=2$ is our base case.
From \eqref{equ:defab} we have,
\begin{align} \label{equ:match}
\frac{A_n}{B_n}
&=
  \frac{a_1}{b_1}\cadd
\cdots\cadd
  \frac{a_n}{b_n}
\end{align}

Comparing equations \eqref{equ:match} and
\eqref{equ:fibi1} we can determine
the following equations.
\begin{align}
A_{n}  =  F_{F_{v+2}-1} / \label{equ:start}
  {F_{F_{v+2}}}  \\
A_{n-1}=  F_{F_{v+1}-1} /
  {F_{F_{v+1}}}  \\
A_{n-2}= F_{F_{v}-1}   /
  {F_{F_{v}}}  \\
a_{n}  = (-1)^{F_{v-1}-1} F_{F_v}
  /F_{F_{v-1}} \\
b_{n}  = F_{F_{v+1}} \label{equ:last}
  /F_{F_{v-1}}
\end{align}

From equation \eqref{equ:ab_equation}
we have
\begin{align} \label{equ:recurencea}
A_n &= b_n A_{n-1} + a_n A_{n-2}
\end{align}

Applying equations \eqref{equ:start}
to \eqref{equ:last} to
equation \eqref{equ:recurencea}
gives
\begin{align*}
F_{F_{v+2}}
&=
\left(
  \frac{F_{F_{v+1}}}{F_{F_{v-1}}}
  \right)
  F_{F_{v+1}-1}
+
(-1)^{F_{v-1}-1}
\left(
  \frac{F_{F_{v}}}{F_{F_{v-1}}}
  \right)
  F_{F_{v-1}}
\end{align*}

Since the Fibonacci numbers form a
sequence where $F_n \ne F_{n+1}$,
we can use the Index-Reduction Formula
where the sequence $g_v$ is $F_n$.
\vs

We have completed the inductive step,
and the result is true.
\end{proof}

%************************************************
%  ***          Lemma Absorption           ***
%************************************************

Recall equation \eqref{equ:goldenK}.
Clearly $K^{golden}_n = K^{golden}_{n+1}$.
Since $K^{golden}$ converges
to $\phi^{golden}$,
we can say that an element
of $K^{golden}$ is
\textit{absorbed} into $\phi^{golden}$.
This motivates the following lemma.

\begin{absorption}

If $\aaa = \frac{1 + \sqrt{5}}{2}$ and
$\aaa = 1+\cfrac{1}{\omega}$
then
$\omega=\aaa$.
\end{absorption}

\begin{proof}
$\aaa = 1+ \cfrac{1}{\omega}$\,
$\Rightarrow$\,
$\aaa-1 = \cfrac{1}{\omega }$\,
$\Rightarrow$\,
$\frac{1}{\aaa} = \cfrac{1}{\omega }$\,
$\Rightarrow$\,
$\aaa=\omega$
\end{proof}

%************************************************
%  ***       Lemma  (Golden Ratio)          ***
%************************************************

The following lemma takes the limit
of equation \eqref{equ:goldenfibi} and
gives a useful relationship between
the Fibonacci sequence and $\phi^{golden}$.

\begin{golden} \label{lem:golden}
\[
\lim_{n \rightarrow \infty}
\frac{F_{n+1}}{F_{n}}
=
\frac{1 + \sqrt{5}}{2}
=
\phi^{golden}
\]
\end{golden}
\begin{proof}

If $\bbb = \frac{1-\sqrt{5}}{2}$, then

\begin{align*}
  \lim_{n \rightarrow \infty}
  \frac{F_{n+1}}{F_{n}}
&=
  \lim_{n \rightarrow \infty}
  \frac{\aaa^{n+1} - \bbb^{n+1}}
  {\aaa^{n} - \bbb^{n}} \\
&=
  \lim_{n \rightarrow \infty}
  \frac{\aaa \aaa^{n} -
       \bbb \bbb^{n}}
  {\aaa^{n} - \bbb^{n}} \\
&=
  \lim_{n \rightarrow \infty}
  \frac{\aaa \aaa^{n} }
  {\aaa^{n} } \\
&=
  \aaa \lim_{n \rightarrow \infty}
  \frac{ \aaa^{n} }
  {\aaa^{n} } \\
&=
  \aaa
\end{align*}
\end{proof}

%************************************************
%  ***        Lemma Transformation        ***
%************************************************
\begin{transformation}

$\cfrac{a}{b}\cadd\cfrac{c}{\omega}$
\,$=$\,
$\cfrac{a/b}{1}\cadd\cfrac{c/b}{\omega}$
\end{transformation}

\begin{proof}

\[
\cfrac{a}{b}\cadd\cfrac{c}{\omega}
=
\cfrac{a}{b + \frac{c}{\omega} }
=
\left(
   \cfrac{a}{b + \frac{c}{\omega} }
   \right)
\left(
   \cfrac{\frac{1}{b}}{\frac{1}{b}}
   \right)
=
\cfrac{a/b}{b/b + \frac{c/b}{\omega} }
=
\cfrac{a/b}{1}\cadd\cfrac{c/b}{\omega}
\]
\end{proof}

The preceding three lemmas will
be used in the proof of the next
theorem.

%************************************************
%  ***            Theorem 3.4               ***
%************************************************
\begin{thm}

\begin{align}
\frac{1+\sqrt{5}}{2}=
1 + \frac{F_{F_1}/F_{F_4}}{1}
\cminus \frac{F_{F_2}/F_{F_5}}{1}
&\cadd \frac{F_{F_3}/F_{F_6}}{1} \\
\cadd \frac{F_{F_4}/F_{F_7}}{1}
&\cminus \frac{F_{F_5}/F_{F_8}}{1}
\cadd \frac{F_{F_6}/F_{F_9}}{1}
\cadd \cdots
\end{align}
\end{thm}

\begin{proof}

Recall equation \eqref{equ:fibi1}
\[
\frac{F_{F_{v+2}-1}}{F_{F_{v+2}}}=
\frac{1}{1}\cadd
\frac{F_{F_2}/F_{F_1}}{F_{F_3}/F_{F_1}}\cadd
\frac{F_{F_3}/F_{F_2}}{F_{F_4}/F_{F_2}}\cminus
\cdots\cadd
\frac
{(-1)^{F_{v-1}-1} F_{F_v}/F_{F_{v-1}}}
{F_{F_{v+1}} / F_{F_{v-1}} },
\]
First, we take the limit of both sides,
\[
\lim_{n \rightarrow \infty}
\frac{F_{F_{v+2}-1}}{F_{F_{v+2}}}=
\frac{1}{1}\cadd
\frac{F_{F_2}/F_{F_1}}{F_{F_3}/F_{F_1}}\cadd
\frac{F_{F_3}/F_{F_2}}{F_{F_4}/F_{F_2}}\cminus
\frac{F_{F_4}/F_{F_3}}{F_{F_5}/F_{F_3}}\cadd
\frac{F_{F_5}/F_{F_4}}{F_{F_6}/F_{F_4}}\cadd
\cdots
\]
Second, we apply the Golden Ratio Lemma,
\[
\aaa^{-1}=
\frac{1}{1}\cadd
\frac{F_{F_2}/F_{F_1}}{F_{F_3}/F_{F_1}}\cadd
\frac{F_{F_3}/F_{F_2}}{F_{F_4}/F_{F_2}}\cminus
\frac{F_{F_4}/F_{F_3}}{F_{F_5}/F_{F_3}}\cadd
\frac{F_{F_5}/F_{F_4}}{F_{F_6}/F_{F_4}}\cadd
\cdots
\]
Third, we take the reciprocal of both sides,
\[
\aaa=
1+
\frac{F_{F_2}/F_{F_1}}{F_{F_3}/F_{F_1}}\cadd
\frac{F_{F_3}/F_{F_2}}{F_{F_4}/F_{F_2}}\cminus
\frac{F_{F_4}/F_{F_3}}{F_{F_5}/F_{F_3}}\cadd
\frac{F_{F_5}/F_{F_4}}{F_{F_6}/F_{F_4}}\cadd
\cdots
\]

Now simplify using
since $F_{F_1} = F_{F_2} = F_{F_3} = 1$,
\[
\aaa=
1+
\frac{1}{1}\cadd
\frac{F_{F_3}/F_{F_2}}{F_{F_4}/F_{F_2}}\cminus
\frac{F_{F_4}/F_{F_3}}{F_{F_5}/F_{F_3}}\cadd
\frac{F_{F_5}/F_{F_4}}{F_{F_6}/F_{F_4}}\cadd
\cdots
\]

Absorb that term (Absorption Lemma),
\[
\aaa=
1+
\frac{F_{F_3}/F_{F_2}}{F_{F_4}/F_{F_2}}\cminus
\frac{F_{F_4}/F_{F_3}}{F_{F_5}/F_{F_3}}\cadd
\frac{F_{F_5}/F_{F_4}}{F_{F_6}/F_{F_4}}\cadd
\cdots
\]

Apply the equivalence lemma,

\[
\aaa=
1+
\frac{F_{F_3}/F_{F_4}}{1}\cminus
\frac{F_{F_2}/F_{F_3}}{F_{F_5}/F_{F_3}}\cadd
\frac{F_{F_5}/F_{F_4}}{F_{F_6}/F_{F_4}}\cadd
\cdots
\]

And again,

\[
\aaa=
1+
\frac{F_{F_3}/F_{F_4}}{1}\cminus
\frac{F_{F_2}/F_{F_5}}{1}\cadd
\frac{F_{F_3}/F_{F_4}}{F_{F_6}/F_{F_4}}\cadd
\cdots
\]

Continuing $n$ times,
\[
\aaa=
1+
\frac{F_{F_3}/F_{F_4}}{1}\cminus
\frac{F_{F_2}/F_{F_5}}{1}\cadd
\frac{F_{F_3}/F_{F_6}}{1}\cadd
\frac{F_{F_4}/F_{F_7}}{1}\cminus
\frac{F_{F_5}/F_{F_8}}{1}\cadd
\cdots
\]

Finally we can substitute $F_{F_1} = F_{F_3}$
to get the desired result.
\[
\frac{1+\sqrt{5}}{2}=
1+
\frac{F_{F_1}/F_{F_4}}{1}\cminus
\frac{F_{F_2}/F_{F_5}}{1}\cadd
\frac{F_{F_3}/F_{F_6}}{1}\cadd
\frac{F_{F_4}/F_{F_7}}{1}\cminus
\frac{F_{F_5}/F_{F_8}}{1}\cadd
\cdots
\]
\end{proof}

%************************************************
%   END           Theorem 3.4              END
%************************************************

%--------------------------------------------
%--------------------------------------------
%                  SECTION
%--------------------------------------------
%-------------------------------------------
\section{Contractions of
Continued Fractions}

\subsection{Linear Algebra and
Continued Fractions}

We have already seen the connection between continued fractions, the Fibonacci sequence, and the golden ratio.  Recall that we created this connection by first defining approximates using a reoccurrence relation.
Then we found the Fibonacci numbers by calculating the canonical numerators and denominators for $K^{golden}$.
The following lemmas will create a new connection between matrix multiplication and the calculation of a canonical numerators and denominators.

%********************************************
%                  Little Lemma          
%********************************************
\begin{LL}

\begin{align}
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n}
=   
  \left[ {\begin{array}{cc}
   F_{n+1} & F_n  \\
   F_n & F_{n-1}  \\
  \end{array} } \right]
\end{align}
\end{LL}

\begin{proof}
\vs

For our base case we have,
\[
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n}
=   
  \left[ {\begin{array}{cc}
   F_2 & F_1  \\
   F_1 & F_0  \\
  \end{array} } \right]
\]
Then if we assume the result, we have
\begin{align*}
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n+1}
&=
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n}
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]\\
&=
  \left[ {\begin{array}{cc}
   F_{n+1} & F_n  \\
   F_n & F_{n-1}  \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right] \\
&=   
  \left[ {\begin{array}{cc}
   F_{n+1}+F_n & F_{n+1} \\ 
   F_n+F_{n-1} & F_{n}   \\
  \end{array} } \right]  \\
&=   
  \left[ {\begin{array}{cc}
   F_{n+2} & F_{n+1} \\ 
   F_{n+1} & F_{n}   \\
  \end{array} } \right] \\
\end{align*}
By induction we have proved the result.\\
\end{proof}
\vs
%************************************************
%        END       Little Lemma      END    
%************************************************

We know we can write the canonical numerators 
and denominators of the golden ratio as
\[
\phi_{\text{golden}} = 1+ 
\frac{1}{1} \cadd
\frac{1}{1} \cadd
\dots =
\frac{F_{n+2}}{F_{n+1}}.
\]
This shows a fundamental connection 
between continued fractions and
the Fibonacci numbers.
The reader may wonder if we can generalize 
the little lemma. We can.

%************************************************
%                  Big Lemma          
%************************************************
\begin{BL}

\begin{align}
  \prod_{i=1}^{n+1}
  \left[ {\begin{array}{cc}
   b_{i-1} & a_i  \\
   1 & 0 \\
  \end{array} } \right]
=   
  \left[ {\begin{array}{cc}
   A_{n} & a_{n+1} A_{n-1}  \\
   B_{n} & a_{n+1} B_{n-1} \\
  \end{array} } \right]
\end{align}
\end{BL}

\begin{proof}

For our base case we have,
\[
  \prod_{i=1}^{2}
  \left[ {\begin{array}{cc}
   b_{i-1} & a_i  \\
   1 & 0 \\
  \end{array} } \right]
=   
  \left[ {\begin{array}{cc}
   b_0 & a_1  \\
   1 & 0 \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b_1 & a_2  \\
   1 & 0 \\
  \end{array} } \right]
=   
  \left[ {\begin{array}{cc}
   b_0 b_1 + a_1 & (a_2) b_0  \\
   b_1 & (a_2) \\
  \end{array} } \right].
=   
  \left[ {\begin{array}{cc}
   A_1 & (a_2) A_0  \\
   B_1 & (a_2) B_0 \\
  \end{array} } \right].
\]

Then if we assume the result, we have
\begin{align*}
  \prod_{i=1}^{n+1}
  \left[ {\begin{array}{cc}
   b_{i-1} & a_i  \\
   1 & 0 \\
  \end{array} } \right]
&=   
  \prod_{i=1}^{n}
  \left[ {\begin{array}{cc}
   b_{i-1} & a_i  \\
   1 & 0 \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b_{n+1} & a_{n+2}  \\
   1 & 0 \\
  \end{array} } \right] \\
&=   
  \left[ {\begin{array}{cc}
   A_n & a_{n+1}A_{n-1}  \\
   B_n & a_{n+1}B_{n-1} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b_{n+1} & a_{n+2}  \\
   1 & 0 \\
  \end{array} } \right] \\
&=   
  \left[ {\begin{array}{cc}
   A_n b_{n+1} + a_{n+1} A_{n-1} & a_{n+2}A_{n}  \\
   B_n b_{n+1} + a_{n+1} B_{n-1} & a_{n+2}B_{n}  \\
  \end{array} } \right] \\
&=   
  \left[ {\begin{array}{cc}
   A_{n+1} & a_{n+2}A_{n}  \\
   B_{n+1} & a_{n+2}B_{n}  \\
  \end{array} } \right]. \\
\end{align*}
By induction we have proved the result.\\
\end{proof}

%********************************************
%        END    Big Lemma      END    
%********************************************

%*******************************************
%            Little Corollary          
%********************************************
\begin{LC}
\begin{align}
F_{m+n} = 
F_{n+1}F_m + F_n F_{m-1} =
F_n F_{m+1} + F_{n-1} F_m
\end{align}
\end{LC}

\begin{proof}

By the little lemma.
\begin{align*}
  \left[ {\begin{array}{cc}
   1 & 1 \\
   1 & 0 \\
  \end{array} } \right]^{n+m}
=
  \left[ {\begin{array}{cc}
   F_{(m+n)+1} & F_{m+n}   \\
   F_{m+n}   & F_{(m+n)-1} \\
  \end{array} } \right] \\
\end{align*}

By the associativity of matrix multiplication
\begin{align*}
  \left[ {\begin{array}{cc}
   1 & 1 \\
   1 & 0 \\
  \end{array} } \right]^{n+m}
&=
  \left[ {\begin{array}{cc}
   1 & 1 \\
   1 & 0 \\
  \end{array} } \right]^{n}
  \left[ {\begin{array}{cc}
   1 & 1 \\
   1 & 0 \\
  \end{array} } \right]^{m} \\
&=
  \left[ {\begin{array}{cc}
   F_{n+1} & F_{n}   \\
   F_{n}   & F_{n-1} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   F_{m+1} & F_{m}   \\
   F_{m}   & F_{m-1} \\
  \end{array} } \right] \\
&=
  \left[ {\begin{array}{cc}
   F_{n+1} F_{m+1} + F_{n}   F_{m} & 
   F_{n+1} F_{m}   + F_{n}   F_{m-1} \\
   F_{n}   F_{m+1} + F_{n-1} F_{m} & 
   F_{n} F_{m}     + F_{n-1} F_{m-1} \\
  \end{array} } \right] \\
\end{align*}

From the lower left and upper right
matrix elements we have the result.
\end{proof}

%********************************************
%        END    Little Corollary       END    
%********************************************

%*******************************************
%            Big Corollary          
%*******************************************
\begin{BC}
\begin{align} \label{equ:bq1}
P_{n+m,\lambda} 
&=
  P_{n, \lambda} P_{m-1,\lambda+n+1} +
  a_{\lambda+n+1} P_{n-1, \lambda}
  Q_{m-1,\lambda+n+1},
\end{align}
\begin{align} \label{equ:bq2}
Q_{n+m,\lambda}
&=
  Q_{n, \lambda} P_{m-1,\lambda+n+1} +
  a_{\lambda+n+1} Q_{n-1, \lambda}
  Q_{m-1,\lambda+n+1}. 
\end{align}
\end{BC}

\begin{proof}

By the associativity of matrices,
\begin{align}
\prod^{m+n}_{k=0}
  \left( {\begin{array}{cc}
   b_{\lambda +k} &F_{\lambda + k +1}   \\
   1   & 0 \\
  \end{array} } \right) 
=
\prod^{n}_{i=0}
  \left( {\begin{array}{cc}
   b_{\lambda +i} & a_{\lambda + i +1}   \\
   1   & 0 \\
  \end{array} } \right) 
\prod^{m}_{j=0}
  \left( {\begin{array}{cc}
   b_{\lambda +n+j} & a_{\lambda + n+j+1}   \\
   1   & 0 \\
  \end{array} } \right) 
\end{align}

By equations \eqref{equ:segment}
and the Big Lemma we can
write this as

\begin{align*}
  \left( {\begin{array}{cc}
   P_{n+m, \lambda} & a_{\lambda +n+m+1} 
                      P_{n+m-1, \lambda}   \\
   Q_{n+m, \lambda}  & a_{\lambda +n+m+1} 
                       Q_{n+m-1, \lambda} \\
  \end{array} } \right) 
=
  \left( {\begin{array}{cc}
   P_{n, \lambda} & a_{\lambda +n+1} 
                      P_{n-1, \lambda}   \\
   Q_{n, \lambda}  & a_{\lambda +n+1} 
                       Q_{n-1, \lambda} \\
  \end{array} } \right) \\
  \left( {\begin{array}{cc}
   P_{m-1, \lambda+n+1} & 
      a_{\lambda +n+m+1} P_{m-2, \lambda+n+1}   \\
   Q_{m-1, \lambda+n+1}  & 
      a_{\lambda +n+m+1} Q_{m-2, \lambda+n+1} \\
  \end{array} } \right) 
\end{align*}
\vs

Computing the first column of the
matrix product gives the result.
\end{proof}

%*******************************
%   END   Big Corollary     END
%*******************************

\begin{thm}
\begin{align} \label{equ:seg_det}
 P_{n,\lambda} Q_{n+m,\lambda}
 -
 P_{n+m, \lambda} Q_{n, \lambda}
 =
 (-1)^{n+1}
 a_{\lambda+1} \cdots
 a_{\lambda+n+1}
 Q_{m-1,\lambda+n+1}
\end{align}
\begin{proof}
 Multiply \eqref{equ:bq1}
 by $Q_{n,\lambda}$
 and \eqref{equ:bq2} by
 $P_{n,\lambda}$.
 Take the difference between
 the resulting equations.
 Use the determinate formula
 for seqments to get the result.
\end{proof}

\end{thm}


Equation \eqref{equ:ab_equation}
allows us to move from
the continued fraction
to the approximants.
\begin{center}
\begin{tikzcd}
  [column sep=large,
   row sep=large]
  b_n + K(a_n/b_n)
    \arrow[r, "\eqref{equ:ab_equation}"]
    &
  \frac{A_n}{B_n}
\end{tikzcd}
\end{center}

The following theorem
(Bernoulli's Theorem)
will allow us to move back.

\begin{center}
\begin{tikzcd}
  [column sep=large,
   row sep=large]
  b_n + K(a_n/b_n)
    &
  \frac{A_n}{B_n}
    \arrow[l, "Bernoulli"]
\end{tikzcd}
\end{center}

%*********************************
%        Theorem Bernoulli
%*********************************
\begin{bernoulli}

The sequences 
$\{ A_n \}^{\infty}_{n=-1}$
and
$\{ B_n \}^{\infty}_{n=-1}$
of complex numbers are the 
canonical numerators and
denominators of some continued
fraction $b_0 + \K(a_n/b_n)$
if and only if

\begin{align} \label{equ:def1}
A_{-1}=B_{0}=1,\,
B_{-1}=0,\,
\Delta_n=A_n B_{n-1} - B_{n}A_{n-1} \ne 0
\end{align}

for all $n \in \mathbb{N}$.
If \eqref{equ:def1} holds,
then $b_0+\K(a_n/b_n)$ is
uniquely determined by
\[
b_0 = A_0,\,
b_1 = B_1,\,
a_1 = A_1 - A_0 B_1 \,,
\]
\begin{align} \label{equ:def2}
a_n=-\frac{\Delta_n}{\Delta_{n-1}},\,
b_n=
\frac
  {A_nB_{n-2} - B_n A_{n-2}}
  {\Delta_{n-1}}
  \hspace{0.2cm} \text{ for } n \ge 2.
\end{align}
\end{bernoulli}

\begin{proof}

Suppose $b_o + \K(a_n/b_n)$ is 
a continued fraction.
Then by the determinant formula
\eqref{equ:determinant} and the initial
conditions \eqref{equ:ab_start},
\eqref{equ:def1} holds.
\vs

On the other hand, suppose we have sequences 
$\{ A_n \}$ and $\{ B_n \}$ and they 
satisfy equation \eqref{equ:def1}.
Then $a_n$ and $b_n$ are solutions of the
system 
\begin{align}
b_n A_{n-1} + a_n A_{n-2} = A_n, \hspace{1cm} 
b_n B_{n-1} + a_n B_{n-2} = B_n
\end{align}
of linear equations.
By applying Crammers rule, we get
equation \eqref{equ:def2}.
Since the determinant of the 
system is $\Delta_{n-1} \ne 0$,
the solution is unique.
\end{proof}

%*********************************
%    END   Theorem     END
%*********************************

\begin{defn}
The partial fraction $d_0+ \K(c_n/d_n)$
is a \textbf{contraction} of
$b_0+\K(a_n/b_n)$
if its classical approximants
$\{ g_n \}$ form a subsequence of the
classical approximants $\{ f_n \}$
of $b_0 + \K(c_n/d_n)$.
\end{defn}

\begin{center}
\begin{tikzcd}
  [column sep=large,
   row sep=large]
  b_n + K(a_n/b_n)
    \arrow[r, "contraction"]
    \arrow[d, "approximate"]
    &
  d_0 + K(c_n/d_n)
    \arrow[d, "approximate"]
  \\
  \{ f_n \} = \frac{A_n}{B_n}
    \arrow[r, "subsequence"]
    \arrow[u, "Bernoulli"]
    &
  \{ g_n \} = \frac{C_n}{D_n}
    \arrow[u, "Bernoulli"]
\end{tikzcd}
\end{center}
\vs

The next theorem defines
the elements of a
contracted continued fraction
in terms of the original
continued fraction.
The stratagy for the following
proof is to
start with an object
in the contracted continued
fraction
$d_0 + K(c_n/d_n)$.
For example $d_0$.
Second, move it down
to the approximants
(using equation \eqref{equ:def2}).
For example $d_0 = D_0$.
Third, move it to the left
using the subsequence
formula.
In this case, the even
elements of the sequence.
For example $D_0 = B_0$.
Finally, move it up to
the original continued
fraction,
using Bernoulli's theorem.
For example $B_0 = b_0$.



%*********************************
%        Even Contraction
%*********************************
\begin{thm}(Even Contraction) \label{cor:even}

The canonical contraction of
$b_0 + \K(a_n/b_n)$ with
\begin{align}
C_k = A_{2k}, \label{equ:evendef}
\hspace{0.5cm}
D_k=B_{2k}
\hspace{0.5cm}
\text{for } k=0,1,2,\dots
\end{align}
exists if and only if $b_{2k} \ne 0$
for $k=1,2,3,\dots,$ and is then given by
\[
b_0+
\frac{b_2 a_1}{b_2 b_1 +a_2}\cminus
\frac
  {a_2 a_3 b_4 / b_2}
  {a_4+b_3 b_4 + a_3 b_4/b_2}\cminus
\frac
  {a_4 a_5 a_6 / b_4}
  {a_6+ b_5 b_6 + a_5 b_6 / b_4}\cminus
  \cdots
\]
\end{thm}

\begin{proof}
We have
\begin{align*}
d_0
 &= D_0 = B_0 = b_0
 && \text{(as desired)}\\
c_1
 &= C_1 - C_0 D_1
    &&
    \text{ equation }
    \eqref{equ:def2} \\
 &=
 A_2 - A_0 B_2
    &&
    \text{ equation }
    \eqref{equ:evendef} \\
 &=
 (b_0 b_1 b_2 + b_0 a_2 + a_1 b_2)
 - (b_0) (b_1 b_2 + a_2)
    &&
    \text{ equation }
    \eqref{equ:firstfs} \\
 &= b_2 a_1
    &&
    \text{ (as desired) }\\
d_1
 &= D_1
    &&
    \text{ equation }
    \eqref{equ:def2} \\
 &=
 B_2
    &&
    \text{ equation }
    \eqref{equ:evendef} \\
 &=
 b_2 b_1 + a_2
    &&
    \text{ equation }
    \eqref{equ:firstfs}
    \text{ (as desired) }\\
\end{align*}

$c_n = -\Delta_n/\Delta_{n-1}$,
where
\begin{align*}
 \Delta_n
 &=
 C_n D_{n-1} - D_n C_{n-1}
 =
 A_{2n} B_{2n-2} - B_{2n} A_{2n-2} \\
 &=
 -B^{(2n-1)}_1
 \prod^{2n-1}_{j=1}(-a_i)
 =
 -b_{2n}
 \prod^{2n-1}_{j=1}
 (-a_j).
\end{align*}

Finally, we have $d_n = P_n/\Delta_{n-1}$,
where
\begin{align*}
 P_n
 &=
 C_n D_{n-2} - D_{n} C_{n-2}
 =
 A_{2n} B_{2n-4} - B_{2n} A_{2n-4} \\
 &=
 -B_{3}^{(2n-3)}
 \prod_{j=1}^{2n-3} (-a_j) \\
 &=
 -[b_{2n}(b_{2n-1} b_{2n-2}
 + a_{2n-1}) +
 a_{2n} b_{2n-2}]
 \prod_{j=1}^{2n-3}(-a_j).
\end{align*}
\end{proof}
%*********************************
%  END   Even Contraction    END
%*********************************

The next theorem
provides a general formula
for the
contracted continued fraction
$d_0 + K(c_n/d_n)$ in terms of the orginal continued fraction
$b_0 + K(a_n/b_n)$.
Since the contraction may
not start at the
first approximate,
the contraction is
a segment.


\begin{general} \label{thm:general}

Let $\{n_k\}$ be a strictly increasing sequence
of integers with $n_0 \ge 0$.

The canonical contraction of
$b_0 + \K(a_n/b_n)$ with
\begin{align}
C_k = A_{n_k}, \label{equ:subdef}
\hspace{0.5cm}
D_k=B_{n_k}
\hspace{0.5cm}
\text{for } k=0,1,2,\dots
\end{align}
exists if and only if
$b_{n_k} \ne 0$
for $k=1,2,3,\dots,$ and is then given by

\begin{align}
\frac{C_k}{D_k}=
\frac{A_{n_k}}{B_{n_k}}=
\delta_0 +
\frac{\gamma_1}{\delta_1}\cadd
\frac{\gamma_2}{\delta_2}\cadd
\condots\cadd
\frac{\gamma_k}{\delta_k},
\end{align}
where $\delta_0 = A_{n_0}/B_{n_0}$,
$\delta_1 = B_{n_1}$.

\[
\gamma_1=(-1)^{n_0} a_1 a_2
\cdots a_{n_{0}+1}
\frac{B_{n_1-n_0-1,n_0+1} }
     {B_{n_0} }
\]
\[
\gamma_2=(-1)^{n_1-n_0 -1}
         a_{n_0 + 2} a_{n_0 + 3}
\cdots a_{n_{1}+1}
\frac{B_{n_0}
      B_{n_2-n_1-1, n_{1}+1} }
      { B_{n_{1}-n_{0}-1, n_0+1 } }
\]
\[
\delta_2=
\frac{B_{n_2-n_0-1,n_0+1} }
     {B_{n_1-n_0-1,n_0+1} }
\]
and for $v > 2$,
\begin{align*}
\gamma_v=(-1)^{n_{v-1} n_{v-2} -1} 
         &a_{n_{v-2} + 2} 
         a_{n_{v-2} + 3} \cdots
         a_{n_{v-1}+1} \\
&\cdot \frac{
    B_{n_{v}-n_{v-1}-1, n_{v-1}+1 }
  } 
  { 
    B_{n_{v-1}-n_{v-2}-1, n_{v-2}+1 }
  }
\end{align*}
and
\[
\delta_v = \frac
  {
    B_{n_{v} - n_{v-2}-1, n_{v-2}+1 }
  }
  {
    B_{n_{v-1} - n_{v-2}-1, n_{v-2}+1 }
  }
\]

Moreover, for $i > 0$, $A_{n_i}$ (resp. $B_{n_i}$)
is the ith classical numerator (resp. denominator)
of the continued fraction in equation (4.1).
This remains true for $i=0$, if the 0th
classical denominator on the right is taken
to be $B_{n_0}$, instead of 1.
\end{general}

\begin{proof}
TODO

$c_k = -\Delta_k/\Delta_{k-1}$,
where
\begin{align*}
 \Delta_k
 &=
 C_k D_{k-1} - D_k C_{k-1}
 =
 A_{n_k} B_{n_k-1} -
 B_{n_k-1} A_{n_k} \\
 &=
 (-1)^{n+1}
 a_{\lambda+1} \cdots
 a_{\lambda+n+1}
 B_{m-1,\lambda+n+1}
 & \text{ equation } \eqref{equ:seg_det} \\
 \Delta_{k-1}
 &=
 C_k D_{k-1} - D_k C_{k-1}
 =
 A_{n_k} B_{n_k-1} -
 B_{n_k-1} A_{n_k} \\
 &=
 (-1)^{n}
 a_{\lambda+1} \cdots
 a_{\lambda+n+1}
 B_{m-1,\lambda+n+1}
 & \text{ equation } \eqref{equ:seg_det}
\end{align*}


\end{proof}

\begin{cor} (Fibinacci Contraction)
 Let $n_0 = m_0 \ge 0$ and for
 $v \ge 1$ put
 $n_v = m_0 + m_1 + \cdots + m_v$,
 where $m_i \in \mathbb{Z}^{+}$
 for $i \ge 1$.
 Then for $i \ge 1$,
\begin{align*}
 \frac{(-1)^{m_{1}-1} F_{m_0}
        F_{n_{i+1} - m_0 - m_1}}
      {F_{n_{i+1}-m_0}}
&=
\frac{(-1)^{m_1 -1} F_{m_0} F_{m_2} }
     { F_{m_1 + m_2}} \\
&\cadd
 \frac{(-1)^{m_2 -1} F_{m_1} F_{m_3} }
     { F_{m_2 + m_3} }
\cadd
\condots
\cadd
\frac{(-1)^{m_i -1} F_{m_{i-1}} F_{m_i +1}}
     {F_{m_i + m_{i+1}} }
\end{align*}

\end{cor}


\newpage
%----------------------------------
%----------------------------------
%         BIBLIOGRAPHY
%----------------------------------
%----------------------------------

\begin{thebibliography}{999}

\bibitem{bernoulli75}
  D. Bernoulli,
  \emph{Disquisitiones ulteriores de indola fractionum
  continuarum}.
  Novi comin,
  Acad. sci. Imper. Petropol.
  20,
  1775.

\bibitem{bowman97}
  D. Bowman,
  \emph{Some rapidly converging continued fractions}.
  Illinois Number Theory Conference,
  April 6-7,
  1997.

\bibitem{horadam85}
  A. F. Horadam and J. M. Mahon,
  \emph{Pell and Pell-Lucas polynomials}.
  The Fibonacci Quarterly,
  23.1,
  (1985),
  7-20.

\bibitem{johnsonPdf}
  R. C. Johnson,
  \emph{Fibonacci numbers and matrices}.

  https://web.archive.org/web/20201130211206/http://maths.dur.ac.uk/~dma0rcj/PED/fib.pdf.

\bibitem{lorentzen}
  L. Lorentzen and H. Waadeland,
  \emph{Continued Fractions Vol. 1: Convergence Theory},
  Atlantis Studies in Mathematics and Science,
  Atlantis Press/World Scientific,
  Paris,
  2008,
  1-8, 77-78, 85-87.

\bibitem{perron1}
  O. Perron,
  \emph{Die Lehre Von Den Kettenbruchen},
  Band I,
  B. G. Teubner,
  Stuttgart,
  1957,
  10-14.

\bibitem{perron2}
  O. Perron,
  \emph{Die Lehre Von Den Kettenbruchen},
  Band II,
  B. G. Teubner,
  Stuttgart,
  1957,
  10-12.


\end{thebibliography}


\end{document}

