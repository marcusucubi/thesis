\documentclass{article}

\usepackage{latexsym}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{thmtools}
\usepackage{tikz-cd}
\usepackage{float}

\newcommand{\vs}{\vspace{0.2in}}
\newcommand{\vvs}{\vspace{0.1in}}
\newcommand{\aaa}{\alpha}
\newcommand{\bbb}{\beta}
\newcommand{\K}{\operatornamewithlimits{K}}
\newcommand{\C}{\mathbb{C}}

\newcommand{\mprod}[2]{
  \overset{\curvearrowright}
 {\prod_{#1}^{#2}}
}


\newcommand{\condots}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{\dots}}}
\newcommand{\cadd}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{+}}}
\newcommand{\cminus}[1][0pt]{\mathbin{\genfrac{}{}{#1}{0}{}{-}}}
\newcommand{\AlgI}{\textbf{$\text{CF}^{-1}$}}
\newcommand{\Sub}{\textbf{Sub}}
\newcommand{\Id}{\textbf{Id}}
\newcommand{\Even}{\textbf{Even}}
\newcommand{\Alg}{\textbf{CF}\,}
\newcommand{\Contrac}{\textbf{Contraction}}
\newcommand{\GoldenF}
  {$[1,1, \dots]$\hspace{0.1cm}}
\newcommand{\GoldenR}
  {$(1+\sqrt{5})/2$\hspace{0.1cm}}
\newcommand{\SilverF}
  {$[2,2, \dots]$\,}
\newcommand{\SilverR}
  {$1+\sqrt{2}$\hspace{0.1cm}}
\newcommand{\GDF}
  {General Determinant Formula}


\declaretheorem[
  style=definition]
  {definition}
\declaretheorem[
  style=definition,
  qed=$\blacktriangle$]
  {example}
\declaretheorem[
  style=definition]
  {discussion}

\newtheorem*{LL}{Little Theorem}
\newtheorem*{BL}{Big Theorem}
\newtheorem*{Cassini}{Cassini's Identity}
\newtheorem*{golden}{Golden Ratio Lemma}
\newtheorem*{transformation}{Transformation Lemma}

\begin{document}

\begin{example}
Let $g_i = h_0 + h_1 + \cdots + h_i$
where $h_i = 2^i$.
Then
$g_i-g_{i-1}=h_i$
and
$g_i-g_{i-2}=h_{i-1}+h_i$.
These properties of $g_i$
will allow us to
simplify the contraction.
Let us calculate the
contraction of \GoldenF
using $f_{i} \mapsto f_{g_i}$
to generate the subsequence.
We can reuse table
\ref{tab:continuedFractionGoldenF}
and table
\ref{tab:partialGoldenF}.
Next, we generate the subsequence.

\begin{table}[H]
  \begin{center}
    \begin{tabular}
      {l|c|c|c|c|c|c|c|c|c}
      i & 0 & 1 & 2 & 3 & 4 &
      $\cdots$ &
      n-2 & n-1 & n \\
      \hline
      $C_i$ &
      $F_3$ & $F_5$ &
      $F_9$ & $F_{17}$ & $F_{33}$ &
      $\cdots$ &
      $F_{g_{i-2}+2}$ &
      $F_{g_{i-1}+2}$ & $F_{g_i+2}$ \\
      \hline
      $D_i$ &
      $F_2$ & $F_4$ &
      $F_8$ & $F_{16}$ & $F_{32}$ &
      $\cdots$ &
      $F_{g_{i-2}+1}$ &
      $F_{g_{i-1}+1}$ & $F_{g_i+1}$
    \end{tabular}
    \caption{$f_{i} \mapsto f_{g_i}$
    subsequence.}
    \label{tab:g_i_subsequence}
  \end{center}
\end{table}

\noindent
We apply \AlgI.

\bgroup
\def\arraystretch{1.0}
\begin{table}[H]
  \begin{center}
    \begin{tabular}{l|c|c|c|c|c}
      i & 0 & 1 & 2 &
      $\cdots$ & n \\
      \hline
      $c_i$ &
      $X$ &
      $F_5 - F_3 F_4$ &
      $\cfrac{
      F_4 F_9 - F_5 F_8
      }{
      F_3 F_4 - F_2 F_5
      }$ &
      $\cdots$ &
      $\cfrac{
       F_{g_{n-1}+1} F_{{g_n}+2}-
       F_{g_{n}+1} F_{g_{n-1}+2}
       }{
       F_{g_{n-2}+2} F_{g_{n-1}+1}-
       F_{g_{n-2}+1} F_{g_{n-1}+2}
       }$
       \\
      \hline
      $d_i$ &
      $F_3/F_2$ &
      $F_4$ &
      $\cfrac{
      F_3 F_8 - F_2 F_9
      }{
      F_3 F_4 - F_2 F_5
      }$ &
      $\cdots$ &
      $\cfrac{
       F_{g_{n-1}+2} F_{{g_n}+1}-
       F_{g_{n}+2} F_{g_{n-2}+1}
       }{
       F_{g_{n-2}+2} F_{g_{n-1}+1}-
       F_{g_{n-2}+1} F_{g_{n-1}+2}
       }$
    \end{tabular}
    \caption{$f_{i} \mapsto f_{g_i}$
    contraction
    (with Fibonacci numbers).}
    \label{tab:g_i_F_values}
  \end{center}
\end{table}
\egroup

\noindent
We can use D’Ocagne’s Identity
to calculate $c_n$.
First, we find the numerator.
\begin{align*}
 F_{g_{n-1}+1} F_{{g_n}+2}-
 F_{g_{n}+1} F_{g_{n-1}+2}
 &=
 (-1)
 (F_{g_{n}+1} F_{{g_{n-1}}+2}-
 F_{g_{n-1}+1} F_{g_{n}+2}) \\
 &=
 (-1)(-1)^{g_{n-1}+1}
 F_{(g_n+1)-(g_{n-1}+1)}\\
 &=
 (-1)^{g_{n-1}}
 F_{g_n-g_{n-1}}\\
\end{align*}

\noindent
Since $g_n-g_{n-1} = h_{n}$,
our final expression is
$(-1)^{g_{n-1}} F_{h_n}=
(-1)^{g_{n-1}} F_{2^n}$.

\noindent
Now we find the denominator.
\begin{align*}
 F_{g_{n-2}+2} F_{{g_{n-1}}+1}-
 F_{g_{n-2}+1} F_{g_{n-1}+2}
 &=
 (F_{g_{n-1}+1} F_{{g_{n-2}}+2}-
 F_{g_{n-2}+1} F_{g_{n-1}+2}) \\
 &=
 (-1)^{g_{n-2}+1}
 F_{(g_{n-1}+1)-(g_{n-2}+1)}\\
 &=
 (-1)^{g_{n-1}+1}
 F_{g_{n-1}-g_{n-2}}.\\
\end{align*}

\noindent
Since $g_{n-1}-g_{n-2} = h_{n-1}$,
our final expression is
$(-1)^{g_{n-1}+1}
 F_{g_{n-1}-g_{n-2}}
 =
 (-1)^{g_{n-1}+1} F_{2^{n-1}}$.

\noindent
Therefore $c_n =
 -F_{2^n} / F_{2^{n-1}}$.

\bgroup
\def\arraystretch{1.5}
\begin{table}[H]
  \begin{center}
    \begin{tabular}{l|c|c|c|c|c}
      i & 0 & 1 & 2 &
      $\cdots$ & n \\
      \hline
      $c_i$ &
      $X$ &
      $-1$ &
      $-3$ &
      $\cdots$ &
      $\cfrac{
      -F_{2^{n}}
      }{
      F_{2^{n-1}}
      }$
       \\
      \hline
      $d_i$ &
      $2$ &
      $3$ &
      $8$ &
      $\cdots$ &
      $\cfrac{
      F_{2^{n-1} \cdot 3}
      }{
      F_{2^{n-1}}
      }$
    \end{tabular}
    \caption{$f_{i} \mapsto f_{g_i}$
    contraction.}
    \label{tab:g_i_values}
  \end{center}
\end{table}
\egroup

\noindent
We can calculate the
numerators and denominators
of the contraction
in order to verify they
approach \GoldenR.

\bgroup
\def\arraystretch{1.0}
\begin{table}[H]
  \begin{center}
    \begin{tabular}{l|c|c|c|c|c}
      i & 0 & 1 & 2 &
      $\cdots$ & n\\
      \hline
      $E_i$ &
      $2$ &
      $5$ &
      $34$ &
      $\cdots$ &
      $\cfrac{1}{F_{2^{n-1}}}
      \left(
      -F_{2^n} F_{g_{n-2}+2}
      +F_{2^{n-1} \cdot 3}
      F_{g_{n-1}+2}
      \right)
      $
      \\
      \hline
      $F_i$ &
      $1$ &
      $3$ &
      $21$ &
      $\cdots$ &
      $\cfrac{1}{F_{2^{n-1}}}
      \left(
      -F_{2^n} F_{g_{n-2}+1}
      +F_{2^{n-1} \cdot 3}
      F_{g_{n-1}+1}
      \right)
      $
    \end{tabular}
    \caption{Numerators and
    denominators
    for $f_{i} \mapsto f_{g_i}$
    contraction.}
  \end{center}
\end{table}
\egroup

\noindent
Therefore, the $n$th
contraction of \GoldenF using
$f_i \mapsto f_{g_i}$
where $g_i = 2^0 + 2^1 + \cdots + 2^i$ is
\begin{align*}
2-
\cfrac{1}{3}
\cminus
\cfrac{3}{8}
\cminus
\cdots
\cminus
\cfrac{
F_{2^{n}}/F_{2^{n-1}}
}{
F_{2^{n-1} \cdot 3}/F_{2^{n-1}}
},
\end{align*}

\noindent
and its $n$th
approximate is
\begin{align*}
\cfrac
{
F_{2^{n-1} \cdot 3}
F_{g_{n-1}+2}
-F_{2^n} F_{g_{n-2}+2}
}
{
F_{2^{n-1} \cdot 3}
F_{g_{n-1}+1}
-F_{2^n} F_{g_{n-2}+1}
}.
\end{align*}

\end{example}

\end{document}

