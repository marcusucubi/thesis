\chapter{Convergence}
\label{chp:convergence}

\begin{lem}
\label{thm:fibiEuclid}
\begin{align}
 \frac{F_{n+2}}{F_{n+1}}=
 1 + \frac{1}{
   \frac{F_{n+1}}{F_{n}}
 }
\end{align}
\end{lem}
\begin{proof}
\begin{align*}
 \frac{F_{n+2}}{F_{n+1}}
 &=
 \frac
   {F_{n+1}+F_n}
   {F_{n+1}}\\
 &=1+\frac{F_n}{F_{n+1}}\\
 &=1+
 \frac{1}
   {\frac{F_{n+1}}{F_n}}
\end{align*}
\end{proof}

\begin{discussion}
Since the elements of \GoldenF
are all 1, the numerators
and denominators become
the Fibonacci numbers.
In  particular, $A_n = F_{n+2}$
and $B_n = F_{n+1}$.
So the $n$th convergent for
\GoldenF is given by,
\begin{align}
\label{equ:goldenStart}
f_n = \frac{F_{n+2}}{F_{n+1}}.
\end{align}

Since \GoldenF is a simple
continued fraction,
$f_n$ (and $f_{n-1}$)
tends to a limit,
say $\omega$,
by Theorem \eqref{thm:allSimpleConverge}.
Using Lemma \eqref{thm:fibiEuclid}
and equation \eqref{equ:goldenStart}
we can write,
\begin{align*}
 f_n &= 1+
 \frac{1}{f_{n-1}}.
\end{align*}
Letting $n$ go to infinity we get,
\begin{align}
 \omega &= 1+
 \frac{1}{\omega}.
\label{equ:golden}
\end{align}
From equation
\eqref{equ:golden} we have
$\omega^2 - \omega - 1 = 0$
and the quadratic equation
gives,
\begin{align*}
 \omega = \frac{1\pm \sqrt{5}}{2}.
\end{align*}
Since $f_n > 1$, $\omega$ is the
\GoldenR.
\label{dis:limitOfKgolden}
\end{discussion}

\noindent
Through the prior discussion,
we obtain the following theorem.

\begin{thm}
\begin{align}
\lim_{n \rightarrow \infty}
\frac{F_{n+1}}{F_{n}} =
\frac{1+\sqrt{5}}{2}
\end{align}
\label{thm:fibiConvergesToGolden}
\end{thm}

Can we generalize this result?
Theorem \ref{thm:fibiConvergesToGolden}
is built on the fact that
the numerators and
denominators of \GoldenF are
the Fibonacci numbers.
Perhaps we can find a generalization
if we consider the continued fraction
$[b, b, \dots]$
where $b$ is a positive integer.
Consider,

\begin{align}
 t_1 t_0=
  \left[ {\begin{array}{cc}
   b & 1 \\
   1 & 0 \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b & 1 \\
   1 & 0 \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   b^2+1 & b \\
   b & 0 \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   A_1 & B_1 \\
   A_0 & B_0 \\
  \end{array} } \right].
  \label{equ:tOnetTwo}
\end{align}

\noindent
By examining the
two matrices on the
right hand side of
\eqref{equ:tOnetTwo},
we see that $A_0 = B_1$.

\noindent
Now consider,
\begin{align}
t_2 t_1 t_0=
  \left[ {\begin{array}{cc}
   b & 1 \\
   1 & 0 \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b^2+1 & b \\
   b & 0 \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   b^3+2b & b^2+1 \\
   b^2+1 & b \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   A_2 & B_2 \\
   A_1 & B_1 \\
  \end{array} } \right].
  \label{equ:tTwotOnetZero}
\end{align}

\noindent
By examining the
two matrices on the
right hand side of
\eqref{equ:tTwotOnetZero},
we see that $A_1 = B_2$.
This pattern will continue.
This leads us to the
following theorem:

\begin{thm}
For $[b, b, \dots]$, where
$b$ is a positive integer,
\begin{align*}
 T_n =
  \left[ {\begin{array}{cc}
   B_{n+1} & B_n     \\
   B_n     & B_{n-1} \\
  \end{array} } \right].
\end{align*}
\label{thm:AisB}
\end{thm}
\begin{proof}
By the prior discussion,
we have our base case:
\begin{align*}
 t_1 t_0 =
  \left[ {\begin{array}{cc}
   B_2 & B_1 \\
   B_1 & B_0 \\
  \end{array} } \right].
\end{align*}
Suppose the theorem is true.
Then,
\begin{align*}
  T_{n+1} = t_{n+1} T_n
  &=
  \left[ {\begin{array}{cc}
   b & 1 \\
   1 & 0   \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   B_{n+1} & B_n     \\
   B_n     & B_{n-1} \\
  \end{array} } \right] \\
  &=
  \left[ {\begin{array}{cc}
   b B_{n+1} + B_{n}   &
   b B_{n}   + B_{n-1} \\
   B_{n}     & B_{n-1}   \\
  \end{array} } \right]\\
  &=
  \left[ {\begin{array}{cc}
   B_{n+2} & B_{n+1}     \\
   B_{n+1}   & B_{n} \\
  \end{array} } \right].
\end{align*}
And by induction, we are done.
\end{proof}

\noindent
Since
\begin{align*}
  t_n =
  \left[ {\begin{array}{cc}
   b & 1  \\
   1 & 0  \\
  \end{array} } \right]
\end{align*}
for any $n$, when our continued
fraction is $[b, b, \dots]$,
we can write $t^n$.

\begin{cor}
Given the continued fraction
$[b, b, \dots]$,
\begin{align}
  \left[ {\begin{array}{cc}
   b & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n}
=
  \left[ {\begin{array}{cc}
   B_{n}   & B_{n-1}  \\
   B_{n-1} & B_{n-2}  \\
  \end{array} } \right].
\end{align}
\end{cor}

\noindent
The \textbf{Pell numbers}
are defined by the following
recursive formula:
\begin{align*}
P_0 &= 0, \hspace{0.5cm} P_1 = 1,\\
P_n &= 2P_{n-1} + P_{n-2}
\hspace{0.5cm}
\text{for}
\hspace{0.5cm}
n \ge 2.
\end{align*}

\noindent
The numerators and denominators
of $[2, 2, \dots]$ are the
Pell numbers.

\begin{table}[H]
  \begin{center}
    \begin{tabular}{l|c|c|c|c|c|c}
      i &-1&0&1&2&
      $\cdots$ & n\\
      \hline
      $A_i$ & $P_1$ & $P_2$ & $P_3$ & $P_4$ &
      $\cdots$ &
      $P_{n+2}$\\
      \hline
      $B_i$ & $P_0$ & $P_1$ & $P_2$ & $P_3$ &
      $\cdots$ &
      $P_{n+1}$
    \end{tabular}
    \caption{Numerators and denominators
    of $[2, 2, \dots]$
    (with Pell numbers).}
  \end{center}
\end{table}

\begin{cor}
\begin{align}
  \left[ {\begin{array}{cc}
   2 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n}
=
  \left[ {\begin{array}{cc}
   P_{n+1} & P_n  \\
   P_n     & P_{n-1}  \\
  \end{array} } \right]
  \label{equ:formulaE}
\end{align}
\end{cor}

\begin{cor}
\label{thm:LL}
\begin{align}
  \label{equ:littleLemma}
  \left[ {\begin{array}{cc}
   1 & 1  \\
   1 & 0  \\
  \end{array} } \right]^{n}
=
  \left[ {\begin{array}{cc}
   F_{n+1} & F_n  \\
   F_n & F_{n-1}  \\
  \end{array} } \right]
\end{align}
\end{cor}

\noindent
Taking the determinant of
equation \ref{equ:littleLemma},
we get
\begin{align}
\label{equ:cassini}
F_{n-1} F_{n+1} - {F_n}^2 = (-1)^n
\end{align}
which is commonly known as
Cassini's Formula.
We can also take the
determinant of
equation \eqref{equ:formulaE}
to get
\begin{align}
P_{n-1} P_{n+1} - {P_n}^2 = (-1)^n.
\end{align}


\noindent
If we apply Discussion
\ref{dis:limitOfKgolden}
to $[b, b, \dots]$,
we obtain the following theorem.

\begin{thm}
A simple continued fraction with
the form $[b, b, \dots]$,
converges to
\begin{align}
 \frac{b + \sqrt{b^2+4}}{2}.
\end{align}
\end{thm}
\begin{proof}
By Theorem \ref{thm:AisB},
we have,
\begin{align*}
 f_n = \frac{B_{n+1}}{B_n}.
\end{align*}
Then,
\begin{align*}
 \frac{B_{n+1}}{B_n}
 &=
 \frac{bB_n+B_{n-1}}{B_n} \\
 &=
 b+\frac{B_{n-1}}{B_n} \\
 &=
 b+\frac{1}{\frac{B_n}{B_{n-1}}}.
\end{align*}
By Theorem \eqref{thm:allSimpleConverge},
$f_n$ tends to a limit as $n$
goes to infinity.
Let $\omega$ be that limit.
So,
\begin{align*}
 \omega = b + \frac{1}{\omega}.
\end{align*}
Solving for $\omega$,
we obtain the result.
\end{proof}

\begin{example}
$[2, 2, \dots]$ converges
to $\sqrt{2}+1$.
\end{example}

\noindent
Let's take another look
at Lemma \ref{thm:fibiEuclid}.
If we repeatedly apply it
to $F_7/F_6$,
we get:
\begin{align*}
 \frac{F_{7}}{F_{6}}
 =
 \frac{13}{8}
 &=1 + \frac{5}{8}
 =1 + \cfrac{1}{
    \cfrac{1}{8/5}
    }
 =1 + \cfrac{1}{
    1 + \cfrac{3}{5}
    }
 =1 + \cfrac{1}{
    1 + \cfrac{1}{5/3}
    }\\
&=
 1 + \cfrac{1}{
    1 + \cfrac{1}{
      1+ \cfrac{2}{3}
      }
    }
 =
 1 + \cfrac{1}{
    1 + \cfrac{1}{
      1+ \cfrac{1}{3/2}
      }
    }
 =
 1 + \cfrac{1}{
    1 + \cfrac{1}{
      1+ \cfrac{1}{
          1+ \cfrac{1}{1+1}
        }
      }
    }\\
 &=[1,1,1,1,1,1].
\end{align*}

\noindent
This leads naturally to
the continued fraction algorithm.
Let $x$ be any real number,
and let $a_0 = [x]$. Then
\begin{align*}
 x=a_0+\alpha_0,
 \hspace{0.5cm}
 0 \le \alpha_0 < 1.
\end{align*}

\noindent
If $\alpha_0 \ne 0$, we can write
\begin{align*}
 \frac{1}{\alpha_0}=a^{\prime}_1,
 \hspace{0.5cm}
 [a^{\prime}_1]=a_1,
 \hspace{0.5cm}
 a^{\prime}_1=a_1+\alpha_1,
 \hspace{0.5cm}
 0 \le \alpha_1 < 1.
\end{align*}

\noindent
If $\alpha_1 \ne 0$, we can write
\begin{align*}
 \frac{1}{\alpha_1}=a^{\prime}_2,
 \hspace{0.5cm}
 0 \le \alpha_2 < 1,
\end{align*}
and so on.

\noindent
For example, applying
the continued fraction
algorithm to $P_4/P_3$ yields:
\begin{align*}
 \frac{P_4}{P_3}
 =
 \frac{12}{5}
 &=2 + \frac{2}{5}
 =2 + \cfrac{1}{
    \cfrac{1}{5/2}
    }
 =1 + \cfrac{1}{
    2 + \cfrac{1}{2}
    }
 =1 + \cfrac{1}{
    2 + \cfrac{1}{2}
    }
 =[2,2,2].
\end{align*}

\noindent
Applying the algorithm
to $\sqrt{2}+1$ yields:
\begin{align*}
 \sqrt{2}+1
 &=
 2+(\sqrt{2}-1)
 =
 2 + \frac{1}{1/(\sqrt{2}-1)}
 =
 2+
 \cfrac{1}{
    \cfrac{1}{\sqrt{2}-1}
    \cdot
    \cfrac{\sqrt{2}+1}{\sqrt{2}+1}
 }\\
 &=2+\cfrac{1}{\sqrt{2}+1}
 =
 2+\cfrac{1}{2+
     \cfrac{1}{2+
       \cfrac{1}{2+
       \ddots
       }
     }
   }
 =[2,2, \cdots].
\end{align*}

\noindent
We state the following theorems
without proof.

\begin{thm}
 Any rational number can be represented
 by a finite simple continued fraction.
\end{thm}

\begin{thm}
 Every irrational number can be expressed
 in just one way as an infinite simple
 continued fraction.
\end{thm}

\noindent
Applying the
continued fraction algorithm
to $\sqrt{3}$ yields:
\begin{align*}
 \sqrt{3}
 &=
 1+(\sqrt{3}-1)
 =
 1 + \frac{1}{1/(\sqrt{3}-1)}
 =
 1+
 \cfrac{1}{
    \cfrac{1}{\sqrt{3}-1}
    \cdot
    \cfrac{\sqrt{3}+1}{\sqrt{3}+1}
 }\\
 &=1+\cfrac{1}{1+
 \cfrac{1}{
 1+\sqrt{3}}
 }
 =
 1+\cfrac{1}{1+
     \cfrac{1}{2+
       \cfrac{2}{1+
       \ddots
       }
     }
   }
   =[1, \overline{1,2}],
\end{align*}
where $[1, \overline{1,2}]$
is a simple periodic
continued fraction.
The digits under the
line repeat.

\noindent
We state the following theorems
without proof.

\begin{thm}
 A periodic continued fraction
 is a quadratic surd,
 i.e. an irrational root
 of a quadratic equation with
 integral coefficients.
\end{thm}

\begin{thm}
 The continued fraction which
 represents a quadratic surd is
 periodic.
\end{thm}



