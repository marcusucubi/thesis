\chapter{Fibonacci and $A_n$, $B_n$
Formulas}
\label{identitieschap}

By comparing the definition
of the Fibonacci numbers
and the Continued Fraction
Mapping, we see that the
Fibonacci numbers are the
numerators and
denominators of \GoldenF.
It follows that when we find
an identity for the Fibonacci
numbers we can find an
analogous identity for
the Continued Fraction
Mapping. We will explore
these identities in this
chapter.

\begin{thm}[Determinant Formula
for General Continued Fractions]
\label{thm:generalDeterminantFormula}
\begin{align}
\label{equ:generalDeterminantFormula}
A_n B_{n-1} - A_{n-1} B_n=
(-1)^{n+1}\, a_1 \dots a_n
\end{align}
\end{thm}

\begin{proof}
Recall equations
\eqref{equ:definitiont} and \eqref{equ:tttT}
from the definition
\ref{equ:definitiont}.
Combining these with
equation \eqref{equ:definitionT},
we obtain
\begin{align*}
  \left[ {\begin{array}{cc}
   A_{n}   & B_{n}   \\
   A_{n-1} & B_{n-1} \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   b_n & a_n \\
   1 & 0   \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b_{n-1} & a_{n-1} \\
   1 & 0   \\
  \end{array} } \right]
   \cdots
  \left[ {\begin{array}{cc}
   b_1 & a_1 \\
   1   & 0    \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   b_0 & 1 \\
   1   & 0 \\
  \end{array} } \right].
\end{align*}
By taking the determinant,
we obtain the result.
\end{proof}

\begin{thm}
(D’Ocagne’s Identity for
$[b,b, \dots ]$)
Given a continued fraction
with denominator $B_n$,
\begin{align}
 B_{m-1}B_{n}+B_{n-1}B_{m}
 =
 (-1)^n B_{m-n-1}
  \label{equ:johnsonsIdentity}
\end{align}
\label{thm:johnsonsIdentity}
\end{thm}
\begin{proof}
(This following proof is
adapted from a proof given by
 Johnson \cite{johnsonPdf}.)
Given a continued fraction
$[b,b, \dots]$, then
by Theorem \ref{thm:AisB}
\begin{align*}
  t^n =
  \left[ {\begin{array}{cc}
   B_{n}   & B_{n-1}   \\
   B_{n-1} & B_{n-2} \\
  \end{array} } \right].
\end{align*}
Consider
$t^{m+n+1} = t^{m+n+1}
\hspace{0.3cm}
\Rightarrow
\hspace{0.3cm}
t^{m} t^{n+1} = t^{m+1} t^n$.
This yields:
\begin{align*}
  &\left[ {\begin{array}{cc}
   B_{m}   & B_{m-1}   \\
   B_{m-1} & B_{m-2} \\
  \end{array} } \right]
  \left[ {\begin{array}{cc}
   B_{n+1}   & B_{n}   \\
   B_{n} & B_{n-1} \\
  \end{array} } \right]
  =
  \left[ {\begin{array}{cc}
   B_{m+1}   & B_{m}   \\
   B_{m} & B_{m-1} \\
  \end{array} } \right].
  \left[ {\begin{array}{cc}
   B_{n}   & B_{n-1}   \\
   B_{n-1} & B_{n-2} \\
  \end{array} } \right].
\end{align*}
Computing the
lower right entry:
\begin{align*}
 B_{m-1}B_{n}+B_{m-2}B_{n-1}
 =
 B_{n-1}B_{m}+B_{n-2}B_{m-1}.
\end{align*}
Rearranging:
\begin{align*}
 B_{m-1}B_{n}-B_{n-1}B_m
 =
 B_{n-2}B_{m-1}-B_{m-2}B_{n-1}
 =
 (-1)(B_{m-2}B_{n-1}-
 B_{n-2}B_{m-1}).
\end{align*}
Recall that $B_{-1}=0$
and $B_{0}=1$
and
repeat $n$ times.
\begin{align*}
 B_{m-1}B_{n}-B_{n-1}B_{m}
 &=
 (-1)^n
 (B_{m-n-1}B_{n-n}-
 B_{n-n-1}B_{m-n})\\
 &=
 (-1)^n B_{m-n-1}.
\end{align*}
\end{proof}
\begin{cor}[D'Ocagne's Identity]
\begin{align}
  F_mF_{n+1}-F_{m+1}F_n=
  (-1)^nF_{m-n}.
  \label{equ:dOcagnesIdentity}
\end{align}
\label{thm:ocagnesIdentity}
\end{cor}
\begin{proof}
If the continued
fraction is $[1,1, \dots]$,
then $B_n = F_{n+1}$.
\end{proof}
\begin{cor}[D'Ocagne's Identity
for Pell numbers]
\begin{align}
  P_m P_{n+1} - P_{m+1} P_n=
  (-1)^n P_{m-n}
  \label{equ:dOcagnesE}
\end{align}
\end{cor}
\begin{proof}
If the continued
fraction is $[2,2, \dots]$,
then $B_n = P_{n+1}$.
\end{proof}

\begin{proof}
[Proof of the \GDF\,\,
(Theorem \ref{thm:GDF})]
Recall equation
\eqref{equ:segmentProduct}
from discussion \ref{discussion:segments},
\begin{align*}
 T_m^{n+1} T_n = T_m
 \hspace{0.4cm}
 \text{for}\; m > n.
\end{align*}
From equation
\eqref{equ:segmentProduct},
we have
\begin{align*}
 \left[ {\begin{array}{cc}
  A_m^{n+1}     & B_m^{n+1}     \\
  A_{m-1}^{n+1} & B_{m-1}^{n+1} \\
 \end{array} } \right]
 \left[ {\begin{array}{cc}
  A_n     & B_n     \\
  A_{n-1} & B_{n-1} \\
 \end{array} } \right]
 &=
 \left[ {\begin{array}{cc}
  A_m     & B_m     \\
  A_{m-1} & B_{m-1} \\
 \end{array} } \right].
\end{align*}
Computing the top row
in this matrix product
gives the following
equations:
\begin{align}
  A_m &= A_n A_{m}^{n+1} +
  A_{n-1} B_m^{n+1},
  \label{equ:Am}\\
  B_m &= B_n A_{m}^{n+1} +
  B_{n-1} B_m^{n+1}.
  \label{equ:Bm}
\end{align}
We multiply \eqref{equ:Am}
by $B_n$ and \eqref{equ:Bm}
by $A_n$ to get
the following equations:
\begin{align}
  A_mB_n &= (A_nB_n) A_{m}^{n+1} +
  (A_{n-1} B_n) B_m^{n+1},
  \label{equ:AmBn}\\
  A_nB_m &= (A_nB_n) A_{m}^{n+1} +
  (A_n B_{n-1}) B_m^{n+1}.
  \label{equ:AnBm}
\end{align}
Now subtract \eqref{equ:AnBm}
from \eqref{equ:AmBn} to get
\begin{align*}
 A_mB_n - A_nB_m
 &=
 (A_{n-1} B_n - A_n B_{n-1})
 B_m^{n+1}\\
 &= (-1)^n\, (a_1 \cdots a_n)\,
 B_m^{n+1}.
 & \text{Using the Determinant Formula.}
\end{align*}
This is the equation desired.
\end{proof}





